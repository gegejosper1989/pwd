<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return date('Y');
    return view('welcome');
});
Route::get('/home', function () {
    return view('home');
});

Route::get('/expired', function () {
    return view('expired');
});
Route::get('/login', function () {
    return view('auth.login');
});
Route::get('/admin', function () {
    return view('auth.login');
});
Route::get('/department', function () {
    return view('auth.login');
    //dd($_ENV);
});
Route::post('/userlogin', 'LoginController@userLogin')->name('userLogin');
Auth::routes();

Route::group(['middleware' =>'adminAuth','prefix' => 'admin'], function(){ 
    Route::get('/home', 'AdminController@index')->name('home');
    Route::get('/rr', 'AdminController@rr')->name('rr');
    
    Route::get('/pr', 'AdminController@pr')->name('pr');
   
    Route::get('/pr/report', 'PurchaseController@prlist')->name('prlist');
    Route::get('/pr/create/{prid}', 'PurchaseController@createPrItems')->name('createPrItems');
    Route::get('/pr/view/{prid}', 'PurchaseController@viewPrItems')->name('viewPrItems');
    Route::post('/pritems/create', 'PurchaseController@createPr')->name('createPr');
    Route::post('/pritems/add', 'PurchaseController@prItemsAdd')->name('prItemsAdd');
    Route::post('/pritems/delete', 'PurchaseController@prItemsDelete')->name('prItemsDelete');
    Route::get('/pr/process/{pr_num}', 'PurchaseController@process_pr')->name('process_pr');
    Route::get('/pr-view/{pr_num}', 'PurchaseController@pr_view')->name('pr_view');
    
    
    Route::get('/prstocksearch', 'StockController@prstocksearch')->name('prstocksearch');
    Route::post('/pr/process', 'PurchaseController@processPr')->name('processPr');


    Route::post('/rr/create', 'ReceivingController@create')->name('createrr');
    Route::get('/rr/create/{rrid}', 'ReceivingController@createRRItems')->name('createRRItems');
    Route::get('/rrstocksearch', 'StockController@rrstocksearch')->name('rrstocksearch');
    Route::get('/rr/save-partial/{rr_num}', 'ReceivingController@rr_save_partial')->name('rr_save_partial');
    Route::get('/rr/save/{rr_num}', 'ReceivingController@rr_save')->name('rr_save');
    Route::get('/rr/rr-view/{rr_num}', 'ReceivingController@rr_view')->name('rr_view');
    Route::post('/rr_items/add', 'ReceivingController@rr_items_add')->name('rr_items_add');
    Route::post('/rr_items/delete', 'ReceivingController@rr_items_delete')->name('rr_items_delete');

    Route::get('/riv', 'AdminController@riv')->name('riv');
    Route::get('/riv/report', 'RivController@rivlist')->name('rivlist');
    Route::get('/riv/create/{rivid}', 'RivController@createRivItems')->name('createRivItems');
    Route::get('/riv/view/{rivid}', 'RivController@viewRivItems')->name('viewRivItems');
    Route::post('/rivitems/create', 'RivController@createRiv')->name('createRiv');
    Route::post('/rivitems/add', 'RivController@rivItemsAdd')->name('rivItemsAdd');
    Route::post('/rivitems/delete', 'RivController@rivItemsDelete')->name('rivItemsDelete');
    Route::post('/rivconcessionaire/add', 'RivController@rivConcessionareAdd')->name('rivConcessionareAdd');
    Route::post('/rivconcessionaire/delete', 'RivController@rivConcessionareDelete')->name('rivConcessionareDelete');
    Route::get('/rivstocksearch', 'StockController@rivstocksearch')->name('rivstocksearch');
    Route::post('/riv/process', 'RivController@processRiv')->name('processRiv');

    
    Route::get('/rms', 'AdminController@rms')->name('rms');
    Route::post('/rms/create', 'ReturnController@rms_create')->name('rms_create');
    Route::get('/rms/create/{return_id}', 'ReturnController@create_rms_items')->name('create_rms_items');
    Route::get('/rms/view/{return_number}', 'ReturnController@rms_view')->name('rms_view');
    Route::get('/rmsstocksearch', 'StockController@rmsstocksearch')->name('rmsstocksearch');
    Route::post('/rmsitems/add', 'ReturnController@rms_items_add')->name('rms_items_add');
    Route::post('/rmsitems/delete', 'ReturnController@rms_items_delete')->name('rms_items_delete');
    Route::post('/rms/process', 'ReturnController@process_rms')->name('process_rms');

    Route::get('/stocks', 'AdminController@stocks')->name('stocks');
    Route::get('/stocks/category/{catcode}', 'StockController@viewstocksbycategory')->name('viewstocksbycategory');
    Route::post('/stocks/category/search', 'StockController@searchstocksbycategory')->name('searchstocksbycategory');
    Route::post('/stocks/add', 'StockController@addStock')->name('addStock');
    
    
    Route::get('/bincard', 'AdminController@bincard')->name('bincard');
    Route::get('/bincard/{stockid}', 'AdminController@viewbincard')->name('viewbincard');
    Route::get('/bincardstocksearch', 'StockController@stocksearchbincard')->name('stocksearchbincard');
    
    Route::get('/watermeter', 'AdminController@watermeter')->name('watermeter');
    Route::get('/reorder', 'AdminController@reorder')->name('reorder');
    Route::post('/reorder/set', 'StockController@setreorder')->name('setreorder');
    
    Route::get('/stocks/category', 'AdminController@category')->name('category');
    Route::post('/stocks/category/add', 'CategoryController@add')->name('categoryadd');
    Route::post('/stocks/category/update', 'CategoryController@update')->name('categoryupdate');
    Route::post('/stocks/category/delete', 'CategoryController@delete')->name('categorydelete');
    Route::get('/stock/{stockid}', 'AdminController@viewbincard')->name('viewstocks');
    Route::get('/stocks/descriptions', 'AdminController@descriptions')->name('descriptions');
    Route::post('/stocks/edit', 'StockController@editStock')->name('editStock');
    
    
    Route::get('/accounts', 'AdminController@accounts')->name('accounts');
    Route::get('/deparments', 'AdminController@deparments')->name('deparments');
    Route::post('/department/add', 'DepartmentController@add')->name('deparmentadd');
    Route::post('/department/update', 'DepartmentController@update')->name('deparmentupdate');
    Route::post('/department/delete', 'DepartmentController@delete')->name('deparmentdelete');

    Route::get('/employees', 'AdminController@employees')->name('employees');
    Route::post('/employee/add', 'EmployeeController@add')->name('employee_add');
    Route::post('/employee/update', 'EmployeeController@update')->name('employee_update');
    Route::post('/employee/delete', 'EmployeeController@delete')->name('employee_delete');

    Route::get('/vehicles', 'AdminController@vehicles')->name('vehicles');
    Route::post('/vehicle/add', 'VehicleController@add')->name('vehicleadd');
    Route::post('/vehicle/update', 'VehicleController@update')->name('vehicleupdate');
    Route::post('/vehicle/delete', 'VehicleController@delete')->name('vehicledelete');

    Route::get('/suppliers', 'AdminController@suppliers')->name('suppliers');
    Route::post('/supplier/add', 'SupplierController@add')->name('supplieradd');
    Route::post('/supplier/update', 'SupplierController@update')->name('supplierupdate');
    Route::post('/supplier/delete', 'SupplierController@delete')->name('supplierdelete');

    Route::get('/logs', 'AdminController@logs')->name('logs');
    Route::get('/backup', 'AdminController@backup')->name('backup');

    Route::get('/stocksearch', 'StockController@stocksearchlive')->name('stocksearchlive');
    Route::get('/stocksearchbudget', 'StockController@stocksearchbudget')->name('stocksearchbudget');
    
    Route::get('/budget', 'AdminController@budget')->name('budget');
    Route::post('/budget/update', 'BudgetController@updatebudget')->name('updatebudget');
    Route::get('/processbudget', 'BudgetController@processbudget')->name('processbudget');
    Route::get('/resetbudget', 'BudgetController@resetbudget')->name('resetbudget');
    Route::get('/resetbudgetsuccess', 'BudgetController@resetbudgetsuccess')->name('resetbudgetsuccess');
    Route::get('/processbudgetsuccess', 'BudgetController@processbudgetsuccess')->name('processbudgetsuccess');

    Route::get('/consessioaniresearch', 'AdminController@consessioaniresearch')->name('consessioaniresearch');
    Route::post('/consessionaire/edit', 'AdminController@consessionaire_edit')->name('consessionaire_edit');


    Route::get('/signatures', 'AdminController@signatures')->name('signatures');
    Route::post('/signatures/rms', 'SignatureController@signatures_rms')->name('signatures_rms');
    Route::post('/signatures/rr', 'SignatureController@signatures_rr')->name('signatures_rr');
    Route::post('/signatures/riv', 'SignatureController@signatures_riv')->name('signatures_riv');
    Route::post('/signatures/pr', 'SignatureController@signatures_pr')->name('signatures_pr');

    Route::get('/users', 'UsersController@readUser')->name('users');   
    Route::get('/users/{id}', 'UsersController@showUser')->name('showusers');   
    Route::post('/users/addusers', 'UsersController@addUser')->name('addusers');
    Route::post('/users/editusers', 'UsersController@editUser')->name('editusers');
    Route::post('/users/deleteusers', 'UsersController@deleteUser')->name('deleteusers');
    Route::post('/users/deletebranchusers', 'UsersController@deleteUser')->name('deleteusers');
    

    Route::get('/logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index'); 
});

Route::group(['middleware' =>'departmentAuth','prefix' => 'department'], function(){ 
    Route::get('/home', 'DeparmentAccountController@index')->name('department_home');
    Route::get('/riv/report', 'DeparmentAccountController@rivlist')->name('rivlist');
    Route::get('/riv/create/{rivid}', 'DeparmentAccountController@create_riv_items')->name('create_riv_items');
    Route::get('/riv/view/{rivid}', 'DeparmentAccountController@view_riv_tems')->name('view_riv_tems');
    Route::post('/rivitems/create', 'DeparmentAccountController@create_riv')->name('create_riv');
    Route::post('/rivitems/add', 'DeparmentAccountController@riv_items_add')->name('riv_items_add');
    Route::post('/rivitems/delete', 'DeparmentAccountController@riv_items_delete')->name('riv_items_delete');
    Route::get('/rivstocksearch', 'DeparmentAccountController@riv_stock_search')->name('riv_stock_search');
    Route::post('/riv/process', 'DeparmentAccountController@process_riv')->name('process_riv');
});