<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRivitemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rivitems', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('rivnumber');
            $table->string('catcode');
            $table->string('stocknumber');
            $table->string('description');
            $table->string('onhandquantity');
            $table->string('reqquantity');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rivitems');
    }
}
