<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReturninfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('returninfos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('return_date');
            $table->string('return_number');
            $table->string('department');
            $table->string('srs_number');
            $table->string('reason');
            $table->string('requestor');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('returninfos');
    }
}
