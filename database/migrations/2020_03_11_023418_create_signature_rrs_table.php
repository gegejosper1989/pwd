<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSignatureRrsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('signature_rrs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('member_1');
            $table->string('member_2');
            $table->string('team_leader');
            $table->string('accepted_by');
            $table->string('certified_correct');
            $table->string('posted_bin_card');
            $table->string('posted_stock_card');
            $table->string('costed_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('signature_rrs');
    }
}
