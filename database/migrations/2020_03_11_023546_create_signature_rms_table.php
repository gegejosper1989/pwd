<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSignatureRmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('signature_rms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('approved_by');
            $table->string('approved_return');
            $table->string('posted_bin_card');
            $table->string('posted_stock_card');
            $table->string('accounted_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('signature_rms');
    }
}
