<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prdetails', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('prdate');
            $table->string('prnum');
            $table->string('prsupplier');
            $table->string('paymentmode');
            $table->string('prstatus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prdetails');
    }
}
