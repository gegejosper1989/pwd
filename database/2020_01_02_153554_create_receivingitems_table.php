<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceivingitemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receivingitems', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('rrnumber');
            $table->string('catcode');
            $table->string('stocknumber');
            $table->string('description');
            $table->string('deliverytype');
            $table->string('quantity');
            $table->string('unitcost');
            $table->string('othercharges')->nullable(false)->change();
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receivingitems');
    }
}
