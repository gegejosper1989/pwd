<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRivdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rivdetails', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('date');

            $table->string('rivnumber');
            $table->string('purpose');
            $table->string('department');
            $table->string('workorder');
            $table->string('vehiclenum');
            $table->string('incharge');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rivdetails');
    }
}
