<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePritemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pritems', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('prnumber');
            $table->string('catnumber');
            $table->string('stocknumber');
            $table->string('reqquantity');
            $table->string('receivequant');
            $table->string('unitcost');
            $table->string('totalcost');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pritems');
    }
}
