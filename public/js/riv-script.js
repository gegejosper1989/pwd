$(document).ready(function() {
  
    $(document).on('click', '.add-modal', function() {
          $('#footer_action_button').text("Update");
          $('#footer_action_button').addClass('glyphicon-check');
          $('#footer_action_button').removeClass('glyphicon-trash');
          $('.actionBtn').addClass('btn-success');
          $('.actionBtn').removeClass('btn-danger');
          $('.actionBtn').addClass('edit');
          $('.modal-title').text('Add Request');
          $('.deleteContent').hide();
          $('.form-horizontal').show();
          $('#fid').val($(this).data('id'));
          $('#stocknum').val($(this).data('stocknumber'));
          $('#stock_id').val($(this).data('stock_id'));
          $('#description').val($(this).data('stockname'));
          $('#catcode').val($(this).data('catcode'));
          $('#onhandQuantity').val($(this).data('quantity'));
         
          $('#stockModal').modal('show');
          //console.log($(this).data('name') + $(this).data('points'));
      });
    
    $(document).on('click', '.delete-modal', function() {

        $('.actionBtn').addClass('btn-success');
        $('.actionBtn').removeClass('btn-danger');
        $('.modal-title').text('Remove Request');
        $('#fid').val($(this).data('id'));
        $('#deleteModal').modal('show');
        //console.log($(this).data('name') + $(this).data('points'));
    });

    $(document).on('click', '.confirm-modal', function() {
        $('#revid').val($(this).data('rivnumber'));
        $('#confirmModal').modal('show');

    });
    $('.modal-footer').on('click', '.confirmProcess', function() {
  
        $.ajax({
            type: 'post',
            url: '/department/riv/process',
            data: {
                '_token': $('input[name=_token]').val(),
                'rivnumber': $('#revid').val()
            },
            success: function(data) {
                window.location.href = '/department/riv/view/' + data.rivnumber; 
            }
              
        });
    });
    
    $('.modal-footer').on('click', '.saveRiv', function() {
  
          $.ajax({
              type: 'post',
              url: '/department/rivitems/add',
              data: {
                  //_token:$(this).data('token'),
                  '_token': $('input[name=_token]').val(),
                  'stocknumber': $('#stocknum').val(),
                  'stock_id': $('#stock_id').val(),
                  'description': $('#description').val(),
                  'onhandquantity': $('#onhandQuantity').val(),
                  'catcode': $('#catcode').val(),
                  'reqquantity': $('#addQuantity').val(),
                  'rivnumber': $('#rivnumberquantity').val()
              },
              success: function(data) {

            $('#rivdetails').append("<tr class='item" + data.id + "'><td>"+ data.catcode +"</td><td>"+ data.stock_id +"</td><td>" + data.description + "</td><td>"+ data.onhandquantity + "</td><td>"+ data.reqquantity + "</td><td class='td-actions'><a class='delete-modal btn btn-danger btn-small' data-id='" + data.id + "'><i class='fa fa-times'></i></a></td></tr>");
                    new PNotify({
                        title: 'Success',
                        text: 'Item successfully updated',
                        type: 'info',
                        delay: 2000,
                        styling: 'bootstrap3'
                    }); 
                }
                
          });
          $('.norecordriv').remove();
      });

      $('.modal-footer').on('click', '.delete', function() {
          $.ajax({
              type: 'post',
              url: '/department/rivitems/delete',
              data: {
                  '_token': $('input[name=_token]').val(),
                  'id': $('#fid').val()
              },
              success: function(data) {
                  console.log(data);
                new PNotify({
                    title: 'Success',
                    text: 'Item successfully deleted',
                    type: 'danger',
                    delay: 2000,
                    styling: 'bootstrap3'
                });  
                $('.item' + data).remove();
              }
          });
      });
      
  });
  