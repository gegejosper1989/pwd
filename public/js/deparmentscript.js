$(document).ready(function() {
    var toValidate = $('#depcode, #depname'),
    valid = false;
    toValidate.keyup(function () {
        if ($(this).val().length > 0) {
            $(this).data('valid', true);
        } else {
            $(this).data('valid', false);
        }
        toValidate.each(function () {
            if ($(this).data('valid') == true) {
                valid = true;
            } else {
                valid = false;
            }
        });
        if (valid === true) {
            $('#btnsave').prop('disabled', false);
        }else{
            $('#btnsave').prop('disabled', true);        
        }
    });

    $(document).on('click', '.edit-modal', function() {
          $('#fid').val($(this).data('id'));
          $('#editDepCode').val($(this).data('depcode'));
          $('#editDepName').val($(this).data('depname'));
          $('#editDepHeadVal').val($(this).data('depheadid'));
          $('#editDepHeadVal').text($(this).data('dephead'));
          $('#departmentModal').modal('show');
      });
      $(document).on('click', '.delete-modal', function() {
          $('#delid').val($(this).data('id'));
          $('#deleteModal').modal('show');
      });
  
      $('.modal-footer').on('click', '.editSave', function() {
  
          $.ajax({
              type: 'post',
              url: '/admin/department/update',
              data: {
                  '_token': $('input[name=_token]').val(),
                  'id': $("#fid").val(),
                  'depcode': $('#editDepCode').val(),
                  'depname': $('#editDepName').val(),
                  'dephead': $('select[name=editDepHead]').val() 
              },
              success: function(data) {
                  $('.item' + data[0].id).replaceWith(`
                        <tr class='item${data[0].id}'>
                        <td> <a href='/admin/deparment/${data[0].depcode}'>${data[0].depcode}</a></td>
                        <td><a href='/admin/deparment/${data[0].depcode}'>${data[0].depname}</a></td>
                        <td>${data[0].dephead}</td>
                        <td><button class='edit-modal btn btn-small btn-success' 
                            data-id="${data[0].id}"
                            data-depcode="${data[0].depcode}"
                            data-depname="${data[0].depname}"
                            data-dephead="${data[0].dephead}"
                            data-depheadid="${data[0].depheadid}">
                            <i class='fa fa-edit'></i></button>
                        </td></tr>`);
                    new PNotify({
                        title: 'Success',
                        text: 'Department successfully updated',
                        type: 'info',
                        delay: 2000,
                        styling: 'bootstrap3'
                    }); 
                }
                
          });
      });
      $("#btnsave").click(function() {
  
          $.ajax({
              type: 'post',
              url: '/admin/department/add',
              data: {
                  '_token': $('input[name=_token]').val(),
                  'depcode': $('input[name=depcode]').val(),
                  'depname': $('input[name=depname]').val(),
                  'dephead': $('select[name=dephead]').val() 
                     
              },
              success: function(data) {
                      $('#departmenttable').append(`
                        <tr class='item${data[0].id}'>
                            <td> <a href='/admin/deparment/${data[0].depcode}'>${data[0].depcode}</a></td>
                            <td><a href='/admin/deparment/${data[0].depcode}'>${data[0].depname}</a></td>
                            <td>${data[0].dephead}</td>
                            <td><button class='edit-modal btn btn-small btn-success' 
                                data-id="${data[0].id}"
                                data-depcode="${data[0].depcode}"
                                data-depname="${data[0].depname}"
                                data-dephead="${data[0].dephead}"
                                data-depheadid="${data[0].depheadid}">
                                <i class='fa fa-edit'></i></button>
                                <a class='delete-modal btn btn-danger btn-small' 
                                data-id='${data[0].id}'>
                                    <i class='fa fa-times'></i></a>
                        </td></tr>`);
                      new PNotify({
                        title: 'Success',
                        text: 'Department successfully added',
                        type: 'success',
                        delay: 2000,
                        styling: 'bootstrap3'
                    }); 
              }
          });
          $('#depcode').val('');
          $('#depname').val('');
      });
      $('.modal-footer').on('click', '.delete', function() {
          $.ajax({
              type: 'post',
              url: '/admin/department/delete',
              data: {
                  '_token': $('input[name=_token]').val(),
                  'id': $('#delid').val()
              },
              success: function(data) {
                new PNotify({
                    title: 'Success',
                    text: 'Department successfully deleted',
                    type: 'danger',
                    delay: 2000,
                    styling: 'bootstrap3'
                });  
                $('.item' + $('#delid').val()).remove();
              }
          });
      });
  });
  