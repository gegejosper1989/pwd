$(document).ready(function() {
  
    $(document).on('click', '.set-modal', function() {
          $('#footer_action_button').text("Update");
          $('#footer_action_button').addClass('glyphicon-check');
          $('#footer_action_button').removeClass('glyphicon-trash');
          $('.actionBtn').addClass('btn-success');
          $('.actionBtn').removeClass('btn-danger');
          $('.actionBtn').addClass('edit');
          $('.modal-title').text('Set Ordering Point');
          $('.deleteContent').hide();
          $('.form-horizontal').show();
          $('#fid').val($(this).data('id'));
          $('#stocknum').val($(this).data('stocknumber'));
          $('#description').val($(this).data('stockname'));
          $('#catcode').val($(this).data('catcode'));
          $('#stockModal').modal('show');
          //console.log($(this).data('name') + $(this).data('points'));
      });
    $(document).on('click', '.cons-delete-modal', function() {

        $('.actionBtn').addClass('btn-success');
        $('.actionBtn').removeClass('btn-danger');
        $('.modal-title').text('Remove Concessionaire');
        $('#consid').val($(this).data('id'));
        $('#consdeleteModal').modal('show');
        //console.log($(this).data('name') + $(this).data('points'));
    });
    $(document).on('click', '.delete-modal', function() {

        $('.actionBtn').addClass('btn-success');
        $('.actionBtn').removeClass('btn-danger');
        $('.modal-title').text('Remove Request');
        $('#fid').val($(this).data('id'));
        $('#deleteModal').modal('show');
        //console.log($(this).data('name') + $(this).data('points'));
    });
     
      $('.modal-footer').on('click', '.setReordering', function() {
  
          $.ajax({
              type: 'post',
              url: '/admin/reorder/set',
              data: {
                  //_token:$(this).data('token'),
                  '_token': $('input[name=_token]').val(),
                  'stocknumber': $('#stocknum').val(),
                  'id': $('#fid').val(),
                  'orderingpoint': $('#orderingpoint').val(),
                  'description': $('#description').val(),
                  'catcode': $('#catcode').val()
              },
              success: function(data) {
                console.log(data);
                $('#reoderingpoint').append("<tr class='setitem" + data[0].id + "'><td>"+ data[0].catcode +"</td><td>"+ data[0].stocknumber +"</td><td>" + data[0].stockname + "</td><td>"+ data[0].orderingpoint + "</td><td class='td-actions'><a class='set-modal btn btn-sm btn-info' data-id='" + data[0].id + "' data-stocknumber='" + data[0].stocknumber + "' data-catcode='" + data[0].catcode + "' data-stockname='"+ data[0].stockname +"'><i class='fa fa-edit'></i></a></td></tr>");
                    new PNotify({
                        title: 'Success',
                        text: 'Ordering Point successfully updated',
                        type: 'info',
                        delay: 2000,
                        styling: 'bootstrap3'
                    }); 
                $('.item' + data[0].id).remove();       
            }
              
          });
      });
      $(document).on('click', '.add-conse', function() {
    
            $.ajax({
                type: 'post',
                url: '/admin/rivconcessionaire/add',
                data: {
                    //_token:$(this).data('token'),
                    '_token': $('input[name=_token]').val(),
                    'meternum': $('#meternum').val(),
                    'address': $('#address').val(),
                    'incharge': $('#incharge').val(),
                    'fullname': $('#fullname').val(),
                    'rivnumber': $('#rivnumbercons').val()
                },
                success: function(data) {

            $('#rivconcessionaire').append("<tr class='cons-item" + data.id + "'><td>"+ data.fullname +"</td><td>"+ data.address +"</td><td>" + data.meternum + "</td><td>"+ data.incharge + "</td><td><a class='cons-delete-modal btn btn-danger btn-small' data-id='" + data.id + "'><i class='fa fa-times'></i></a></td></tr>");
                    new PNotify({
                        title: 'Success',
                        text: 'User successfully updated',
                        type: 'info',
                        delay: 2000,
                        styling: 'bootstrap3'
                    }); 
                }
                
            });
            $('#incharge').val('');
            $('#address').val('');
            $('#meternum').val('');
            $('#fullname').val('');

        });

        $(document).on('click', '.add-conse', function() {
    
            $.ajax({
                type: 'post',
                url: '/admin/rivconcessionaire/add',
                data: {
                    //_token:$(this).data('token'),
                    '_token': $('input[name=_token]').val(),
                    'meternum': $('#meternum').val(),
                    'address': $('#address').val(),
                    'incharge': $('#incharge').val(),
                    'fullname': $('#fullname').val(),
                    'rivnumber': $('#rivnumber').val()
                },
                success: function(data) {

            $('#rivconcessionaire').append("<tr class='cons-item" + data.id + "'><td>"+ data.fullname +"</td><td>"+ data.address +"</td><td>" + data.meternum + "</td><td>"+ data.incharge + "</td><td><a class='cons-delete-modal btn btn-danger btn-small' data-id='" + data.id + "'><i class='fa fa-times'></i></a></td></tr>");
                    new PNotify({
                        title: 'Success',
                        text: 'User successfully updated',
                        type: 'info',
                        delay: 2000,
                        styling: 'bootstrap3'
                    }); 
                }
                
            });
            $('#incharge').val('');
            $('#address').val('');
            $('#meternum').val('');
            $('#fullname').val('');

        });
      $('.modal-footer').on('click', '.delete', function() {
          $.ajax({
              type: 'post',
              url: '/admin/rivitems/delete',
              data: {
                  '_token': $('input[name=_token]').val(),
                  'id': $('#fid').val()
              },
              success: function(data) {
                  console.log(data);
                new PNotify({
                    title: 'Success',
                    text: 'Item successfully deleted',
                    type: 'danger',
                    delay: 2000,
                    styling: 'bootstrap3'
                });  
                $('.item' + data).remove();
              }
          });
      });
      $('.modal-footer').on('click', '.delete-cons', function() {
        $.ajax({
            type: 'post',
            url: '/admin/rivconcessionaire/delete',
            data: {
                '_token': $('input[name=_token]').val(),
                'id': $('#consid').val()
            },
            success: function(data) {
                console.log(data);
              new PNotify({
                  title: 'Success',
                  text: 'Item successfully deleted',
                  type: 'danger',
                  delay: 2000,
                  styling: 'bootstrap3'
              });  
              $('.cons-item' + data).remove();
            }
        });
    });
  });
  