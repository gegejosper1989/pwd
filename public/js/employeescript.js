$(document).ready(function() {
    var toValidate = $('#fname, #mname, #lname, #position'),
    valid = false;
    toValidate.keyup(function () {
        if ($(this).val().length > 0) {
            $(this).data('valid', true);
        } else {
            $(this).data('valid', false);
        }
        toValidate.each(function () {
            if ($(this).data('valid') == true) {
                valid = true;
            } else {
                valid = false;
            }
        });
        if (valid === true) {
            $('#btnsave').prop('disabled', false);
        }else{
            $('#btnsave').prop('disabled', true);        
        }
    });

    $(document).on('click', '.edit-modal', function() {
          $('#fid').val($(this).data('id'));
          $('#edit_fname').val($(this).data('fname'));
          $('#edit_mname').val($(this).data('mname'));
          $('#edit_lname').val($(this).data('lname'));
          $('#edit_position').val($(this).data('position'));
          $('#edit_department').val($(this).data('depcode'));
          $('#employeeModal').modal('show');
      });
      $(document).on('click', '.delete-modal', function() {
          $('#delid').val($(this).data('id'));
          $('#deleteModal').modal('show');
      });
  
      $('.modal-footer').on('click', '.editSave', function() {
  
          $.ajax({
              type: 'post',
              url: '/admin/employee/update',
              data: {
                  '_token': $('input[name=_token]').val(),
                  'id': $("#fid").val(),
                  'mname': $('#edit_mname').val(),
                  'lname': $('#edit_lname').val(),
                  'fname': $('#edit_fname').val(),
                  'position': $('#edit_position').val(),
                  'department': $('select[name=edit_department]').val() 
              },
              success: function(data) {
                  $('.item' + data[0].id).replaceWith(`
                    <tr class='item${data[0].id}'>
                        <td>${data[0].fname} ${data[0].mname} ${data[0].lname}</td>
                        <td>${data[0].position}</td>
                        <td>${data[0].department}</td>
                        <td><button class='edit-modal btn btn-small btn-success' 
                            data-id="${data[0].id}"
                            data-department="${data[0].department}"
                            data-position="${data[0].position}"
                            data-fname="${data[0].fname}"
                            data-lname="${data[0].lname}"
                            data-mname="${data[0].mname}"
                            >
                            <i class='fa fa-edit'></i></button>
                        </td>
                    </tr>`);
                    new PNotify({
                        title: 'Success',
                        text: 'Employee successfully updated',
                        type: 'info',
                        delay: 2000,
                        styling: 'bootstrap3'
                    }); 
                }
                
          });
      });
      $("#btnsave").click(function() {
  
          $.ajax({
              type: 'post',
              url: '/admin/employee/add',
              data: {
                  '_token': $('input[name=_token]').val(),
                  'fname': $('input[name=fname]').val(),
                  'mname': $('input[name=mname]').val(),
                  'lname': $('input[name=lname]').val(),
                  'position': $('input[name=position]').val(),
                  'department': $('select[name=department]').val() 
                     
              },
              success: function(data) {
                      $('#employeetable').append(`
                        <tr class='item${data[0].id}'>
                            <td>${data[0].fname} ${data[0].mname} ${data[0].lname}</td>
                            <td>${data[0].position}</td>
                            <td>${data[0].department}</td>
                            <td><button class='edit-modal btn btn-small btn-success' 
                                data-id="${data[0].id}"
                                data-department="${data[0].department}"
                                data-position="${data[0].position}"
                                data-fname="${data[0].fname}"
                                data-lname="${data[0].lname}"
                                data-mname="${data[0].mname}"
                                >
                                <i class='fa fa-edit'></i></button>
                                <a class='delete-modal btn btn-danger btn-small' 
                                data-id='${data[0].id}'>
                                    <i class='fa fa-times'></i></a>
                        </td></tr>`);
                      new PNotify({
                        title: 'Success',
                        text: 'Employee successfully added',
                        type: 'success',
                        delay: 2000,
                        styling: 'bootstrap3'
                    }); 
              }
          });
          $('#depcode').val('');
          $('#depname').val('');
      });
      $('.modal-footer').on('click', '.delete', function() {
          $.ajax({
              type: 'post',
              url: '/admin/employee/delete',
              data: {
                  '_token': $('input[name=_token]').val(),
                  'id': $('#delid').val()
              },
              success: function(data) {
                new PNotify({
                    title: 'Success',
                    text: 'Employee successfully deleted',
                    type: 'danger',
                    delay: 2000,
                    styling: 'bootstrap3'
                });  
                $('.item' + $('#delid').val()).remove();
              }
          });
      });
  });
  