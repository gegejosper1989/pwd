$(document).ready(function() {

    $(document).on('click', '.edit-modal', function() {
          $('#footer_action_button').text("Update");
          $('#footer_action_button').addClass('glyphicon-check');
          $('#footer_action_button').removeClass('glyphicon-trash');
          $('.actionBtn').addClass('btn-success');
          $('.actionBtn').removeClass('btn-danger');
          $('.actionBtn').addClass('edit');
          $('.modal-title').text('Edit Details');
          $('.deleteContent').hide();
          $('.form-horizontal').show();
          $('#fid').val($(this).data('id'));
          $('#edit_meter_num').val($(this).data('meternum'));
          $('#edit_consessionaire').val($(this).data('fullname'));
          $('#edit_address').val($(this).data('address'));
          $('#edit_incharge').val($(this).data('incharge'));
          $('#riv_num').val($(this).data('riv_num'));
          $('#date_created').val($(this).data('date_created'));
          $('#consessionaireModal').modal('show');
          //console.log($(this).data('name') + $(this).data('points'));
      });

      $('.modal-footer').on('click', '.saveEdit', function() {
  
          $.ajax({
              type: 'post',
              url: '/admin/consessionaire/edit',
              data: {
                  //_token:$(this).data('token'),
                  '_token': $('input[name=_token]').val(),
                  'id': $("#fid").val(),
                  'meternum': $('#edit_meter_num').val(),
                  'fullname': $('#edit_consessionaire').val(),
                  'address': $('#edit_address').val(),
                  'incharge': $('#edit_incharge').val(),
                  'riv_num': $('#riv_num').val(),
                  'date_created': $('#date_created').val()
              },
              success: function(data) {
                  //console.log(data);
                $('.row' + data[0].id).replaceWith(`
                  <tr class="row${data[0].id}">
                    <td><a href="/admin/riv/view/${data[0].riv_num}">${data[0].riv_num}</a></td>
                    <td>${data[0].date_created}</td>
                    <td>${data[0].meternum}</td>
                    <td>${data[0].fullname}</td>
                    <td>${data[0].address}</td>
                    <td>${data[0].incharge}</td>
                    <td><button class="btn edit-modal btn-success btn-sm" 
                        data-id="${data[0].id}"
                        data-meternum="${data[0].meternum}"
                        data-fullname="${data[0].fullname}"
                        data-address="${data[0].address}"
                        data-riv_num="${data[0].incharge}"
                        data-date_created="${data[0].date_created}"
                        ><i class="fa fa-edit"></i>Edit</button>
                        <a href="/admin/consessionaire/${data[0].id}" class="btn btn-warning btn-sm"><i class="fa fa-search"></i> View</a>
                    </td>
                    </tr>`);
                    new PNotify({
                        title: 'Success',
                        text: 'Stock successfully updated',
                        type: 'info',
                        delay: 2000,
                        styling: 'bootstrap3'
                    }); 
                }
                
          });
      });
      $("#stocksave").click(function() {
  
          $.ajax({
              type: 'post',
              url: '/admin/stocks/add',
              data: {
                  '_token': $('input[name=_token]').val(),
                  'stockcat': $('select[name=stockcat]').val(),
                  'stocknumber': $('input[name=stocknumber]').val(),
                  'description': $('input[name=description]').val(),
                  'orderingpoint': $('input[name=orderingpoint]').val(),
                  'quantity': $('input[name=quantity]').val(),
                  'unit': $('select[name=unit]').val()
                  
              },
              success: function(data) {
                console.log(data);
                $('#tablestocks').append(`
                    <tr class='row${data.id}'>
                    <td>${data.catcode}</td>
                    <td>${data.stocknumber}</td>
                    <td>${data.stockname}</td>
                    <td>${data.quantity}</td>
                    <td>${data.unit}</td>
                    <td>${data.orderingpoint}</td>
                    <td>
                    <button class='btn edit-modal btn-success btn-sm' 
                    data-id="${data.id}"
                    data-catcode="${data.catcode}"
                    data-stocknumber="${data.stocknumber}"
                    data-stockname="${data.stockname}"
                    data-quantity="${data.quantity}"
                    data-unit="${data.unit}"
                    data-ordering="${data.orderingpoint}"
                    ><i class="fa fa-edit"></i>Edit</button>
                    <a href="/admin/consessionaire/${data.id}" class="btn btn-warning btn-sm"><i class="fa fa-search"></i> View</a></td></tr>`);
                new PNotify({
                title: 'Success',
                text: 'Consessionaire successfully updated',
                type: 'success',
                delay: 2000,
                styling: 'bootstrap3'
                });         
              },
  
          });
          $('#stocknumber').val('');
          $('#description').val('');
          $('#orderingpoint').val('');
          $('#quantity').val('');
      });
      $('.modal-footer').on('click', '.delete', function() {
          $.ajax({
              type: 'post',
              url: '/admin/branches/users/delete',
              data: {
                  '_token': $('input[name=_token]').val(),
                  'id': $('.did').text()
              },
              success: function(data) {
                new PNotify({
                    title: 'Success',
                    text: 'User successfully deleted',
                    type: 'danger',
                    delay: 2000,
                    styling: 'bootstrap3'
                });  
                $('.item' + $('.did').text()).remove();
              }
          });
      });
  });
  