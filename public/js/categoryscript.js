$(document).ready(function() {
    var toValidate = $('#catcode, #catname'),
    valid = false;
    toValidate.keyup(function () {
        if ($(this).val().length > 0) {
            $(this).data('valid', true);
        } else {
            $(this).data('valid', false);
        }
        toValidate.each(function () {
            if ($(this).data('valid') == true) {
                valid = true;
            } else {
                valid = false;
            }
        });
        if (valid === true) {
            $('#btnsave').prop('disabled', false);
        }else{
            $('#btnsave').prop('disabled', true);        
        }
    });

    $(document).on('click', '.edit-modal', function() {
          $('#fid').val($(this).data('id'));
          $('#editCatCode').val($(this).data('catcode'));
          $('#editCatName').val($(this).data('catname'));
          $('#categoryModal').modal('show');
      });
      $(document).on('click', '.delete-modal', function() {
          $('#delid').val($(this).data('id'));
          $('#deleteModal').modal('show');
      });
  
      $('.modal-footer').on('click', '.editSave', function() {
  
          $.ajax({
              type: 'post',
              url: '/admin/stocks/category/update',
              data: {
                  //_token:$(this).data('token'),
                  '_token': $('input[name=_token]').val(),
                  'id': $("#fid").val(),
                  'catcode': $('#editCatCode').val(),
                  'catname': $('#editCatName').val() 
              },
              success: function(data) {
                  $('.item' + data.id).replaceWith("<tr class='item" + data.id + "'><td>"+ data.catcode +"</td><td>"+ data.catname +"</td><td><button class='btn btn-success btn-sm edit-modal' data-id='" + data.id + "' data-catcode='" + data.catcode + "' data-catname='" + data.catname + "'><i class='fa fa-pencil'> </i></button></td></tr>");
                  //console.log("success");
                    new PNotify({
                        title: 'Success',
                        text: 'Category successfully updated',
                        type: 'info',
                        delay: 2000,
                        styling: 'bootstrap3'
                    }); 
                }
                
          });
      });
      $("#btnsave").click(function() {
  
          $.ajax({
              type: 'post',
              url: '/admin/stocks/category/add',
              data: {
                  '_token': $('input[name=_token]').val(),
                  'catcode': $('input[name=catcode]').val(),
                  'catname': $('input[name=catname]').val()   
              },
              success: function(data) {
                      $('#categorytable').append("<tr class='item" + data.id + "'><td> <a href='/admin/stocks/category/"+data.catcode+"'>"+ data.catcode +"</a></td><td><a href='/admin/stocks/category/"+data.catcode+"'>"+ data.catname +"</a></td><td><button class='edit-modal btn btn-small btn-success' data-id='" + data.id + "' data-catname='" + data.catname + "' data-catcode='" + data.catcode + "'><i class='fa fa-edit'></i></button><a class='delete-modal btn btn-danger btn-small' data-id='" + data.id + "'><i class='fa fa-times'></i></a></td></tr>");
                      new PNotify({
                        title: 'Success',
                        text: 'Category successfully added',
                        type: 'success',
                        delay: 2000,
                        styling: 'bootstrap3'
                    }); 
              }
          });
          $('#catcode').val('');
          $('#catname').val('');
      });
      $('.modal-footer').on('click', '.delete', function() {
          $.ajax({
              type: 'post',
              url: '/admin/stocks/category/delete',
              data: {
                  '_token': $('input[name=_token]').val(),
                  'id': $('#delid').val()
              },
              success: function(data) {
                new PNotify({
                    title: 'Success',
                    text: 'Category successfully deleted',
                    type: 'danger',
                    delay: 2000,
                    styling: 'bootstrap3'
                });  
                $('.item' + $('#delid').val()).remove();
              }
          });
      });
  });
  