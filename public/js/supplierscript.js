$(document).ready(function() {
    var toValidate = $('#suppliername'),
    valid = false;
    toValidate.keyup(function () {
        if ($(this).val().length > 0) {
            $(this).data('valid', true);
        } else {
            $(this).data('valid', false);
        }
        toValidate.each(function () {
            if ($(this).data('valid') == true) {
                valid = true;
            } else {
                valid = false;
            }
        });
        if (valid === true) {
            $('#btnsave').prop('disabled', false);
        }else{
            $('#btnsave').prop('disabled', true);        
        }
    });

    $(document).on('click', '.edit-modal', function() {
          $('#fid').val($(this).data('id'));
          $('#editSupplierName').val($(this).data('suppliername'));
          $('#supplierModal').modal('show');
      });
      $(document).on('click', '.delete-modal', function() {
          $('#delid').val($(this).data('id'));
          $('#deleteModal').modal('show');
      });
  
      $('.modal-footer').on('click', '.editSave', function() {
  
          $.ajax({
              type: 'post',
              url: '/admin/supplier/update',
              data: {
                  //_token:$(this).data('token'),
                  '_token': $('input[name=_token]').val(),
                  'id': $("#fid").val(),
                  'suppliername': $('#editSupplierName').val()
              },
              success: function(data) {
                  $('.item' + data.id).replaceWith("<tr class='item" + data.id + "'><td>"+ data.suppliername +"</td><td><button class='btn btn-success btn-sm edit-modal' data-id='" + data.id + "' data-suppliername='" + data.suppliername + "'><i class='fa fa-pencil'> </i></button></td></tr>");
                  //console.log("success");
                    new PNotify({
                        title: 'Success',
                        text: 'Vehicle successfully updated',
                        type: 'info',
                        delay: 2000,
                        styling: 'bootstrap3'
                    }); 
                }
                
          });
      });
      $("#btnsave").click(function() {
  
          $.ajax({
              type: 'post',
              url: '/admin/supplier/add',
              data: {
                  '_token': $('input[name=_token]').val(),
                  'suppliername': $('input[name=suppliername]').val()   
              },
              success: function(data) {
                      $('#suppliertable').append("<tr class='item" + data.id + "'><td> <a href='/admin/supplier/"+data.id+"'>"+ data.suppliername +"</a></td><td><button class='edit-modal btn btn-small btn-success' data-id='" + data.id + "' data-suppliername='" + data.suppliername + "'><i class='fa fa-edit'></i></button><a class='delete-modal btn btn-danger btn-small' data-id='" + data.id + "'><i class='fa fa-times'></i></a></td></tr>");
                      new PNotify({
                        title: 'Success',
                        text: 'Supplier successfully added',
                        type: 'success',
                        delay: 2000,
                        styling: 'bootstrap3'
                    }); 
              }
          });
          $('#suppliername').val('');
      });
      $('.modal-footer').on('click', '.delete', function() {
          $.ajax({
              type: 'post',
              url: '/admin/supplier/delete',
              data: {
                  '_token': $('input[name=_token]').val(),
                  'id': $('#delid').val()
              },
              success: function(data) {
                new PNotify({
                    title: 'Success',
                    text: 'Supplier successfully deleted',
                    type: 'danger',
                    delay: 2000,
                    styling: 'bootstrap3'
                });  
                $('.item' + $('#delid').val()).remove();
              }
          });
      });
  });
  