$(document).ready(function() {
  
    $(document).on('click', '.add-modal', function() {
          $('#footer_action_button').text("Update");
          $('#footer_action_button').addClass('glyphicon-check');
          $('#footer_action_button').removeClass('glyphicon-trash');
          $('.actionBtn').addClass('btn-success');
          $('.actionBtn').removeClass('btn-danger');
          $('.actionBtn').addClass('edit');
          $('.modal-title').text('Return Stock');
          $('.deleteContent').hide();
          $('.form-horizontal').show();
          $('#fid').val($(this).data('id'));
          $('#stocknum').val($(this).data('stocknumber'));
          $('#stock_id').val($(this).data('stockid'));
          $('#stockname').val($(this).data('stockname'));
          $('#catcode').val($(this).data('catcode'));
          $('#onhandQuantity').val($(this).data('quantity'));
         
          $('#stockModal').modal('show');
          //console.log($(this).data('name') + $(this).data('points'));
      });
    $(document).on('click', '.delete-modal', function() {

        $('.actionBtn').addClass('btn-success');
        $('.actionBtn').removeClass('btn-danger');
        $('.modal-title').text('Remove Request');
        $('#fid').val($(this).data('id'));
        $('#deleteModal').modal('show');
        //console.log($(this).data('name') + $(this).data('points'));
    });

    $(document).on('click', '.confirm-modal', function() {
        $('.modal-title').text('Process Return Material Slip');
        $('#return_number_confirm').val($(this).data('return_number'));
        $('#confirmModal').modal('show');

    });
    $('.modal-footer').on('click', '.confirmProcess', function() {
        
        $.ajax({
            type: 'post',
            url: '/admin/rms/process',
            data: {
                '_token': $('input[name=_token]').val(),
                'return_number': $('#return_number_confirm').val()
            },
            success: function(data) {
                window.location.href = '/admin/rms/view/' + data.return_number; 
            }
              
        });
    });
    
    $('.modal-footer').on('click', '.save_rms', function() {
  
          $.ajax({
              type: 'post',
              url: '/admin/rmsitems/add',
              data: {
                  //_token:$(this).data('token'),
                  '_token': $('input[name=_token]').val(),
                  'stocknumber': $('#stocknum').val(),
                  'stock_id': $('#stock_id').val(),
                  'stockname': $('#stockname').val(),
                  'catcode': $('#catcode').val(),
                  'return_quantity': $('#return_quantity').val(),
                  'return_number': $('#return_number').val()
              },
              success: function(data) {

            $('#rmsdetails').append("<tr class='item" + data.id + "'><td>"+ data.cat_number +"</td><td>"+ data.stock_id +"</td><td>" + data.stock_name + "</td><td>"+ data.quantity + "</td><td class='td-actions'><a class='delete-modal btn btn-danger btn-small' data-id='" + data.id + "'><i class='fa fa-times'></i></a></td></tr>");
                    new PNotify({
                        title: 'Success',
                        text: 'Stock successfully added',
                        type: 'info',
                        delay: 2000,
                        styling: 'bootstrap3'
                    }); 
                }     
          });
          $('.norecordriv').remove();
      });
      

      $('.modal-footer').on('click', '.delete', function() {
          $.ajax({
              type: 'post',
              url: '/admin/rmsitems/delete',
              data: {
                  '_token': $('input[name=_token]').val(),
                  'id': $('#fid').val()
              },
              success: function(data) {
                  console.log(data);
                new PNotify({
                    title: 'Success',
                    text: 'Item successfully deleted',
                    type: 'danger',
                    delay: 2000,
                    styling: 'bootstrap3'
                });  
                $('.item' + data).remove();
              }
          });
      });
      
  });
  