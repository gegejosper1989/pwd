$(document).ready(function() {

    $(document).on('click', '.edit-modal', function() {
          $('#footer_action_button').text("Update");
          $('#footer_action_button').addClass('glyphicon-check');
          $('#footer_action_button').removeClass('glyphicon-trash');
          $('.actionBtn').addClass('btn-success');
          $('.actionBtn').removeClass('btn-danger');
          $('.actionBtn').addClass('edit');
          $('.modal-title').text('Edit');
          $('.deleteContent').hide();
          $('.form-horizontal').show();
          $('#fid').val($(this).data('id'));
          $('#editStockNum').val($(this).data('stocknumber'));
          $('#editDescription').val($(this).data('stockname'));
          $('#editCatVal').val($(this).data('catcode'));
          $('#editCatVal').text($(this).data('catname'));
          $('editCat').append($('<option>').val($(this).data('catcode')).text($(this).data('catcode')))
          //$('#editCatVal').text($(this).data('catcode'));
          $('#editUnitVal').val($(this).data('unit'));
          $('#editUnitVal').text($(this).data('unit'));
          $('#editQuantity').val($(this).data('quantity'));
          $('#editOrdering').val($(this).data('ordering'));
          $('#stockModal').modal('show');
          //console.log($(this).data('name') + $(this).data('points'));
      });
      var toValidate = $('#editOrdering'),
      valid = false;
      if ($('#editOrdering').length < 0 ) {
          $('#saveEdit').prop('disabled', false);
      }else{
          $('#saveEdit').prop('disabled', true);        
      }
      toValidate.keyup(function () {
          if ($(this).val().length > 0) {
              $(this).data('valid', true);
          } else {
              $(this).data('valid', false);
          }
          toValidate.each(function () {
              if ($(this).data('valid') == true) {
                  valid = true;
              } else {
                  valid = false;
              }
          });
          if (valid === true) {
              $('#saveEdit').prop('disabled', false);
          }else{
              $('#saveEdit').prop('disabled', true);        
          }
      });
      $('.modal-footer').on('click', '.saveEdit', function() {
  
          $.ajax({
              type: 'post',
              url: '/admin/stocks/edit',
              data: {
                  //_token:$(this).data('token'),
                  '_token': $('input[name=_token]').val(),
                  'id': $("#fid").val(),
                  'stocknum': $('#editStockNum').val(),
                  'description': $('#editDescription').val(),
                  'quantity': $('#editQuantity').val(),
                  'category': $('#editCat').val(),
                  'unit': $('#editUnit').val(),
                  'orderingpoint': $('#editOrdering').val() 
              },
              success: function(data) {
                  console.log(data);
                $('.row' + data[0].id).replaceWith(`
                  <tr class="row${data[0].id}">
                    <td><a href="/admin/category/${data[0].id}">${data[0].catcode}</a></td>
                    <td><a href="/admin/stock/${data[0].id}">${data[0].stocknumber}</a></td>
                    <td>${data[0].stockname}</td>
                    <td>${data[0].quantity}</td>
                    <td>${data[0].unit}</td>
                    <td>${data[0].orderingpoint}</td>
                    <td><button class="btn edit-modal btn-success btn-sm" 
                        data-id="${data[0].id}"
                        data-catcode="${data[0].catcode}"
                        data-stocknumber="${data[0].stocknumber}"
                        data-stockname="${data[0].stockname}"
                        data-quantity="${data[0].quantity}"
                        data-unit="${data[0].unit}"
                        data-ordering="${data[0].orderingpoint}"
                        ><i class="fa fa-edit"></i>Edit</button>
                        <a href="/admin/bincard/${data[0].id}" class="btn btn-info btn-sm"><i class="fa fa-folder"></i> Bin Card</a> 
                        <a href="/admin/stock/${data[0].id}" class="btn btn-warning btn-sm"><i class="fa fa-search"></i> View Stock</a>
                    </td>
                    </tr>`);
                    new PNotify({
                        title: 'Success',
                        text: 'Stock successfully updated',
                        type: 'info',
                        delay: 2000,
                        styling: 'bootstrap3'
                    }); 
                }
                
          });
      });
      $("#stocksave").click(function() {
  
          $.ajax({
              type: 'post',
              url: '/admin/stocks/add',
              data: {
                  '_token': $('input[name=_token]').val(),
                  'stockcat': $('select[name=stockcat]').val(),
                  'stocknumber': $('input[name=stocknumber]').val(),
                  'description': $('input[name=description]').val(),
                  'orderingpoint': $('input[name=orderingpoint]').val(),
                  'quantity': $('input[name=quantity]').val(),
                  'unit': $('select[name=unit]').val()
                  
              },
              success: function(data) {
                console.log(data);
                $('#tablestocks').append(`
                    <tr class='row${data.id}'>
                    <td>${data.catcode}</td>
                    <td>${data.stocknumber}</td>
                    <td>${data.stockname}</td>
                    <td>${data.quantity}</td>
                    <td>${data.unit}</td>
                    <td>${data.orderingpoint}</td>
                    <td>
                    <button class='btn edit-modal btn-success btn-sm' 
                    data-id="${data.id}"
                    data-catcode="${data.catcode}"
                    data-stocknumber="${data.stocknumber}"
                    data-stockname="${data.stockname}"
                    data-quantity="${data.quantity}"
                    data-unit="${data.unit}"
                    data-ordering="${data.orderingpoint}"
                    ><i class="fa fa-edit"></i>Edit</button>
                    <a href="/admin/bincard/${data.id}" class="btn btn-info btn-sm"><i class="fa fa-folder"></i> Bin Card</a> 
                    <a href="/admin/stock/${data.id}" class="btn btn-warning btn-sm"><i class="fa fa-search"></i> View Stock</a> </td></tr>`);
                new PNotify({
                title: 'Success',
                text: 'Stock successfully added',
                type: 'success',
                delay: 2000,
                styling: 'bootstrap3'
                });         
              },
  
          });
          $('#stocknumber').val('');
          $('#description').val('');
          $('#orderingpoint').val('');
          $('#quantity').val('');
      });
      $('.modal-footer').on('click', '.delete', function() {
          $.ajax({
              type: 'post',
              url: '/admin/branches/users/delete',
              data: {
                  '_token': $('input[name=_token]').val(),
                  'id': $('.did').text()
              },
              success: function(data) {
                new PNotify({
                    title: 'Success',
                    text: 'User successfully deleted',
                    type: 'danger',
                    delay: 2000,
                    styling: 'bootstrap3'
                });  
                $('.item' + $('.did').text()).remove();
              }
          });
      });
  });
  