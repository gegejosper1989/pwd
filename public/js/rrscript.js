$(document).ready(function() {
  
    $(document).on('click', '.add_pr', function() {
        $('#pr_number').val($(this).data('prnum'));
    });

    var toValidate = $('#unit_cost, #rec_quantity'),
    valid = false;
    toValidate.keyup(function () {
        if ($(this).val().length > 0) {
            $(this).data('valid', true);
        } else {
            $(this).data('valid', false);
        }
        toValidate.each(function () {
            if ($(this).data('valid') == true) {
                valid = true;
            } else {
                valid = false;
            }
        });
        if (valid === true) {
            $('#saveRR').prop('disabled', false);
        }else{
            $('#saveRR').prop('disabled', true);        
        }
    });

    $(document).on('click', '.add-modal', function() {
        $('#footer_action_button').text("Update");
        $('#footer_action_button').addClass('glyphicon-check');
        $('#footer_action_button').removeClass('glyphicon-trash');
        $('.actionBtn').addClass('btn-success');
        $('.actionBtn').removeClass('btn-danger');
        $('.actionBtn').addClass('edit');
        $('.modal-title').text('Add Request');
        $('.deleteContent').hide();
        $('.form-horizontal').show();
        $('#fid').val($(this).data('id'));
        $('#stocknum').val($(this).data('stocknumber'));
        $('#stockid').val($(this).data('stockid'));
        $('#description').val($(this).data('stockname'));
        $('#catcode').val($(this).data('catcode'));
        $('#onhandQuantity').val($(this).data('quantity'));
       
        $('#stockModal').modal('show');
        //console.log($(this).data('name') + $(this).data('points'));
    });

    $(document).on('click', '.cons-delete-modal', function() {

        $('.actionBtn').addClass('btn-success');
        $('.actionBtn').removeClass('btn-danger');
        $('.modal-title').text('Remove Concessionaire');
        $('#consid').val($(this).data('id'));
        $('#consdeleteModal').modal('show');
        //console.log($(this).data('name') + $(this).data('points'));
    });
    $(document).on('click', '.delete-modal', function() {

        $('.actionBtn').addClass('btn-success');
        $('.actionBtn').removeClass('btn-danger');
        $('.modal-title').text('Remove Request');
        $('#fid').val($(this).data('id'));
        $('#deleteModal').modal('show');
        //console.log($(this).data('name') + $(this).data('points'));
    });

    $(document).on('click', '.confirm-modal', function() {
        $('#revid').val($(this).data('rivnumber'));
        $('#confirmModal').modal('show');

    });
    $('.modal-footer').on('click', '.confirmProcess', function() {
  
        $.ajax({
            type: 'post',
            url: '/admin/riv/process',
            data: {
                '_token': $('input[name=_token]').val(),
                'rivnumber': $('#revid').val()
            },
            success: function(data) {
                window.location.href = '/admin/riv/view/' + data.rivnumber; 
            }
              
        });
    });
    
    $('.modal-footer').on('click', '.saveRR', function() {
  
          $.ajax({
              type: 'post',
              url: '/admin/rr_items/add',
              data: {
                  //_token:$(this).data('token'),
                  '_token': $('input[name=_token]').val(),
                  'stocknumber': $('#stocknum').val(),
                  'stockid': $('#stockid').val(),
                  'description': $('#description').val(),
                  'onhandquantity': $('#onhandQuantity').val(),
                  'catcode': $('#catcode').val(),
                  'rec_quantity': $('#rec_quantity').val(),
                  'rr_number': $('#rr_number').val(),
                  'order_type': $('#order_type').val(),
                  'unit_cost': $('#unit_cost').val()
              },
              success: function(data) {

            $('#rr_details').append("<tr class='item" + data.id + "'><td>"+ data.catcode +"</td><td>"+ data.stockid +"</td><td>" + data.description + "</td><td>"+ data.unitcost + "</td><td>"+ data.quantity + "</td><td class='td-actions'><a class='delete-modal btn btn-danger btn-small' data-id='" + data.id + "'><i class='fa fa-times'></i></a></td></tr>");
                    new PNotify({
                        title: 'Success',
                        text: 'Stock successfully added',
                        type: 'info',
                        delay: 2000,
                        styling: 'bootstrap3'
                    }); 
                }
                
          });
          $('.norecordriv').remove();
      });

      $('.modal-footer').on('click', '.delete', function() {
          $.ajax({
              type: 'post',
              url: '/admin/rr_items/delete',
              data: {
                  '_token': $('input[name=_token]').val(),
                  'id': $('#fid').val()
              },
              success: function(data) {
                  console.log(data);
                new PNotify({
                    title: 'Success',
                    text: 'Item successfully deleted',
                    type: 'danger',
                    delay: 2000,
                    styling: 'bootstrap3'
                });  
                $('.item' + data).remove();
              }
          });
      });
      
});
  