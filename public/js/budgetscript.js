$(document).ready(function() {
  
    $(document).on('click', '.edit-modal', function() {
          $('#fid').val($(this).data('id'));
          $('#stockid').val($(this).data('stockid'));
          $('#StockNum').val($(this).data('stocknumber'));
          $('#Description').val($(this).data('stockname'));
          $('#department').val($(this).data('department'));
          $('#editconsumed').val($(this).data('consumed'));
          $('#editbudget').val($(this).data('budget'));
          $('#catcode').val($(this).data('catcode'));
          $('#stockModal').modal('show');
          //console.log($(this).data('name') + $(this).data('points'));
      });
     
      $('.modal-footer').on('click', '.saveEdit', function() {
  
          $.ajax({
              type: 'post',
              url: '/admin/budget/update',
              data: {
                  //_token:$(this).data('token'),
                  '_token': $('input[name=_token]').val(),
                  'id': $("#fid").val(),
                  'stockid': $("#stockid").val(),
                  'catcode': $('#catcode').val(),
                  'stocknumber': $('#StockNum').val(),
                  'stockname': $('#Description').val(),
                  'consumed': $('#editconsumed').val(),
                  'department': $('#department').val(),
                  'budget': $('#editbudget').val() 
              },
              success: function(data) {
                  console.log(data);
                $('.row' + data[0].id).replaceWith(`
                  <tr class="row${data[0].id}">
                    <td><a href="/admin/category/${data[0].catcode}">${data[0].catcode}</a></td>
                    <td><a href="/admin/stock/${data[0].stockid}">${data[0].stocknumber}</a></td>
                    <td>${data[0].stockname}</td>
                    <td>${data[0].department}</td>
                    <td>${data[0].budget}</td>
                    <td>${data[0].consumed}</td>
                    <td><button class="btn edit-modal btn-success btn-sm" 
                        data-id="${data[0].id}"
                        data-catcode="${data[0].catcode}"
                        data-stocknumber="${data[0].stocknumber}"
                        data-stockid="${data[0].stockid}"
                        data-stockname="${data[0].stockname}"
                        data-consumed="${data[0].consumed}"
                        data-department="${data[0].department}"
                        data-budget="${data[0].budget}"
                        ><i class="fa fa-edit"></i></button>
                    </td>
                    </tr>`);
                    new PNotify({
                        title: 'Success',
                        text: 'Budget successfully updated',
                        type: 'info',
                        delay: 2000,
                        styling: 'bootstrap3'
                    }); 
                }
                
          });
      });

  });
  