$(document).ready(function() {
  
    $(document).on('click', '.add-modal', function() {
          $('#footer_action_button').text("Update");
          $('#footer_action_button').addClass('glyphicon-check');
          $('#footer_action_button').removeClass('glyphicon-trash');
          $('.actionBtn').addClass('btn-success');
          $('.actionBtn').removeClass('btn-danger');
          $('.actionBtn').addClass('edit');
          $('.modal-title').text('Add Request');
          $('.deleteContent').hide();
          $('.form-horizontal').show();
          $('#fid').val($(this).data('id'));
          $('#stocknum').val($(this).data('stocknumber'));
          $('#stockid').val($(this).data('stockid'));
          //$('#stockid').val($(this).data('stockid'));
          $('#description').val($(this).data('stockname'));
          $('#catcode').val($(this).data('catcode'));
          $('#onhandQuantity').val($(this).data('quantity'));
         
          $('#stockModal').modal('show');
          //console.log($(this).data('name') + $(this).data('points'));
      });

    $(document).on('click', '.delete-modal', function() {

        $('.actionBtn').addClass('btn-success');
        $('.actionBtn').removeClass('btn-danger');
        $('.modal-title').text('Remove Request');
        $('#fid').val($(this).data('id'));
        $('#deleteModal').modal('show');
        //console.log($(this).data('name') + $(this).data('points'));
    });

    $(document).on('click', '.confirm-modal', function() {
        $('#prnum').val($(this).data('prnum'));
        $('#confirmModal').modal('show');

    });
    $('.modal-footer').on('click', '.confirmProcess', function() {
  
        $.ajax({
            type: 'post',
            url: '/admin/pr/process',
            data: {
                '_token': $('input[name=_token]').val(),
                'prnum': $('#prnum').val()
            },
            success: function(data) {
                window.location.href = '/admin/pr/view/' + data.prnum; 
            }
              
        });
    });
    
    $('.modal-footer').on('click', '.saveRiv', function() {
  
          $.ajax({
              type: 'post',
              url: '/admin/pritems/add',
              data: {
                  //_token:$(this).data('token'),
                  '_token': $('input[name=_token]').val(),
                  'stocknumber': $('#stocknum').val(),
                  'stockid': $('#stockid').val(),
                  'description': $('#description').val(),
                  'stockquantity': $('#stockquantity').val(),
                  'catcode': $('#catcode').val(),
                  'reqquantity': $('#addQuantity').val(),
                  'prnum': $('#prnumberquantity').val()
              },
              success: function(data) {

            $('#prdetails').append("<tr class='item" + data.id + "'><td>"+ data.catnumber +"</td><td>"+ data.stockid +"</td><td>"+ data.description +"</td><td>"+ data.reqquantity + "</td><td class='td-actions'><a class='delete-modal btn btn-danger btn-small' data-id='" + data.id + "'><i class='fa fa-times'></i></a></td></tr>");
                    new PNotify({
                        title: 'Success',
                        text: 'Item successfully added',
                        type: 'info',
                        delay: 2000,
                        styling: 'bootstrap3'
                    }); 
                }
                
          });
          $('.norecordriv').remove();
      });

      $('.modal-footer').on('click', '.delete', function() {
          $.ajax({
              type: 'post',
              url: '/admin/pritems/delete',
              data: {
                  '_token': $('input[name=_token]').val(),
                  'id': $('#fid').val()
              },
              success: function(data) {
                  console.log(data);
                new PNotify({
                    title: 'Success',
                    text: 'Item successfully deleted',
                    type: 'danger',
                    delay: 2000,
                    styling: 'bootstrap3'
                });  
                $('.item' + data).remove();
              }
          });
      });
  });
  