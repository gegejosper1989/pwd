@extends('layouts.admin')

@section('content')

<div class="right_col" role="main">
<div class="row">
        <div class="col-md-12 col-sm-12 col-lg-12">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>Purchase Request Details</h4>
                </div>
                <div class="col-md-4 col-sm-12 col-lg-3">
                <label for="Date">Date</label>
                <input class="form-control" name="text" id="today" value="{{$DetailPr->prdate}}" readonly>
                </div>
                <div class="col-md-4 col-sm-12 col-lg-3">
                <label for="PR Number">PR Number</label>
                <input class="form-control" type="text" name="prnumber" value="{{$DetailPr->prnum}}" readonly>
                </div>
                <div class="col-md-4 col-sm-12 col-lg-3">
                <label for="Supplier">Supplier</label>
                <input class="form-control" type="text" name="supplier" value="{{$DetailPr->supplier->suppliername}}" readonly>
                </div>
                <div class="col-md-4 col-sm-12 col-lg-3">
                <label for="Payment Mode">Payment Mode</label>
                <input class="form-control" type="text" name="paymentmode" value="{{$DetailPr->paymentmode}}" readonly>
                </div>
                <div class="clearfix"></div>
            <br>
           
            </div>
        </div><!--col-->
    </div><!--row-->
    <div class="row">
        <div class="col-md-6 col-sm-12 col-lg-5">
            <div class="x_panel tile">
                <h5>Stock Search</h5>

                <input type="text" id="search" class="form-control" placeholder="Search Stock...">  
                <div class="input-group">
                        
                    </div>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                <th>Category </th>
                                <th>SN</th>
                                <th>Description</th>
                                <th>Qty</th>
                            
                                <th></th>
                                </tr>
                            </thead>
                            <tbody class="stockbinresult">
                            @foreach($dataStock as $Stock)
                                <tr>
                                    <td><a href="/admin/stocks/category/{{$Stock->category->catcode}}">{{$Stock->category->catcode}}</a></td>
                                    <td><a href="/admin/stock/{{$Stock->id}}">{{$Stock->stocknumber}}</a></td>
                                    <td>{{$Stock->stockname}}</td>
                                    <td>{{$Stock->quantity}}</td>
                                    <td>
                                    
                                        <a href="javascript:;" 
                                            class="add-modal btn btn-sm btn-success" 
                                            data-stocknumber="{{$Stock->stocknumber}}"
                                            data-stockid="{{$Stock->id}}"
                                            data-catcode="{{$Stock->category->catcode}}" 
                                            data-stockname="{{$Stock->stockname}}" 
                                            data-quantity="{{$Stock->quantity}}" 
                                            data-id="{{$Stock->id}}" >
                                            <i class="fa fa-plus"> 
                                        </i></a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
            
            </div>
        </div><!--col-->
        <div class="col-md-6 col-sm-12 col-lg-7">
            <div class="x_panel tile">
                <table class="table table-striped" id="prdetails">
                <thead>
                    <tr>
                    <th>Category </th>
                    <th>Stock No</th>
                    <th>Description</th>
                    <th>Request Quantity</th>
                    <th></th>
                    </tr>
                </thead>
                <tbody>
                   
                    @forelse($dataPrDetail as $prDetail)
                        <tr class="item{{$prDetail->id}}">
                            <td>{{$prDetail->catnumber}}</td>
                            <td>{{$prDetail->stock->stocknumber}}</td>
                            <td>{{$prDetail->stock->stockname}}</td>
                            <td>{{$prDetail->reqquantity}}</td>
                            <td><a class='delete-modal btn btn-danger btn-small' data-id='{{$prDetail->id}}'>
                                <i class='fa fa-times'></i></a>
                            </td>
                        </tr>
                    @empty
                    <tr class="norecordriv"><td colspan="6" align="center"> <em>No Record</em></td></tr>
                    @endforelse

                </tbody>
            </table>
            <a href="/admin/pr/process/{{$DetailPr->prnum}}" class="btn btn-sm btn-success pull-right"> <i class="fa fa-file-text"></i> Save Purchase Request</a>
                <div class="clearfix"></div>
            </div>
    </div><!--row-->
</div>

<div id="stockModal" class="modal fade bs-example-modal-lg" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
               
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
              
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">
                    {{ csrf_field() }}
                    <input type="hidden" class="form-control" id="id">
                    <div class="row">
                        <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <label class="control-label" for="Stock No.">Stock No.</label>
                            <input type="text" class="form-control" name="stocknum" id="stocknum" readonly> 
                            <input type="hidden" class="form-control" name="stockid" id="stockid" readonly> 
                        </div>
                        <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <label class="control-label" for="Description">Description</label>
                            <input type="text" class="form-control" name="description" id="description" readonly> 
                        </div>
                        <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <label class="control-label" for="Quantity">Stock Quantity</label>
                            <input type="text" class="form-control" name="stockquantity" id="stockquantity" readonly> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <label class="control-label">Category</label>
                            <input type="text" class="form-control" name="catcode" id="catcode" readonly> 
                        </div>
                        
                        <div class="form-group col-md-8 col-lg-8 col-sm-12 col-xs-12">
                            <label class="control-label">Quantity</label>
                            <input type="number" class="form-control" name="addQuantity" id="addQuantity"> 
                            <input type="hidden" class="form-control" name="prnumberquantity" id="prnumberquantity" value="{{$DetailPr->prnum}}"> 
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn saveRiv btn-primary" data-dismiss="modal" id="rivAdd">Add</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="deleteModal" class="modal fade bs-example-modal-lg" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
               
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel"></h4>
              
            </div>
            <div class="modal-body">
            <p>
            Are you sure you want to remove this item?
            </p>
            <form class="form-horizontal" role="form">
                {{ csrf_field() }}
                <input type="hidden" class="form-control" id="fid">
                
            </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn delete btn-danger" data-dismiss="modal">Remove</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="confirmModal" class="modal fade bs-example-modal-lg" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
               
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel"> Process RIV</h4>
              
            </div>
            <div class="modal-body">
            <p>
            Are you sure you want to process this RIV?
            </p>
            <form class="form-horizontal" role="form">
                {{ csrf_field() }}
                <input type="hidden" class="form-control" id="revid">
                
            </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn confirmProcess btn-success" data-dismiss="modal">Process</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="consdeleteModal" class="modal fade bs-example-modal-lg" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
               
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
              
            </div>
            <div class="modal-body">
            <p>
            Are you sure you want to remove this concessionaire?
            </p>
            <form class="form-horizontal" role="form">
                    {{ csrf_field() }}
                    <input type="hidden" class="form-control" id="consid">
                 
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn delete-cons btn-danger" data-dismiss="modal">Remove</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$('#search').on('keyup',function(){
  $value=$(this).val();
  $department = $('#searchdepartment').val();
  $.ajax({
    type : 'get',
    url : '{{URL::to('admin/prstocksearch')}}',
    data:{'search':$value
    },
    success:function(data){
      $('.stockbinresult').html(data);
    } 
  });
})


// $('#addQuantity').on('keyup',function(){
//     $quantity=$(this).val();
//     $onhand = $('#onhandQuantity').val();
//     if ($quantity <= $onhand ) {
//         $('#rivAdd').prop('disabled', false);
//     }else{
//         $('#rivAdd').prop('disabled', true);        
//     }
//   console.log($quantity +' + ' + $onhand);
// })
</script> 
<script type="text/javascript">
$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/prscript.js') }}"></script>
@endsection