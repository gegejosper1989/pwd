@extends('layouts.admin')

@section('content')

<div class="right_col" role="main">
    <div class="row">
    <div class="col-md-6 col-sm-12 col-lg-4">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>Bin Card </h4>
                </div>
               
                    <div class="input-group">
                        <input type="text" id="search" class="form-control" placeholder="Search Stock...">  
                    </div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th>Category </th>
                        <th>Stock No</th>
                        <th>Description</th>
                        
                        <th></th>
                        </tr>
                    </thead>
                    <tbody class="stockbinresult">
                    @foreach($dataStock as $Stock)
                        <tr>
                            <td><a href="/admin/category/{{$Stock->id}}">{{$Stock->catcode}}</a></td>
                            <td><a href="/admin/stock/{{$Stock->id}}">{{$Stock->stocknumber}}</a></td>
                            <td>{{$Stock->stockname}}</td>
                           
                            <td>
                                <a href="/admin/bincard/{{$Stock->id}}" class="btn btn-info btn-sm"><i class="fa fa-search"></i></a>                                
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            
            </div>
    </div><!--col-->
        <div class="col-md-6 col-sm-12 col-lg-8">
            <div class="x_panel tile">


            
            
            <div class="x_content">
            
            </div><!--x_content-->
        </div><!--col-->
    </div><!--row-->
</div>
<script type="text/javascript">
$('#search').on('keyup',function(){
  $value=$(this).val();
  $.ajax({
    type : 'get',
    url : '{{URL::to('admin/bincardstocksearch')}}',
    data:{'search':$value},
    success:function(data){
      $('.stockbinresult').html(data);
    } 
  });
})
</script> 
<script type="text/javascript">
$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>
<script src="{{ asset('js/app.js') }}"></script>
@endsection