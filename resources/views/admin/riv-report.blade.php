@extends('layouts.admin')

@section('content')

<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-lg-12">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>List of Request and Issue Voucher</h4>
                </div>

                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th>Date </th>
                        <th>RIV No.</th>
                        <th>Department</th>
                        
                        <th>Purpose</th>
                        <th>Work Order</th>
                        <th>Vehicle Number</th>
                        <th>Status</th>
                        </tr>
                    </thead>
                    <tbody class="rivresult">
                        @foreach($dataRecentRiv as $rivdetails)
                            <tr>
                                <td><a href="/admin/riv/view/{{$rivdetails->id}}">{{$rivdetails->date}}</a></td>
                                <td><a href="/admin/riv/view/{{$rivdetails->id}}">{{$rivdetails->rivnumber}}</a></td>
                                <td>{{$rivdetails->department}}</td>
                                <td>{{$rivdetails->purpose}}</td>
                                <td>{{$rivdetails->workorder}}</td>
                                <td>{{$rivdetails->vehiclenum}}</td>
                                <td>{{$rivdetails->status}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
               {{$dataRecentRiv->links()}}
            </div>
        </div>

    </div><!--row-->
</div>


@endsection