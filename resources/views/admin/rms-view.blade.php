@extends('layouts.admin')

@section('content')

<div class="right_col" role="main">
<div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
            <div class="title-header text-center" >
                <h3>PAGADIAN CITY WATER DISTRICT</h3>
                <p>Datoc St. Gatas Dist., Pagadian City</p>
                <p>Tel No. 2141-747-2144 Fax No. 2142-179</p>
                <h4>RETURN MATERIAL SLIP</h4> <br>
            </div>
            <div class="col-xs-8">
            <label for="">Department : </label> {{$return_info->department}} <br>
            </div>
            
            <div class="col-xs-4">
                <label for="">RMS No.</label> {{$return_info->return_number}} <br>
                <label for="">Ref SRS No.</label> {{$return_info->srs_num}} <br>
                <label for="Date">Date : </label> {{$return_info->return_date}} <br>    
            </div>
        </div><!--col-->
    </div><!--row-->
    <div class="row">
        <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
            <div class="x_panel tile">
                <table class="table table-bordered" id="rmsdetails">
                <thead>
                    <tr>
                    <th>Description</th>
                    <th>QTY.</th> 
                    <th>Unit</th>
                    <th>Unit Price</th>
                    <th>Amount</th>
                    <th>Acc. No.</th>
                    <th>Location/Owner</th> 
                    </tr>
                </thead>
                <tbody>                  
                    @forelse($return_detail as $return)
                        <tr class="item{{$return->id}}">
                            <td>{{$return->stock->stockname}}</td>
                            <td>{{$return->quantity}}</td>
                            <td>{{$return->stock->unit}}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    @empty
                    <tr class="norecordriv"><td colspan="6" align="center"> <em>No Record</em></td></tr>
                    @endforelse
                    <tr>
                        <td colspan="7"> Reason for return : {{$return_info->reason}}</td>
                    </tr>
                    <tr>
                        <td>Returned by:  
                            <br><br><br>
                            {{$return_info->employee->lname}}, {{$return_info->employee->fname}} {{$return_info->employee->mname}}
                        </td>
                        <td colspan="2">Approved by:  
                            <br><br><br>
                            {{$data_rms->employee_approved->fname}} {{$data_rms->employee_approved->mname}} {{$data_rms->employee_approved->lname}}
                        </td>
                        <td colspan="2">Approved for Return:  
                            <br><br><br>
                            {{$data_rms->employee_approved_return->fname}} {{$data_rms->employee_approved_return->mname}} {{$data_rms->employee_approved_return->lname}}
                        </td>
                        <td colspan="2">
                            RMS NO.
                        </td>
                    </tr>
                    <tr>
                        <td>Requisitioner
                        </td>
                        <td colspan="2">{{$data_rms->employee_approved->position}}
                        </td>
                        <td colspan="2">{{$data_rms->employee_approved_return->position}}
                        </td>
                        <td colspan="2">
                        </td>
                    </tr>
                    <tr>
                        <td>Date:</td>
                        <td colspan="2">Date:</td>
                        <td colspan="2">Date:</td>
                        <td colspan="2">
                        </td>
                    </tr>
                    <tr>
                        <td>Received/Posted to Bin Card:  
                            <br><br><br>
                            {{$data_rms->employee_posted_bin_card->fname}} {{$data_rms->employee_posted_bin_card->mname}} {{$data_rms->employee_posted_bin_card->lname}}
                        </td>
                        <td colspan="2">Posted to Stock Card:  
                            <br><br><br>
                            {{$data_rms->employee_posted_stock_card->fname}} {{$data_rms->employee_posted_stock_card->mname}} {{$data_rms->employee_posted_stock_card->lname}}
                        </td>
                        <td colspan="2">Accounted by:  
                            <br><br><br>
                            {{$data_rms->employee_accounted_by->fname}} {{$data_rms->employee_accounted_by->mname}} {{$data_rms->employee_accounted_by->lname}}
                        </td>
                        <td colspan="2">
                            
                        </td>
                    </tr>
                    <tr>
                        <td>{{$data_rms->employee_posted_bin_card->fname}} {{$data_rms->employee_posted_bin_card->mname}} {{$data_rms->employee_posted_bin_card->position}}
                        </td>
                        <td colspan="2">{{$data_rms->employee_posted_stock_card->fname}} {{$data_rms->employee_posted_stock_card->mname}} {{$data_rms->employee_posted_stock_card->position}}
                        </td>
                        <td colspan="2">{{$data_rms->employee_accounted_by->position}}
                        </td>
                        <td colspan="2">
                        </td>
                    </tr>
                    <tr>
                        <td>Date:</td>
                        <td colspan="2">Date:</td>
                        <td colspan="2">Date:</td>
                        <td colspan="2">
                        </td>
                    </tr>
                </tbody>
            </table>
            </div>
        
           
        
    <!-- <div class="row">
        <div class="col-xs-3">
            <p>Requisition Officer:</p> 
            <br><br>
            <span style="text-decoration:underline">HELEN P. SALVACION</span><br>
            Deparment Manager, ASD

            <br><br><br>
            <p>Approved for Release:</p> 
            <br><br>
            <span style="text-decoration:underline">HELEN P. SALVACION</span><br>
            Deparment Manager, ASD
        </div>
        <div class="col-xs-4">
        
        </div>
        <div class="col-xs-5">
            <p><em>Recieved the supplies shown above as issued</em></p> 
            <br><br><br>

            
            <em>Signature of person receiving supplies</em>
            <br><br><br>
            <p>Issued by:</p> 
            <br><br>
            <span style="text-decoration:underline">FLORENCIO S. BALUDO</span><br>
            Storekeeper
            <br><br><br>
            <p>Posted by:</p> 
            <br>
            <span style="text-decoration:underline">EDEN O. MADULA</span><br>
            Bookkeeper
        </div>
    </div>
    </div>row -->

    <div class="clearfix"></div>
    <br>
    <div align="center">
    <button id="btn-print" class="btn btn-success btn-sm no-print"
    ><i class="fa fa-print"></i> Print</button>
    </div>
</div>
</div>
<script type="text/javascript">
$('#btn-print').on('click',function(){
    $('body').removeClass('nav-sm').addClass('nav-md');
    window.print();
})
</script> 

<script src="{{ asset('js/app.js') }}"></script>
@endsection