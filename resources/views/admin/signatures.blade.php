@extends('layouts.admin')

@section('content')

<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-lg-12">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>RETURN MATERIAL SLIP Signatures</h4>
                </div>
                @if(Session::has('success_rms'))
                    <div class="alert alert-success">
                        {{ Session::get('success_rms') }}
                        @php
                        Session::forget('success_rms');
                        @endphp
                    </div>
                @endif
                <form action="{{route('signatures_rms')}}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="rms_id" value="{{$data_rms->id}}">
                    <div class="col-md-4 col-sm-12 col-lg-4">
                    <label for="Approved by">Approved by</label>
                        <select name="approved_by" id="approved_by" class="form-control">
                            <option value="{{$data_rms->approved_by}}">{{$data_rms->employee_approved->fname}} {{$data_rms->employee_approved->mname}} {{$data_rms->employee_approved->lname}}</option>
                            @foreach($data_employee as $Employee)
                                <option value="{{$Employee->id}}">{{$Employee->fname}} {{$Employee->mname}} {{$Employee->lname}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4 col-sm-12 col-lg-4">
                    <label for="Approved for Return">Approved for Return</label>
                        <select name="approved_return" id="approved_return" class="form-control">
                            <option value="{{$data_rms->approved_return}}">{{$data_rms->employee_approved_return->fname}} {{$data_rms->employee_approved_return->mname}} {{$data_rms->employee_approved_return->lname}}</option>
                            @foreach($data_employee as $Employee)
                                <option value="{{$Employee->id}}">{{$Employee->fname}} {{$Employee->mname}} {{$Employee->lname}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4 col-sm-12 col-lg-4">
                    <label for="Posted">Posted to Bincard</label>
                        <select name="posted_bin_card" id="posted_bin_card" class="form-control">
                            <option value="{{$data_rms->posted_bin_card}}">{{$data_rms->employee_posted_bin_card->fname}} {{$data_rms->employee_posted_bin_card->mname}} {{$data_rms->employee_posted_bin_card->lname}}</option>
                            @foreach($data_employee as $Employee)
                                <option value="{{$Employee->id}}">{{$Employee->fname}} {{$Employee->mname}} {{$Employee->lname}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4 col-sm-12 col-lg-4">
                    <label for="Posted">Posted to Stock</label>
                        <select name="posted_stock_card" id="posted_stock_card" class="form-control">
                        <option value="{{$data_rms->posted_stock_card}}">{{$data_rms->employee_posted_stock_card->fname}} {{$data_rms->employee_posted_stock_card->mname}} {{$data_rms->employee_posted_stock_card->lname}}</option>
                            @foreach($data_employee as $Employee)
                                <option value="{{$Employee->id}}">{{$Employee->fname}} {{$Employee->mname}} {{$Employee->lname}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4 col-sm-12 col-lg-4">
                    <label for="Accounted">Accounted by</label>
                        <select name="accounted_by" id="accounted_by" class="form-control">
                        <option value="{{$data_rms->accounted_by}}">{{$data_rms->employee_accounted_by->fname}} {{$data_rms->employee_accounted_by->mname}} {{$data_rms->employee_accounted_by->lname}}</option>
                            @foreach($data_employee as $Employee)
                                <option value="{{$Employee->id}}">{{$Employee->fname}} {{$Employee->mname}} {{$Employee->lname}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="clearfix"></div> <br>
                    <div class="text-center">
                    <button class="btn btn-info btn-md" id="btnsave">Save</button>
                    </div>
                </form>
            </div>
        </div><!--col-->
    </div><!--row-->
    <div class="row">
        <div class="col-md-12 col-sm-12 col-lg-12">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>PURCHASE REQUEST Signatures</h4>
                </div>
                @if(Session::has('success_pr'))
                    <div class="alert alert-success">
                        {{ Session::get('success_pr') }}
                        @php
                        Session::forget('success_pr');
                        @endphp
                    </div>
                @endif
                <form action="{{route('signatures_pr')}}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="pr_id" value="{{$data_pr->id}}">
                    <div class="col-md-4 col-sm-12 col-lg-4">
                    <label for="Prepared by:">Prepared by:</label>
                        <select name="prepared_by" id="prepared_by" class="form-control">
                            <option value="{{$data_pr->prepared_by}}">{{$data_pr->employee_prepared_by->fname}} {{$data_pr->employee_prepared_by->mname}} {{$data_pr->employee_prepared_by->lname}}</option>
                            @foreach($data_employee as $Employee)
                                <option value="{{$Employee->id}}">{{$Employee->fname}} {{$Employee->mname}} {{$Employee->lname}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4 col-sm-12 col-lg-4">
                    <label for="Certified by:">Certified by:</label>
                        <select name="certified_by" id="certified_by" class="form-control">
                            <option value="{{$data_pr->certified_by}}">{{$data_pr->employee_certified_by->fname}} {{$data_pr->employee_certified_by->mname}} {{$data_pr->employee_certified_by->lname}}</option>
                            @foreach($data_employee as $Employee)
                                <option value="{{$Employee->id}}">{{$Employee->fname}} {{$Employee->mname}} {{$Employee->lname}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4 col-sm-12 col-lg-4">
                    <label for="Approved by:">Approved by:</label>
                        <select name="approved_by" id="approved_by" class="form-control">
                            <option value="{{$data_pr->approved_by}}">{{$data_pr->employee_approved_by->fname}} {{$data_pr->employee_approved_by->mname}} {{$data_pr->employee_approved_by->lname}}</option>
                            @foreach($data_employee as $Employee)
                                <option value="{{$Employee->id}}">{{$Employee->fname}} {{$Employee->mname}} {{$Employee->lname}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="clearfix"></div> <br>
                    <div class="text-center">
                    <button class="btn btn-info btn-md" id="btnsave">Save</button>
                    </div>
                </form>
            </div>
        </div><!--col-->
    </div><!--row-->
    <div class="row">
        <div class="col-md-12 col-sm-12 col-lg-12">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>REQUEST & ISSUE VOUCHER Signatures</h4>
                </div>
                @if(Session::has('success_riv'))
                    <div class="alert alert-success">
                        {{ Session::get('success_riv') }}
                        @php
                        Session::forget('success_riv');
                        @endphp
                    </div>
                @endif
                <form action="{{route('signatures_riv')}}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="riv_id" value="{{$data_riv->id}}">
                    <div class="col-md-4 col-sm-12 col-lg-4">
                    <label for="Requisition Officer:">Requisition Officer:</label>
                        <select name="request_officer" id="request_officer" class="form-control">
                            <option value="{{$data_riv->request_officer}}">{{$data_riv->employee_request_officer->fname}} {{$data_riv->employee_request_officer->mname}} {{$data_riv->employee_request_officer->lname}}</option>
                            @foreach($data_employee as $Employee)
                                <option value="{{$Employee->id}}">{{$Employee->fname}} {{$Employee->mname}} {{$Employee->lname}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4 col-sm-12 col-lg-4">
                    <label for="Approved for Release:">Approved for Release:</label>
                        <select name="approved_release" id="approved_release" class="form-control">
                            <option value="{{$data_riv->approved_release}}">{{$data_riv->employee_approved_release->fname}} {{$data_riv->employee_approved_release->mname}} {{$data_riv->employee_approved_release->lname}}</option>
                            @foreach($data_employee as $Employee)
                                <option value="{{$Employee->id}}">{{$Employee->fname}} {{$Employee->mname}} {{$Employee->lname}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4 col-sm-12 col-lg-4">
                    <label for="Issued by:">Issued by:</label>
                        <select name="issued_by" id="issued_by" class="form-control">
                            <option value="{{$data_riv->issued_by}}">{{$data_riv->employee_issued_by->fname}} {{$data_riv->employee_issued_by->mname}} {{$data_riv->employee_issued_by->lname}}</option>
                            @foreach($data_employee as $Employee)
                                <option value="{{$Employee->id}}">{{$Employee->fname}} {{$Employee->mname}} {{$Employee->lname}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4 col-sm-12 col-lg-4">
                    <label for="Posted by:">Posted by:</label>
                        <select name="posted_by" id="stock_card" class="form-control">
                        <option value="{{$data_riv->posted_by}}">{{$data_riv->employee_posted_by->fname}} {{$data_riv->employee_posted_by->mname}} {{$data_riv->employee_posted_by->lname}}</option>
                            @foreach($data_employee as $Employee)
                                <option value="{{$Employee->id}}">{{$Employee->fname}} {{$Employee->mname}} {{$Employee->lname}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="clearfix"></div> <br>
                    <div class="text-center">
                    <button class="btn btn-info btn-md" id="btnsave">Save</button>
                    </div>
                </form>
            </div>
        </div><!--col-->
    </div><!--row-->
    <div class="row">
        <div class="col-md-12 col-sm-12 col-lg-12">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>RECEIVING REPORT Signatures</h4>
                </div>
                @if(Session::has('success_rr'))
                    <div class="alert alert-success">
                        {{ Session::get('success_rr') }}
                        @php
                        Session::forget('success_rr');
                        @endphp
                    </div>
                @endif
                <form action="{{route('signatures_rr')}}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="rr_id" value="{{$data_rr->id}}">
                    <div class="col-md-4 col-sm-12 col-lg-4">
                    <label for="Member 1">Member 1</label>
                        <select name="member_1" id="member_1" class="form-control">
                            <option value="{{$data_rr->member_1}}">{{$data_rr->employee_member_1->fname}} {{$data_rr->employee_member_1->mname}} {{$data_rr->employee_member_1->lname}}</option>
                            @foreach($data_employee as $Employee)
                                <option value="{{$Employee->id}}">{{$Employee->fname}} {{$Employee->mname}} {{$Employee->lname}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4 col-sm-12 col-lg-4">
                    <label for="Member 2">Member 2</label>
                        <select name="member_2" id="member_2" class="form-control">
                            <option value="{{$data_rr->member_2}}">{{$data_rr->employee_member_2->fname}} {{$data_rr->employee_member_2->mname}} {{$data_rr->employee_member_2->lname}}</option>
                            @foreach($data_employee as $Employee)
                                <option value="{{$Employee->id}}">{{$Employee->fname}} {{$Employee->mname}} {{$Employee->lname}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4 col-sm-12 col-lg-4">
                    <label for="Team Leader">Team Leader</label>
                        <select name="team_leader" id="team_leader" class="form-control">
                            <option value="{{$data_rr->team_leader}}">{{$data_rr->employee_team_leader->fname}} {{$data_rr->employee_team_leader->mname}} {{$data_rr->employee_team_leader->lname}}</option>
                            @foreach($data_employee as $Employee)
                                <option value="{{$Employee->id}}">{{$Employee->fname}} {{$Employee->mname}} {{$Employee->lname}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4 col-sm-12 col-lg-4">
                    <label for="Accepted by:">Accepted by:</label>
                        <select name="accepted_by" id="accepted_by" class="form-control">
                        <option value="{{$data_rr->accepted_by}}">{{$data_rr->employee_accepted_by->fname}} {{$data_rr->employee_accepted_by->mname}} {{$data_rr->employee_accepted_by->lname}}</option>
                            @foreach($data_employee as $Employee)
                                <option value="{{$Employee->id}}">{{$Employee->fname}} {{$Employee->mname}} {{$Employee->lname}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4 col-sm-12 col-lg-4">
                    <label for="Certified Correct:">Certified Correct:</label>
                        <select name="certified_correct" id="certified_correct" class="form-control">
                        <option value="{{$data_rr->certified_correct}}">{{$data_rr->employee_certified_correct->fname}} {{$data_rr->employee_certified_correct->mname}} {{$data_rr->employee_certified_correct->lname}}</option>
                            @foreach($data_employee as $Employee)
                                <option value="{{$Employee->id}}">{{$Employee->fname}} {{$Employee->mname}} {{$Employee->lname}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4 col-sm-12 col-lg-4">
                    <label for="Posted to Bincard:">Posted to Bincard:</label>
                        <select name="posted_bin_card" id="posted_bin_card" class="form-control">
                        <option value="{{$data_rr->posted_bin_card}}">{{$data_rr->employee_posted_bin_card->fname}} {{$data_rr->employee_posted_bin_card->mname}} {{$data_rr->employee_posted_bin_card->lname}}</option>
                            @foreach($data_employee as $Employee)
                                <option value="{{$Employee->id}}">{{$Employee->fname}} {{$Employee->mname}} {{$Employee->lname}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4 col-sm-12 col-lg-4">
                    <label for="Costed by:">Costed by:</label>
                        <select name="costed_by" id="costed_by" class="form-control">
                        <option value="{{$data_rr->costed_by}}">{{$data_rr->employee_costed_by->fname}} {{$data_rr->employee_costed_by->mname}} {{$data_rr->employee_costed_by->lname}}</option>
                            @foreach($data_employee as $Employee)
                                <option value="{{$Employee->id}}">{{$Employee->fname}} {{$Employee->mname}} {{$Employee->lname}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4 col-sm-12 col-lg-4">
                    <label for="Posted to Stock Card:">Posted to Stock Card:</label>
                        <select name="posted_stock_card" id="posted_stock_card" class="form-control">
                        <option value="{{$data_rr->posted_stock_card}}">{{$data_rr->employee_posted_stock_card->fname}} {{$data_rr->employee_posted_stock_card->mname}} {{$data_rr->employee_posted_stock_card->lname}}</option>
                            @foreach($data_employee as $Employee)
                                <option value="{{$Employee->id}}">{{$Employee->fname}} {{$Employee->mname}} {{$Employee->lname}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="clearfix"></div> <br>
                    <div class="text-center">
                    <button class="btn btn-info btn-md" id="btnsave">Save</button>
                    </div>
                </form>
            </div>
        </div><!--col-->
    </div><!--row-->
<script type="text/javascript">
$('#search').on('keyup',function(){
  $value=$(this).val();
  $.ajax({
    type : 'get',
    url : '{{URL::to('admin/rrstocksearch')}}',
    data:{'search':$value},
    success:function(data){
      $('.stockbinresult').html(data);
    } 
  });
})
</script> 
<script type="text/javascript">
$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/rrscript.js') }}"></script>
@endsection