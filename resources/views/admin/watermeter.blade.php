@extends('layouts.admin')

@section('content')

<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-6 col-sm-12 col-lg-12">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>Water Meters</h4>
                    
                </div>
                <div class="col-md-5 col-sm-5 col-xs-12 form-group top_search">
                        <div class="input-group">
                            <input id="search" type="text" class="form-control" placeholder="Search Consessionaire...">  
                        </div>
                        {{$data_water_meters->links()}}
                    </div>
                
                <table class="table table-striped">
                <thead>
                    <tr>
                    <th>RIV No. </th>
                    <th>Date Acquired </th>
                    <th>Meter Number</th>
                    <th>Concessionare</th>
                    <th>Address</th>
                    <th>Incharge</th>
                    
                    <th></th>
                    </tr>
                </thead>
                <tbody class="consessionaire">
                    @foreach($data_water_meters as $water_meter)
                    <tr class="row{{$water_meter->id}}">
                        <td><a href="/admin/riv/view/{{$water_meter->rivnumber}}">{{$water_meter->rivnumber}}</a></td>
                        <td>{{$water_meter->created_at->format('m-d-Y')}}</td>
                        <td>{{$water_meter->meternum}}</td>
                        <td>{{$water_meter->fullname}}</td>
                        <td>{{$water_meter->address}}</td>
                        <td>{{$water_meter->incharge}}</td>
                        <td><a href="javascript:;" 
                            data-id = "{{$water_meter->id}}"
                            data-meternum = "{{$water_meter->meternum}}"
                            data-fullname = "{{$water_meter->fullname}}"
                            data-address = "{{$water_meter->address}}"
                            data-incharge = "{{$water_meter->incharge}}"
                            data-riv_num = "{{$water_meter->rivnumber}}"
                            data-date_created = "{{$water_meter->created_at->format('m-d-Y')}}"
                            class="edit-modal btn btn-success btn-sm"><i class="fa fa-edit"></i> Edit</a>
                        <a href="/admin/consessionaire/{{$water_meter->id}}" class="btn btn-warning btn-sm"><i class="fa fa-search"></i> View</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

                <div class="clearfix"></div>
            </div>
        </div><!--col-->
        
    </div><!--row-->
</div>
<div id="consessionaireModal" class="modal fade bs-example-modal-lg" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
               
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
              
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">
                    {{ csrf_field() }}
                    <input type="hidden" class="form-control" id="fid">
                    <input type="hidden" class="form-control" id="riv_num">
                    <input type="hidden" class="form-control" id="date_created">
                    <div class="row">
                        <div class="form-group col-md-6 col-lg-6 col-sm-12 col-xs-12">
                            <label class="control-label" for="Meter No.">Meter No.</label>
                            <input type="text" class="form-control" name="edit_meter_num" id="edit_meter_num"> 
                        </div>
                        
                        <div class="form-group col-md-6 col-lg-6 col-sm-12 col-xs-12">
                            <label class="control-label" for="Consessionaire">Consessionaire</label>
                            <input type="text" class="form-control" name="edit_consessionaire" id="edit_consessionaire"> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6 col-lg-6 col-sm-12 col-xs-12">
                            <label class="control-label" for="Address">Address</label>
                            <input type="text" class="form-control" name="edit_address" id="edit_address"> 
                        </div>
                        <div class="form-group col-md-6 col-lg-6 col-sm-12 col-xs-12">
                            <label class="control-label">Incharge</label>
                            <input type="text" class="form-control" name="edit_incharge" id="edit_incharge"> 
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="saveEdit" class="btn saveEdit btn-primary" data-dismiss="modal">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$('#search').on('keyup',function(){
  $value=$(this).val();
  $.ajax({
    type : 'get',
    url : '{{URL::to('admin/consessioaniresearch')}}',
    data:{'search':$value},
    success:function(data){
      $('.consessionaire').html(data);
    } 
  });
})
</script> 
<script src="{{ asset('js/consescript.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
@endsection