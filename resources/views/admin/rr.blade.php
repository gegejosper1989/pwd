@extends('layouts.admin')

@section('content')

<div class="right_col" role="main">
    <div class="row">
    <div class="col-md-6 col-sm-12 col-lg-4">
        <div class="x_panel tile">
            <div class="x_title">
                <h4>Latest Purchase Request </h4>
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                        <th>PR Number</th>
                        <th>Supplier</th>
                        <th>Status</th>
                        <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data_pr as $Prdetail)
                        <tr>
                            <td><a href="/admin/pr-view/{{$Prdetail->prnum}}">{{$Prdetail->prnum}}</a></td>
                            <td>{{$Prdetail->supplier->suppliername}}</td>
                            <td><em>{{$Prdetail->prstatus}}</em></td>
                            <td><a data-prnum="{{$Prdetail->prnum}}" class="btn btn-sm btn-info add_pr"><i class="fa fa-plus"></a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="x_panel tile">
            <div class="x_title">
                <h4>Recent Recieving Report </h4>
            </div>
            <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                        <th>Date</th>
                        <th>RR Number</th>
                        <th>Supplier</th>
                        <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($data_rr as $rr)
                        <tr>
                            <td>{{$rr->rdate}}</td>
                            <td>
                                @if($rr->status == 'initial' || $rr->status == 'partial' )
                                <a href="/admin/rr/create/{{$rr->id}}">{{$rr->rrnumber}}</a>
                                @else
                                <a href="/admin/rr/rr-view/{{$rr->rrnumber}}">{{$rr->rrnumber}}</a>
                                @endif
                            </td>
                            
                            <td>{{$rr->supplierdetail->suppliername}}</td>
                            <td><em>{{$rr->status}}</em></td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="4"><em>No Data</em></td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
        </div>
    </div>
    <div class="col-md-6 col-sm-12 col-lg-8">
    <form action="{{route('createrr')}}" method="post">
        {{ csrf_field() }}
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>Recieving Report </h4>
                </div>

                <div class="clearfix"></div>
                <div class="col-md-4 col-sm-12 col-lg-3">
                    <label for="PR Number">PR Number</label>
                    <input class="form-control" type="text" name="pr_number" placeholder="PR Number" id="pr_number" required>
                </div>
                <div class="col-md-4 col-sm-12 col-lg-3">
                    <label for="Date">Date</label>
                    <input class="form-control" type="date" name="rdate" placeholder="Date" required>
                </div>
                <div class="col-md-4 col-sm-12 col-lg-3">
                    <label for="RR Number">RR Number</label>
                    <?php 
                        $num = $rrCount + 1;
                        if(strlen((string)$num) == 1) {
                            $riv = "00".$num;
                        }
                        elseif(strlen((string)$num) == 2) {
                            $riv = "0".$num;
                        }
                        elseif(strlen((string)$num) == 3){
                            $riv =  $num;
                        }
                    ?>
                    <input class="form-control" type="text" name="rrnumber" value="RR-<?php echo date('y'); ?>-<?php echo date('m'); ?>-{{$riv}}" required>
                </div>
                <div class="col-md-4 col-sm-12 col-lg-3">
                    <label for="Supplier">Supplier</label>

                   <select name="supplier" id="supplier">
                        @foreach($dataSupplier as $Supplier)
                            <option value="{{$Supplier->id}}">{{$Supplier->suppliername}}</option>
                        @endforeach
                   </select>
                </div>
                <div class="clearfix"></div> <br>
                <div class="col-md-3 col-sm-12 col-lg-3">
                    <label for="Address">Address</label>
                    <input type="text" class="form-control" id="supaddress" name="supaddress" placeholder="Address" required>
                </div>
                <div class="col-md-3 col-sm-12 col-lg-3">
                    <label for="Delivery Reciept #">Delivery Reciept #</label>
                    <input type="text" class="form-control" id="drnum" name="drnum" placeholder="Delivery Reciept #" required>
                </div>
                <div class="col-md-3 col-sm-12 col-lg-3">
                    <label for="Charge Invoice #">Charge Invoice #</label>
                    <input type="text" class="form-control" id="chargeinvoicenum" name="chargeinvoicenum" placeholder="Charge Invoice #" required>
                </div>
                <div class="col-md-3 col-sm-12 col-lg-3">
                    <label for="Order Type">Order Type</label>
                    <select name="ordertype" id="ordertype" class="form-control">
                        <option value="Job Order">Job Order</option>
                        <option value="Purchase Request">Purchase Request</option>
                    </select>
                </div>
                <div class="row">
                <div class="col-md-12 col-sm-12 col-lg-12 text-center">
                <br>
                <input type="submit" class="btn btn-lg btn-info btn-sm" class="text-center" value="Create">
                
                </div>
            </div>
        </div><!--col-->
    </div><!--row-->
</div>
<script type="text/javascript">
    $("#supplier").select2({
      tags: true
    });
</script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/rrscript.js') }}"></script>
@endsection