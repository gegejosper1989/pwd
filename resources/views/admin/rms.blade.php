@extends('layouts.admin')

@section('content')

<div class="right_col" role="main">
    <div class="row">
    <div class="col-md-6 col-sm-12 col-lg-5">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>Recent Returns</h4>
                </div>
                <div class="clearfix"></div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th>Date </th>
                        <th>RMS No</th>
                        <th>Status</th>
                        <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($return_data as $return)
                        <tr>
                            <td>{{$return->return_date}}</td>
                            <td>
                            @if($return->status == 'initial')
                            <a href="/admin/rms/create/{{$return->return_number}}">{{$return->return_number}}</a>
                            @else 
                            <a href="/admin/rms/view/{{$return->return_number}}">{{$return->return_number}}</a></a>
                            @endif    
                            </td>
                            <td>{{$return->status}}</td>
                            
                            <td><a href="/admin/rms/view/{{$return->return_number}}" class="btn btn-success btn-sm"><i class="fa fa-search"></i></a></td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="4"><em>No Record</em></td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            
            </div>
        </div><!--col-->
        <div class="col-md-8 col-sm-12 col-lg-7">
            <form action="{{route('rms_create')}}" method="post">
            {{ csrf_field() }}
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>Create RMS </h4>
                </div>
                <div class="col-md-4 col-sm-12 col-lg-6">
                <label for="Date">Date</label>
                <input class="form-control" type="date" name="return_date" id="return_date" required>
                </div>
                <div class="col-md-4 col-sm-12 col-lg-6">
                <label for="RMS Number">RMS Number</label>
                    <?php 
                        $num = $returnCount + 1;
                        if(strlen((string)$num) == 1) {
                            $return = "00".$num;
                        }
                        elseif(strlen((string)$num) == 2) {
                            $return = "0".$num;
                        }
                        elseif(strlen((string)$num) == 3){
                            $return =  $num;
                        }
                    ?>  
                <input class="form-control" type="text" name="return_number" value="RMS-<?php echo date('y'); ?>-<?php echo date('m'); ?>-{{$return}}" required>
                </div>
                <div class="col-md-4 col-sm-12 col-lg-6">
                <label for="Responsibility">Responsibility Center</label>
                <select name="department" id="department" class="form-control">
                    @foreach($dataDeparment as $Department)
                        <option value="{{$Department->depcode}}">{{$Department->depcode}}</option>
                    @endforeach
                </select>
                </div>
                <div class="col-md-4 col-sm-12 col-lg-6">
                <label for="Ref SRS Number">Ref SRS No.</label>
                <input class="form-control" type="text" name="srs_num" value="" required>
                </div>
                <div class="col-md-4 col-sm-12 col-lg-12">
                <label for="Requisitioner">Requisitioner</label>
            
                    <select name="requisitioner" id="requisitioner" class="form-control">
                        @foreach($data_employee as $Employee)
                            <option value="{{$Employee->id}}">{{$Employee->fname}} {{$Employee->mname}} {{$Employee->lname}}</option>
                        @endforeach
                    </select>
                </div>
                requestor
                <div class="col-md-12 col-sm-6 col-xs-12 form-group">
                <label for="Reason">Reason for Return</label>
                <textarea name="reason" id="reason" cols="30" rows="4" class="form-control" required></textarea>
                
                </div>
                <div class="row">
                <div class="col-md-12 col-sm-12 col-lg-12 text-center">
                <br>
                <input type="submit" class="btn btn-lg btn-info btn-sm" class="text-center" value="Create">
                
            </div>
            </form>
        </div><!--col-->
    
    </div><!--row-->
</div>
</div>
<script type="text/javascript">
    $("#requisitioner").select2({
      tags: true
    });
</script>
@endsection