@extends('layouts.admin')

@section('content')

<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-6 col-sm-12 col-lg-3">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>Stock Descriptions</h4>
                </div>
                <label for="Stock Category">Stock Category</label>
                {{ csrf_field() }}
                <select name="stockcat" id="stockcat" class="form-control">
                    @foreach($dataCategory as $Category)
                        <option value="{{$Category->catcode}}" >{{$Category->catname}}</option>
                    @endforeach
                </select>
                <label for="Stock Number">Stock Number</label>
                <input type="text" name="stocknumber" id="stocknumber" class="form-control">
                <label for="Description">Description</label>
                <input type="text" name="description" id="description" class="form-control">
                <label for="Ordering Point">Ordering Point</label>
                <input type="text" name="orderingpoint" id="orderingpoint" class="form-control">
                <label for="Quantity">Quantity</label>
                <input type="text" name="quantity" id="quantity" class="form-control">
                <label for="Unit">Unit</label>
                <select name="unit" id="unit" class="form-control">
                    <option value="roll/s">roll/s</option>
                    <option value="ream/s">ream/s</option>
                    <option value="box/es">box/es</option>
                    <option value="pc/s">pc/s</option>
                    <option value="bot/s">bot/s</option>
                    <option value="pad/s">pad/s</option>
                    <option value="kg/s">kg/s</option>
                    <option value="liter/s">liter/s</option>
                    <option value="can/s">can/s</option>
                    <option value="pack/s">pack/s</option>
                    <option value="stub/s">stub/s</option>
                    <option value="gal/s">gal/s</option>
                    <option value="liters">liters</option>
                    <option value="pail/s">pail/s</option>
                    <option value="drum/s">drum/s</option>
                    <option value="set/s">set/s</option>
                    <option value="meter/s">meter/s</option>
                    <option value="bag/s">bag/s</option>   
                </select>
                <br>
                <button type="submit" class="btn btn-info btn-sm" id="stocksave"> Save</button>
                <div class="clearfix"></div>
            </div>
        </div><!--col-->
        <div class="col-md-9 col-sm-12 col-lg-9">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>Stocks on Hand</h4>
                    {{$dataStock->links()}}
                </div>
                <div class="col-md-5 col-sm-5 col-xs-12 form-group top_search">
                    <div class="input-group">
                        <input type="text" id="search" class="form-control" placeholder="Search Stock...">  
                    </div>
                </div>
                    
                    <table class="table table-striped" id="tablestocks">
                    <thead>
                        <tr>
                        <th>Category </th>
                        <th>Stock No</th>
                        <th>Description</th>
                        <th>Stock Onhand</th>
                        <th>Unit</th>
                        <th>Ordering Point</th>
                        </tr>
                    </thead>
                    <tbody class="stockresult">
                       @foreach($dataStock as $Stock)
                        <tr class="row{{$Stock->id}}">
                            <td><a href="/admin/stocks/category/{{$Stock->catcode}}">{{$Stock->catcode}}</a></td>
                            <td><a href="/admin/stock/{{$Stock->id}}">{{$Stock->stocknumber}}</a></td>
                            <td>{{$Stock->stockname}}</td>
                            <td>{{$Stock->quantity}}</td>
                            <td>{{$Stock->unit}}</td>
                            <td>{{$Stock->orderingpoint}}</td>
                            <td><button class="btn edit-modal btn-success btn-sm" 
                                data-id="{{$Stock->id}}"
                                data-catcode="{{$Stock->catcode}}"
                                data-catname="{{$Stock->category->catname}}"
                                data-stocknumber="{{$Stock->stocknumber}}"
                                data-stockname="{{$Stock->stockname}}"
                                data-quantity="{{$Stock->quantity}}"
                                data-unit="{{$Stock->unit}}"
                                data-ordering="{{$Stock->orderingpoint}}"
                                ><i class="fa fa-edit"></i>Edit</button>
                                <a href="/admin/bincard/{{$Stock->id}}" class="btn btn-info btn-sm"><i class="fa fa-folder"></i> Bin Card</a> 
                                <a href="/admin/stock/{{$Stock->id}}" class="btn btn-warning btn-sm"><i class="fa fa-search"></i> View Stock</a>   
                            </td>
                        </tr>
                      @endforeach
                    </tbody>
                </table>
                {{$dataStock->links()}}
                <div class="clearfix"></div>
            </div>
        </div><!--col-->
    </div><!--row-->
</div>

<div id="stockModal" class="modal fade bs-example-modal-lg" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
               
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
              
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">
                    {{ csrf_field() }}
                    <input type="hidden" class="form-control" id="fid">
                    <div class="row">
                        <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <label class="control-label" for="Stock No.">Stock No.</label>
                            <input type="text" class="form-control" name="editStockNum" id="editStockNum"> 
                        </div>
                        <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <label class="control-label" for="Description">Description</label>
                            <input type="text" class="form-control" name="editDescription" id="editDescription"> 
                        </div>
                        <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <label class="control-label" for="Quantity">Quantity</label>
                            <input type="text" class="form-control" name="editQuantity" id="editQuantity"> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <label class="control-label">Category</label>
                            <select name="editCat" id="editCat" class="form-control">
                                <option value="" id="editCatVal"></option>
                                @foreach($dataCategory as $Category)
                                    <option value="{{$Category->catcode}}" >{{$Category->catname}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
                        <label class="control-label">Unit</label>
                            <select name="editUnit" id="editUnit" class="form-control">
                                <option value="" id="editUnitVal"></option>
                                <option value="roll/s">roll/s</option>
                                <option value="ream/s">ream/s</option>
                                <option value="box/es">box/es</option>
                                <option value="pc/s">pc/s</option>
                                <option value="bot/s">bot/s</option>
                                <option value="pad/s">pad/s</option>
                                <option value="kg/s">kg/s</option>
                                <option value="liter/s">liter/s</option>
                                <option value="can/s">can/s</option>
                                <option value="pack/s">pack/s</option>
                                <option value="stub/s">stub/s</option>
                                <option value="gal/s">gal/s</option>
                                <option value="liters">liters</option>
                                <option value="pail/s">pail/s</option>
                                <option value="drum/s">drum/s</option>
                                <option value="set/s">set/s</option>
                                <option value="meter/s">meter/s</option>
                                <option value="bag/s">bag/s</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <label class="control-label">Ordering Point</label>
                            <input type="text" class="form-control" name="editOrdering" id="editOrdering"> 
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn saveEdit btn-primary" data-dismiss="modal">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$('#search').on('keyup',function(){
  $value=$(this).val();
  $.ajax({
    type : 'get',
    url : '{{URL::to('admin/stocksearch')}}',
    data:{'search':$value},
    success:function(data){
      $('.stockresult').html(data);
    } 
  });
})
</script> 
<script type="text/javascript">
$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/stockscript.js') }}"></script>
@endsection