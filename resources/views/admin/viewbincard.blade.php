@extends('layouts.admin')

@section('content')

<div class="right_col" role="main">
    <div class="row">
    <div class="col-md-6 col-sm-12 col-lg-4 no-print">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>Bin Card </h4>
                </div>
               
                    <div class="input-group">
                        <input type="text" id="search" class="form-control" placeholder="Search Stock...">  
                    </div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th>Category </th>
                        <th>Stock No</th>
                        <th>Description</th>
                        
                        <th></th>
                        </tr>
                    </thead>
                    <tbody class="stockbinresult">
                    @foreach($dataStock as $Stock)
                        <tr>
                            <td><a href="/admin/category/{{$Stock->id}}">{{$Stock->catcode}}</a></td>
                            <td><a href="/admin/stock/{{$Stock->id}}">{{$Stock->stocknumber}}</a></td>
                            <td>{{$Stock->stockname}}</td>
                           
                            <td>
                                <a href="/admin/bincard/{{$Stock->id}}" class="btn btn-info btn-sm"><i class="fa fa-search"></i></a>                                
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            
            </div>
    </div><!--col-->
        <div class="col-md-6 col-sm-12 col-lg-8 col-xs-12">
            <div class="x_panel tile">
                <div class="x_title">
                    Stock Category : <strong>{{$getStock->catcode}}</strong>  <br>
                    Stock Number:  <strong>{{$getStock->stocknumber}}</strong><br>
                    Name:  <strong>{{$getStock->stockname}}</strong><br>
                    Quantity:  <strong>{{$getStock->quantity}}</strong><br>
                    Unit:  <strong>{{$getStock->unit}}</strong><br>
                </div>
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                        <th>Date </th>
                        <th>Reference</th>
                        <th>Recieved</th>
                        <th>Issued</th>
                        <th>Balance on Hand</th>
                        <th>Unit Cost</th>
                        <th>Department</th>
                        <th class="no-print"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data_bincard as $bincard)
                        <tr>
                            <td>{{$bincard->created_at->format('m-d-Y')}}</td>
                            <td>{{$bincard->reference}}</td>
                            @if($bincard->type == 'RECEIVED')
                            <td>{{$bincard->quantity}}</td>
                            <td>0</td>
                            @elseif($bincard->type == 'ISSUED')
                            <td>0</td>
                            <td>{{$bincard->quantity}}</td>
                            @else
                            <td>0</td>
                            <td>0</td>
                            @endif
                            <td>{{$bincard->balance}}</td>  
                            <td>{{$bincard->unitcost}}</td> 
                            <td>{{$bincard->department}}</td> 
                            <td class="no-print">
                                @if($bincard->type == 'RECEIVED')
                                <a href="/admin/rr/rr-view/{{$bincard->reference}}" class="btn btn-success btn-sm"><i class="fa fa-search"></i></a></td>

                                @elseif($bincard->type == 'ISSUED')
                                <a href="/admin/riv/view/{{$bincard->reference}}" class="btn btn-success btn-sm"><i class="fa fa-search"></i></a></td>
                                @else

                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            
            <div class="clearfix"></div>
            <button id="btn-print" class="btn btn-success btn-sm no-print"><i class="fa fa-print"></i> Print</button>
            </div>
            <div class="x_content">
            
            </div><!--x_content-->
        </div><!--col-->
    </div><!--row-->
</div>
<script type="text/javascript">
$('#search').on('keyup',function(){
  $value=$(this).val();
  $.ajax({
    type : 'get',
    url : '{{URL::to('admin/bincardstocksearch')}}',
    data:{'search':$value},
    success:function(data){
      $('.stockbinresult').html(data);
    } 
  });
})
</script> 
<script type="text/javascript">
$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>
<script type="text/javascript">
$('#btn-print').on('click',function(){
    $('body').removeClass('nav-sm').addClass('nav-md');
    window.print();
})
</script> 
<script src="{{ asset('js/app.js') }}"></script>
@endsection