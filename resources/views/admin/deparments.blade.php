@extends('layouts.admin')

@section('content')

<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-6 col-sm-12 col-lg-4">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>Department</h4>
                    
                </div>
                    {{ csrf_field() }}
                    <label for="Department Code">Department Code</label>
                    <input type="text" name="depcode" class="form-control" id="depcode">
                    <label for="Department Name">Department Name</label>
                    <input type="text" name="depname"  class="form-control" id="depname">
                    <label for="Department Head">Department Head</label>
                    <select name="dephead" id="dephead" class="form-control">
                        @foreach($dataEmployee as $Employee)
                            <option value="{{$Employee->id}}">{{$Employee->fname}} {{$Employee->mname}} {{$Employee->lname}}</option>
                        @endforeach
                    </select>
                    <br>
                    <button class="btn btn-info btn-sm" id="btnsave" disabled>Save</button>
                <div class="clearfix"></div>
            </div>
        </div><!--col-->
        
        <div class="col-md-8 col-sm-12 col-lg-8">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>Deparment Lists</h4>
                    
                </div>
                <table class="table table-striped" id="departmenttable">
                    <thead>
                        <tr>
                        <th>Department Code </th>
                        <th>Department Name</th>
                        <th>Department Head</th>
                        <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($dataDeparment as $Department)
                        <tr class="item{{$Department->id}}">
                            <td><a href="/admin/deparment/{{$Department->depcode}}">{{$Department->depcode}}</a></td>
                            <td><a href="/admin/deparment/{{$Department->depcode}}">{{$Department->depname}}</a></td>
                            <td>{{$Department->employee->fname}} {{$Department->employee->mname}} {{$Department->employee->lname}}</td>
                            <td><button class="btn btn-success btn-sm edit-modal"
                                data-id="{{$Department->id}}"
                                data-depcode="{{$Department->depcode}}"
                                data-depname="{{$Department->depname}}"
                                data-dephead="{{$Department->employee->fname}} {{$Department->employee->mname}} {{$Department->employee->lname}}"
                                data-depheadid="{{$Department->dephead}}"
                                ><i class="fa fa-edit"></i></button></td>
                        </tr>
                        @endforeach
                        
                    </tbody>
                </table>
                <div class="clearfix"></div>
            </div>
        </div><!--col-->
    </div><!--row-->
</div>
<div id="departmentModal" class="modal fade bs-example-modal-lg" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
               
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Update Department</h4>
              
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">
                    {{ csrf_field() }}
                    <input type="hidden" class="form-control" id="fid">
                    <div class="row">
                        <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <label class="control-label" for="Category Code">Department Code</label>
                            <input type="text" class="form-control" name="editDepCode" id="editDepCode"> 
                        </div>
                        <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <label class="control-label" for="Category Name">Deparment Name</label>
                            <input type="text" class="form-control" name="editDepName" id="editDepName"> 
                        </div>
                        <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <label class="control-label" for="Category Name">Department Head</label>
                            <select name="editDepHead" id="editDepHead" class="form-control">
                                <option value="" id="editDepHeadVal"></option>
                                @foreach($dataEmployee as $Employee)
                                <option value="{{$Employee->id}}">{{$Employee->fname}} {{$Employee->mname}} {{$Employee->lname}}</option>
                                @endforeach
                            </select>
                        </div>
                        
                    </div>
                    
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary editSave" data-dismiss="modal">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="deleteModal" class="modal fade bs-example-modal-lg" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
               
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Delete Department</h4>
              
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">
                    {{ csrf_field() }}
                    <input type="hidden" class="form-control" id="delid">
                </form>
                <p>Are you sure you want to delete this department?</p>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger delete" data-dismiss="modal">Delete</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/deparmentscript.js') }}"></script>
@endsection