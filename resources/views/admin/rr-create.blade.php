@extends('layouts.admin')

@section('content')

<div class="right_col" role="main">
<div class="row">
        <div class="col-md-12 col-sm-12 col-lg-12">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>Receiving Request Form</h4>
                </div>
                <div class="col-md-4 col-sm-12 col-lg-3">
                <label for="PR Number">PR Number</label>
                <input class="form-control" name="text" id="pr_number" value="{{$rrReceiving->pr_number}}" readonly>
                </div>
                <div class="col-md-4 col-sm-12 col-lg-3">
                <label for="Date">Date</label>
                <input class="form-control" name="text" id="today" value="{{$rrReceiving->rdate}}" readonly>
                </div>
                <div class="col-md-4 col-sm-12 col-lg-3">
                <label for="RR Number">RR Number</label>
                <input class="form-control" type="text" name="rrnumber" value="{{$rrReceiving->rrnumber}}" readonly>
                </div>
                <div class="col-md-4 col-sm-12 col-lg-3">
                <label for="Responsibility">Supplier</label>
                <input class="form-control" type="text" name="supplier" value="{{$rrReceiving->supplier}}" readonly>
                </div>
                <div class="clearfix"></div>
            <br>
           
            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 form-group">
                <label for="Address">Address</label>
                <input class="form-control" type="text" name="address" value="{{$rrReceiving->address}}" readonly>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 form-group">
                <label for="Delivery Reciept #">Delivery Reciept #</label>
                <input class="form-control" type="text" name="drnum" value="{{$rrReceiving->deliverynum}}" readonly>
            </div><div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 form-group">
                <label for="Charge Invoice #">Charge Invoice #</label>
                <input class="form-control" type="text" name="chargeinvoicenum" value="{{$rrReceiving->chargenum}}" readonly>
            </div>
            <div class="col-md-3  col-lg-3 col-sm-6 col-xs-12 form-group">
                <label for="Order Type">Order Type</label>
                <input class="form-control" type="text" name="ordertype" value="{{$rrReceiving->ordertype}}" readonly>
            </div>
            
            </div>
        </div><!--col-->
    </div><!--row-->
    <div class="row">
        <div class="col-md-6 col-sm-12 col-lg-5">
            <div class="x_panel tile">
                <h5>Purchase Request #: {{$rrReceiving->pr_number}} stock details</h5>
                <div class="input-group">
                        
                    </div>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                <th>Category </th>
                                <th>SN</th>
                                <th>Description</th>
                                <th>Qty</th>
                                <th></th>
                                </tr>
                            </thead>
                            <tbody class="stockbinresult">
                            @foreach($pr_item as $Stock)
                                <tr>
                                    <td><a href="/admin/stocks/category/{{$Stock->stock->catcode}}">{{$Stock->stock->catcode}}</a></td>
                                    <td><a href="/admin/stock/{{$Stock->id}}">{{$Stock->stock->stocknumber}}</a></td>
                                    <td>{{$Stock->stock->stockname}}</td>
                                    <td>{{$Stock->reqquantity}}</td>
                                    <td>
                                        <a href="javascript:;" 
                                            class="add-modal btn btn-sm btn-success" 
                                            data-stocknumber="{{$Stock->stock->stocknumber}}"
                                            data-stockid="{{$Stock->stocknumber}}"
                                            data-catcode="{{$Stock->stock->catcode}}" 
                                            data-stockname="{{$Stock->stock->stockname}}" 
                                            data-quantity="{{$Stock->reqquantity}}"
                                            data-ordertype="{{$rrReceiving->ordertype}}" 
                                            data-id="{{$Stock->id}}" >
                                            <i class="fa fa-plus"> 
                                        </i></a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
            
            </div>
        </div><!--col-->
        <div class="col-md-6 col-sm-12 col-lg-7">
            <div class="x_panel tile">
                <table class="table table-striped" id="rr_details">
                <thead>
                    <tr>
                    <th>Category </th>
                    <th>Stock No</th>
                    <th>Description</th>
                    <th>Unit Cost</th>
                    <th>Quantity</th>
                    <th></th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($rr_items as $rr_item)
                       <tr class="item{{$rr_item->id}}">
                           <td>{{$rr_item->catcode}}</td>
                           <td>{{$rr_item->stock->stocknumber}}</td>
                           <td>{{$rr_item->description}}</td>
                           <td>{{$rr_item->unitcost}}</td>
                           <td>{{$rr_item->quantity}}</td>
                           <td>
                            <a class='delete-modal btn btn-danger btn-small' data-id='{{$rr_item->id}}'>
                                <i class='fa fa-times'></i></a>
                           </td>
                       </tr> 
                    @empty
                    
                    @endforelse
                </tbody>
            </table>
            
            </div>
            
    </div><!--row-->
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-lg-12 text-center">
        <a href="/admin/rr/save-partial/{{$rrReceiving->rrnumber}}" class="btn btn-info btn-lg">Save Partial</a>
        <a href="/admin/rr/save/{{$rrReceiving->rrnumber}}" class="btn btn-success btn-lg">Receive</a>
        </div>
    </div>
</div>

<div id="stockModal" class="modal fade bs-example-modal-lg" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">
                    {{ csrf_field() }}
                    <input type="hidden" class="form-control" id="id">
                    <div class="row">
                        <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <label class="control-label" for="Stock No.">Stock No.</label>
                            <input type="text" class="form-control" name="stocknum" id="stocknum" readonly> 
                            <input type="text" class="form-control" name="stockid" id="stockid" readonly> 
                        </div>
                        <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <label class="control-label" for="Description">Description</label>
                            <input type="text" class="form-control" name="description" id="description" readonly> 
                        </div>
                        <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <label class="control-label" for="Quantity">Remaining Quantity</label>
                            <input type="text" class="form-control" name="onhandQuantity" id="onhandQuantity" readonly> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <label class="control-label">Category</label>
                            <input type="text" class="form-control" name="catcode" id="catcode" readonly> 
                        </div>
                        <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <label class="control-label">Unit Cost</label>
                            <input type="number" class="form-control" name="unit_cost" id="unit_cost"> 
                        </div>
                        <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <label class="control-label">Quantity</label>
                            <input type="number" class="form-control" name="rec_quantity" id="rec_quantity"> 
                            <input type="hidden" class="form-control" name="rr_number" id="rr_number" value="{{$rrReceiving->rrnumber}}"> 
                            <input type="hidden" class="form-control" name="order_type" id="order_type" value="{{$rrReceiving->ordertype}}"> 
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" id="saveRR" class="btn saveRR btn-primary" data-dismiss="modal" disabled>Add</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="deleteModal" class="modal fade bs-example-modal-lg" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
               
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel"></h4>
              
            </div>
            <div class="modal-body">
            <p>
            Are you sure you want to remove this item?
            </p>
            <form class="form-horizontal" role="form">
                {{ csrf_field() }}
                <input type="hidden" class="form-control" id="fid">
                
            </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn delete btn-danger" data-dismiss="modal">Remove</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="confirmModal" class="modal fade bs-example-modal-lg" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
               
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel"> Process RIV</h4>
              
            </div>
            <div class="modal-body">
            <p>
            Are you sure you want to process this RIV?
            </p>
            <form class="form-horizontal" role="form">
                {{ csrf_field() }}
                <input type="hidden" class="form-control" id="revid">
                
            </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn confirmProcess btn-success" data-dismiss="modal">Process</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="consdeleteModal" class="modal fade bs-example-modal-lg" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
               
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
              
            </div>
            <div class="modal-body">
            <p>
            Are you sure you want to remove this concessionaire?
            </p>
            <form class="form-horizontal" role="form">
                    {{ csrf_field() }}
                    <input type="hidden" class="form-control" id="consid">
                 
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn delete-cons btn-danger" data-dismiss="modal">Remove</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$('#search').on('keyup',function(){
  $value=$(this).val();
  $.ajax({
    type : 'get',
    url : '{{URL::to('admin/rrstocksearch')}}',
    data:{'search':$value},
    success:function(data){
      $('.stockbinresult').html(data);
    } 
  });
})
</script> 
<script type="text/javascript">
$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/rrscript.js') }}"></script>
@endsection