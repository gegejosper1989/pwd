@extends('layouts.admin')

@section('content')

<div class="right_col" role="main">
<div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
            <div class="title-header text-center" >
                <h3>PAGADIAN CITY WATER DISTRICT</h3>
                <p>Datoc St. Gatas Dist., Pagadian City</p>
                <p>Tel No. 2141-747-2144 Fax No. 2142-179</p>
                <h4>RECIEVING REPORT</h4>
            </div>
            <div class="clearfix"></div>
            <br><br>
            <div class="col-xs-4">
                <label for="">Supplier</label> {{$data_rr_info->pr_detail->supplier->suppliername}} <br>
                <label for="">Address</label> {{$data_rr_info->pr_detail->supplier->address}}
            </div>
            <div class="col-xs-4">
                <label for="">Purchase Order No.</label> {{$data_rr_info->pr_number}} <br>
                <label for="">Delivery Reciept No.</label> 
            </div>
            <div class="col-xs-4">
                <label for="">RR No.</label> {{$data_rr_info->rrnumber}} <br>
                <label for="Date">Date : </label> {{$data_rr_info->rdate}} <br>
            </div>
        </div><!--col-->
    </div><!--row-->
    <div class="clearfix"></div>
            <br>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-lg-12">
            <div class="x_panel tile">
                <table class="table table-bordered" id="rivdetails">
                <thead>
                    <tr>
                    <th>Stock No.</th> 
                    <th>ITEM</th>
                    <th>QTY</th>
                    <th>Unit</th> 
                    <th>Unit Price</th>
                    <th>Total Cost</th>
                    <th>Other Charges</th>
                    <th>Total Delivery Cost</th>
                    </tr>
                </thead>
                <tbody>                  
                    @forelse($data_rr_item as $rr_item)
                        <tr class="item{{$rr_item->id}}">
                            <td>{{$rr_item->stock->stocknumber}}</td>
                            <td>{{$rr_item->stock->stockname}}</td>    
                            <td>{{$rr_item->quantity}}</td>
                            <td>{{$rr_item->stock->unit}}</td>
                            <td>{{number_format($rr_item->unitcost,2)}}</td>
                            @php
                                $totalcost = $rr_item->quantity * $rr_item->unitcost;
                            @endphp    
                            <td>{{number_format($totalcost,2)}}</td>
                            @php
                                $total_delivery_cost = $rr_item->othercost + $totalcost;
                            @endphp
                            <td>{{number_format($rr_item->othercost,2)}}</td>
                            <td>{{$total_delivery_cost}}</td>
                        </tr>
                    @empty
                    <tr class="norecordriv"><td colspan="6" align="center"> <em>No Record</em></td></tr>
                    @endforelse
                </tbody>
            </table>
            </div>
            <div class="row">
                <div class="col-xs-3">
                    <p>Prepared by:</p> 
                    <br><br>
                    <span style="text-decoration:underline">{{$data_rr->employee_member_1->fname}} {{$data_rr->employee_member_1->mname}} {{$data_rr->employee_member_1->lname}}</span><br>
                    Member

                    <br><br><br>
                    <span style="text-decoration:underline">{{$data_rr->employee_member_2->fname}} {{$data_rr->employee_member_2->mname}} {{$data_rr->employee_member_2->lname}}</span><br>
                    Member

                    <br><br><br>

                    <span style="text-decoration:underline">{{$data_rr->employee_team_leader->fname}} {{$data_rr->employee_team_leader->mname}} {{$data_rr->employee_team_leader->lname}}</span><br>
                    Team Leader

                </div>
                <div class="col-xs-3">
                    <p>Accepted by:</p> 
                    <br><br>
                    <span style="text-decoration:underline">{{$data_rr->employee_accepted_by->fname}} {{$data_rr->employee_accepted_by->mname}} {{$data_rr->employee_accepted_by->lname}}</span><br>
                    {{$data_rr->employee_accepted_by->position}}
                </div>
                <div class="col-xs-3">
                    <p>Certified Correct:</p> 
                    <br><br>
                    <span style="text-decoration:underline">{{$data_rr->employee_certified_correct->fname}} {{$data_rr->employee_certified_correct->mname}} {{$data_rr->employee_certified_correct->lname}}</span><br>
                    {{$data_rr->employee_certified_correct->position}}
                    <br><br><br>
                    <p>Posted to Bincard:</p> 
                    <br>
                    <span style="text-decoration:underline">{{$data_rr->employee_posted_bin_card->fname}} {{$data_rr->employee_posted_bin_card->mname}} {{$data_rr->employee_posted_bin_card->lname}}</span><br>
                    {{$data_rr->employee_posted_bin_card->position}}
                </div>
                <div class="col-xs-3">
                    <p>Costed by:</p> 
                    <br><br>
                    <span style="text-decoration:underline">{{$data_rr->employee_costed_by->fname}} {{$data_rr->employee_costed_by->mname}} {{$data_rr->employee_costed_by->lname}}</span><br>
                    {{$data_rr->employee_costed_by->position}}
                    <br><br><br>
                    <p>Posted to Stock/Property Card:</p> 
                    <br>
                    <span style="text-decoration:underline">{{$data_rr->employee_posted_stock_card->fname}} {{$data_rr->employee_posted_stock_card->mname}} {{$data_rr->employee_posted_stock_card->lname}}</span><br>
                    {{$data_rr->employee_posted_stock_card->position}}
                </div>
            </div>
            <div align="center">
                <button id="btn-print" class="btn btn-success btn-sm no-print"><i class="fa fa-print"></i> Print</button>
            </div>
    </div><!--row-->
</div>
<script type="text/javascript">
$('#btn-print').on('click',function(){
    $('body').removeClass('nav-sm').addClass('nav-md');
    window.print();
})
</script> 

<script src="{{ asset('js/app.js') }}"></script>
@endsection