@extends('layouts.admin')

@section('content')

<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-6 col-sm-12 col-lg-6">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>Setting Re-ordering Point </h4>
                    
                </div>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                            <th>Category </th>
                            <th width="150">Stock No</th>
                            <th>Description</th>
                            
                            <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        {{ csrf_field() }}
                            @foreach($dataStockNoReorder as $StockReOrder)
                            <tr class="item{{$StockReOrder->id}}">
                                <td>{{$StockReOrder->catcode}}</td>
                                <td>{{$StockReOrder->stocknumber}}</td>
                                <td>{{$StockReOrder->stockname}}</td>
                               
                                <td>
                                    <a href="javascript:;" 
                                        class="set-modal btn btn-sm btn-success" 
                                        data-stocknumber="{{$StockReOrder->stocknumber}}"
                                        data-catcode="{{$StockReOrder->catcode}}" 
                                        data-stockname="{{$StockReOrder->stockname}}" 
                                        data-id="{{$StockReOrder->id}}" >
                                        <i class="fa fa-pencil">
                                    </i></a>
                                </td> 
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <a href="#" class="btn btn-sm btn-success pull-right"> <i class="fa fa-save"></i> Save</a>
                <div class="clearfix"></div>
            </div>
        </div><!--col-->
        <div class="col-md-6 col-sm-12 col-lg-6">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>Re-ordering Point</h4>
                    
                </div>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                            <th>Category </th>
                            <th width="150">Stock No</th>
                            <th>Description</th>
                            <th>Ordering Point</th>
                            <th></th>
                            </tr>
                        </thead>
                        <tbody id="reoderingpoint">
                        {{ csrf_field() }}
                            @foreach($dataStock as $Stock)
                            <tr class="setitem{{$Stock->id}}">
                                <td>{{$Stock->catcode}}</td>
                                <td>{{$Stock->stocknumber}}</td>
                                <td>{{$Stock->stockname}}</td>
                                <td>{{$Stock->orderingpoint}}</td>
                                <td>
                                    <a href="javascript:;" 
                                        class="set-modal btn btn-sm btn-info" 
                                        data-stocknumber="{{$Stock->stocknumber}}"
                                        data-catcode="{{$Stock->catcode}}" 
                                        data-stockname="{{$Stock->stockname}}" 
                                        data-id="{{$Stock->id}}" >
                                        <i class="fa fa-edit">
                                    </i></a>
                                </td> 
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                <div class="clearfix"></div>
            </div>
        </div><!--col-->
        
    </div><!--row-->

<div id="stockModal" class="modal fade bs-example-modal-lg" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
               
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
              
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">
                    {{ csrf_field() }}
                    <input type="hidden" class="form-control" id="fid">
                    <div class="row">
                        <div class="form-group col-md-6 col-lg-6 col-sm-12 col-xs-12">
                            <label class="control-label" for="Stock No.">Stock No.</label>
                            <input type="text" class="form-control" name="stocknum" id="stocknum" readonly> 
                        </div>
                        <div class="form-group col-md-6 col-lg-6 col-sm-12 col-xs-12">
                            <label class="control-label" for="Description">Description</label>
                            <input type="text" class="form-control" name="description" id="description" readonly> 
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6 col-lg-6 col-sm-12 col-xs-12">
                            <label class="control-label">Category</label>
                            <input type="text" class="form-control" name="catcode" id="catcode" readonly> 
                        </div>
                        
                        <div class="form-group col-md-6 col-lg-6 col-sm-12 col-xs-12">
                            <label class="control-label">Ordering Point</label>
                            <input type="number" class="form-control" name="orderingpoint" id="orderingpoint"> 
                           
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn setReordering btn-primary" data-dismiss="modal">Save</button>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/reorderscript.js') }}"></script>
@endsection