@extends('layouts.admin')

@section('content')

<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-6 col-sm-12 col-lg-4">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>Employee</h4>
                    
                </div>
                    {{ csrf_field() }}
                    <label for="First Name">First Name</label>
                    <input type="text" name="fname" class="form-control" id="fname">
                    <label for="Middle Name">Middle Name</label>
                    <input type="text" name="mname"  class="form-control" id="mname">
                    <label for="Last Name">Last Name</label>
                    <input type="text" name="lname"  class="form-control" id="lname">
                    <label for="Position">Position</label>
                    <input type="text" name="position"  class="form-control" id="position">
                    <label for="Department">Department</label>
                    <select name="department" id="department" class="form-control">
                        @foreach($data_deparment as $deparment)
                            <option value="{{$deparment->depcode}}">{{$deparment->depcode}}</option>
                        @endforeach
                    </select>
                    <br>
                    <button class="btn btn-info btn-sm" id="btnsave" disabled>Save</button>
                <div class="clearfix"></div>
            </div>
        </div><!--col-->
        
        <div class="col-md-8 col-sm-12 col-lg-8">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>Employees List</h4>
                    
                </div>
                <table class="table table-striped" id="employeetable">
                    <thead>
                        <tr>
                        <th>Full Name</th>
                        <th>Position</th>
                        <th>Department</th>
                        <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data_employee as $employee)
                        <tr class="item{{$employee->id}}">
                            
                            
                            <td>{{$employee->fname}} {{$employee->mname}} {{$employee->lname}}</td>
                            <td>{{$employee->position}}</td>
                            <td>{{$employee->department}}</td>
                            <td><button class="btn btn-success btn-sm edit-modal"
                                data-id="{{$employee->id}}"
                                data-depcode="{{$employee->department}}"
                                data-position="{{$employee->position}}"
                                data-fname="{{$employee->fname}}"
                                data-lname="{{$employee->lname}}"
                                data-mname="{{$employee->mname}}"
                                ><i class="fa fa-edit"></i></button></td>
                        </tr>
                        @endforeach
                        
                    </tbody>
                </table>
                <div class="clearfix"></div>
            </div>
        </div><!--col-->
    </div><!--row-->
</div>
<div id="employeeModal" class="modal fade bs-example-modal-lg" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
               
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Update Employee</h4>
              
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">
                    {{ csrf_field() }}
                    <input type="hidden" class="form-control" id="fid">
                    <div class="row">
                        <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <label class="control-label" for="First Name">First Name</label>
                            <input type="text" class="form-control" name="edit_fname" id="edit_fname"> 
                        </div>
                        <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <label class="control-label" for="Middle Name">Middle Name</label>
                            <input type="text" class="form-control" name="edit_mname" id="edit_mname"> 
                        </div>
                        <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <label class="control-label" for="Last Name">Last Name</label>
                            <input type="text" class="form-control" name="edit_lname" id="edit_lname"> 
                        </div>
                        <div class="form-group col-md-4 col-lg-6 col-sm-12 col-xs-12">
                            <label class="control-label" for="Position">Position</label>
                            <input type="text" class="form-control" name="edit_position" id="edit_position"> 
                        </div>
                        <div class="form-group col-md-4 col-lg-6 col-sm-12 col-xs-12">
                            <label class="control-label" for="Category Name">Department Head</label>
                            <select name="edit_department" id="edit_department" class="form-control">
                                @foreach($data_deparment as $deparment)
                                    <option value="{{$deparment->depcode}}">{{$deparment->depcode}}</option>
                                @endforeach
                            </select>
                        </div>
                        
                    </div>
                    
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary editSave" data-dismiss="modal">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="deleteModal" class="modal fade bs-example-modal-lg" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
               
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Delete Department</h4>
              
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">
                    {{ csrf_field() }}
                    <input type="hidden" class="form-control" id="delid">
                </form>
                <p>Are you sure you want to delete this department?</p>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger delete" data-dismiss="modal">Delete</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/employeescript.js') }}"></script>
@endsection