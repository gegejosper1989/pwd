@extends('layouts.admin')

@section('content')

<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-6 col-sm-12 col-lg-12">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>Yearly Budget</h4>
                </div>
                <div class="col-md-5 col-sm-5 col-xs-12 form-group top_search">
                    <div class="input-group">
                        <input type="text" id="search" class="form-control" placeholder="Search Stock...">  
                    </div>

                    <a href="/admin/processbudget" class="btn btn-success btn-sm">Import department yearly budget</a><a href="/admin/resetbudget" class="btn btn-warning btn-sm">Reset department yearly budget</a>
                </div>
                <div class="clearfix"></div>
                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <?php $count1 = 0; ?>
                      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        @foreach($dataDepartment as $Department)
                            <?php $count1++; ?>
                            <li role="presentation" class="@if($count1 == 1) active @endif"><a href="#budget{{$Department->id}}" id="budget{{$Department->id}}-tab" role="tab" data-toggle="tab" aria-expanded="true">{{$Department->depcode}}</a></li>
                        @endforeach
                      </ul>
                      <div id="myTabContent" class="tab-content">
                      <?php $count = 0; ?>
                      @foreach($dataDepartment as $Departmentdetails)
                        <?php $count++; ?>
                            <div role="tabpanel" class="tab-pane fade @if($count == 1) active @endif in yearlybudget" id="budget{{$Departmentdetails->id}}" aria-labelledby="#budget{{$Departmentdetails->id}}">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                    <th>Category </th>
                                    <th>Stock No</th>
                                    <th>Description</th>
                                    <th>Department</th>
                                    <th>Budget</th>
                                    <th>Consumed</th>
                                    </tr>
                                </thead>
                                <tbody class="stockresult">
                                <?php $countbudget=0; ?>
                                @foreach($dataBudget as $Budget)
                                    @if($Budget->department == $Departmentdetails->depcode)
                                    <tr class="row{{$Budget->id}}">
                                        <td><a href="/admin/stocks/category/{{$Budget->category}}">{{$Budget->category}}</a></td>
                                        <td><a href="/admin/stock/{{$Budget->id}}">{{$Budget->stock->stocknumber}}</a></td>
                                        <td>{{$Budget->stock->stockname}}</td>
                                        <td>{{$Budget->department}}</td>
                                        <td>{{$Budget->budget}}</td>
                                        <td>{{$Budget->consumed}}</td>
                                        <td><button class="btn edit-modal btn-success btn-sm" 
                                            data-id="{{$Budget->id}}"
                                            data-catcode="{{$Budget->category}}"
                                            data-stocknumber="{{$Budget->stock->stocknumber}}"
                                            data-stockid="{{$Budget->stock->id}}"
                                            data-stockname="{{$Budget->stock->stockname}}"
                                            data-consumed="{{$Budget->consumed}}"
                                            data-department="{{$Budget->department}}"
                                            data-budget="{{$Budget->budget}}"
                                            ><i class="fa fa-edit"></i></button>
                                        
                                        </td>
                                    </tr>
                                    <?php $countbudget++; ?>
                                    @endif
                                @endforeach

                                <tr><td>{{$countbudget}}</td></tr>
                                </tbody>
                            </table>
                            </div>
                        @endforeach
                      </div>
                </div>   

                <div class="clearfix"></div>
            </div>
        </div><!--col-->
        
    </div><!--row-->
</div>
<div id="stockModal" class="modal fade bs-example-modal-lg" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Update Budget</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">
                    {{ csrf_field() }}
                    <input type="hidden" class="form-control" id="fid">
                    <input type="hidden" class="form-control" id="stockid">
                    <div class="row">
                        <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <label class="control-label" for="Quantity">Category Code</label>
                            <input type="text" class="form-control" name="catcode" id="catcode" readonly> 
                        </div>
                        <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <label class="control-label" for="Stock No.">Stock No.</label>
                            <input type="text" class="form-control" name="StockNum" id="StockNum" readonly> 
                        </div>
                        <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <label class="control-label" for="Description">Description</label>
                            <input type="text" class="form-control" name="Description" id="Description" readonly> 
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <label class="control-label" for="Deparment">Deparment</label>
                            <input type="text" class="form-control" name="department" id="department" readonly> 
                        </div>
                        <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <label class="control-label" for="Budget">Budget</label>
                            <input type="text" class="form-control" name="editbudget" id="editbudget"> 
                        </div>
                        <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <label class="control-label" for="Consumed">Consumed</label>
                            <input type="text" class="form-control" name="editconsumed" id="editconsumed"> 
                        </div>
                        
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn saveEdit btn-primary" data-dismiss="modal">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$('#search').on('keyup',function(){
  $value=$(this).val();
  $.ajax({
    type : 'get',
    url : '{{URL::to('admin/stocksearchbudget')}}',
    data:{'search':$value},
    success:function(data){
      $('.stockresult').html(data);
    } 
  });
})
</script> 
<script type="text/javascript">
$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/budgetscript.js') }}"></script>
@endsection