@extends('layouts.admin')

@section('content')

<div class="right_col" role="main">
<div class="row">
        <div class="col-md-12 col-sm-12 col-lg-12">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>Return Material Slip</h4>
                </div>
                <div class="col-md-4 col-sm-12 col-lg-4">
                <label for="Date">Date</label>
                <input class="form-control" name="text" id="today" value="{{$return_data->return_date}}" readonly>
                </div>
                <div class="col-md-4 col-sm-12 col-lg-4">
                <label for="RMS Number">RMS Number</label>
                <input class="form-control" type="text" name="return_number" value="{{$return_data->return_number}}" readonly>
                </div>
                <div class="col-md-4 col-sm-12 col-lg-4">
                <label for="Department">Department</label>
                <input class="form-control" type="text" name="department" value="{{$return_data->department}}" readonly>
                </div>
                <div class="col-md-4 col-sm-12 col-lg-4">
                <label for="srs">Ref SRS No.</label>
                <input class="form-control" type="text" name="srs_number" value="{{$return_data->srs_num}}" readonly>
                </div>
                <div class="col-md-4 col-sm-12 col-lg-4">
                <label for="Requisitioner">Requisitioner</label>
                <input class="form-control" type="text" name="requisitioner" value="{{$return_data->employee->lname}}, {{$return_data->employee->fname}} {{$return_data->employee->mname}}" readonly>
                </div>
                <div class="col-md-4 col-sm-12 col-lg-4">
                <label for="Reason">Reason</label>
                    <textarea class="form-control" name="reason" id="" cols="30" rows="1" disabled> {{$return_data->reason}}</textarea>
                    <p></p>
                </div>
                <div class="clearfix"></div>
            
            </div>
        </div><!--col-->
    </div><!--row-->
    <div class="row">
        <div class="col-md-6 col-sm-12 col-lg-5">
            <div class="x_panel tile">
                <h5>Stock Search</h5>

                <input type="text" id="search" class="form-control" placeholder="Search Stock...">  
                <div class="input-group">
                        
                    </div>
                    <table class="table table-striped">
                            <thead>
                                <tr>
                                <th>Category </th>
                                <th>SN</th>
                                <th>Description</th>
                                <th>Qty</th>
                            
                                <th></th>
                                </tr>
                            </thead>
                            <tbody class="stockbinresult">
                            @foreach($dataStock as $Stock)
                                <tr>
                                    <td><a href="/admin/stocks/category/{{$Stock->category->catcode}}">{{$Stock->category->catcode}}</a></td>
                                    <td><a href="/admin/stock/{{$Stock->id}}">{{$Stock->stocknumber}}</a></td>
                                    <td>{{$Stock->stockname}}</td>
                                    <td>{{$Stock->quantity}}</td>
                                    <td>
                                    
                                        <a href="javascript:;" 
                                            class="add-modal btn btn-sm btn-success" 
                                            data-stocknumber="{{$Stock->stocknumber}}"
                                            data-stockid="{{$Stock->id}}"
                                            data-catcode="{{$Stock->category->catcode}}" 
                                            data-stockname="{{$Stock->stockname}}" 
                                            data-quantity="{{$Stock->quantity}}" 
                                            data-id="{{$Stock->id}}" >
                                            <i class="fa fa-plus"> 
                                        </i></a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
            </div>
        </div><!--col-->
        <div class="col-md-6 col-sm-12 col-lg-7">
            <div class="x_panel tile">
                <table class="table table-striped" id="rmsdetails">
                    <thead>
                        <tr>
                        <th>Category </th>
                        <th>Stock No</th>
                        <th>Description</th>
                        <th>Quantity</th>
                        <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($return_detail as $return)
                            <tr class="item{{$return->id}}">
                                <td>{{$return->cat_number}}</td>
                                <td>{{$return->stock->stocknumber}}</td>
                                <td>{{$return->stock->stockname}}</td>
                                <td>{{$return->quantity}}</td>
                                <td><a class='delete-modal btn btn-danger btn-small' data-id='{{$return->id}}'>
                                    <i class='fa fa-times'></i></a>
                                </td>
                            </tr>
                        @empty
                        <tr class="norecordriv"><td colspan="6" align="center"> <em>No Record</em></td></tr>
                        @endforelse
                    </tbody>
                </table>
            
                <div class="clearfix"></div>
                <div align="center">
                <button data-return_number="{{$return_data->return_number}}" class="btn btn-md btn-info confirm-modal" class="text-center"> Save</button>
                </div>
            </div>

        
    </div><!--row-->
</div>

<div id="stockModal" class="modal fade bs-example-modal-lg" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
               
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
              
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">
                    {{ csrf_field() }}
                    <input type="hidden" class="form-control" id="id">
                    <div class="row">
                        <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <label class="control-label" for="Stock No.">Stock No.</label>
                            <input type="text" class="form-control" name="stocknum" id="stocknum" readonly> 
                        </div>
                        <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <label class="control-label" for="Description">Description</label>
                            <input type="text" class="form-control" name="stockname" id="stockname" readonly> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
                            <label class="control-label">Category</label>
                            <input type="text" class="form-control" name="catcode" id="catcode" readonly> 
                        </div>
                        
                        <div class="form-group col-md-8 col-lg-8 col-sm-12 col-xs-12">
                            <label class="control-label">Quantity</label>
                            <input type="number" class="form-control" name="return_quantity" id="return_quantity"> 
                            <input type="hidden" class="form-control" name="return_number" id="return_number" value="{{$return_data->return_number}}"> 
                            <input type="hidden" class="form-control" name="stock_id" id="stock_id" > 
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn save_rms btn-primary" data-dismiss="modal" id="rivAdd">Add</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="deleteModal" class="modal fade bs-example-modal-lg" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
               
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel"></h4>
              
            </div>
            <div class="modal-body">
            <p>
            Are you sure you want to remove this item?
            </p>
            <form class="form-horizontal" role="form">
                {{ csrf_field() }}
                <input type="hidden" class="form-control" id="fid">
                
            </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn delete btn-danger" data-dismiss="modal">Remove</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="confirmModal" class="modal fade bs-example-modal-lg" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
               
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel"> Process RMS</h4>
              
            </div>
            <div class="modal-body">
            <p>
            Are you sure you want to process this RMS?
            </p>
            <form class="form-horizontal" role="form">
                {{ csrf_field() }}
                <input type="hidden" class="form-control" id="return_number_confirm">
                
            </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn confirmProcess btn-success" data-dismiss="modal">Process</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="consdeleteModal" class="modal fade bs-example-modal-lg" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
               
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
              
            </div>
            <div class="modal-body">
            <p>
            Are you sure you want to remove this concessionaire?
            </p>
            <form class="form-horizontal" role="form">
                    {{ csrf_field() }}
                    <input type="hidden" class="form-control" id="consid">
                 
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn delete-cons btn-danger" data-dismiss="modal">Remove</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$('#search').on('keyup',function(){
  $value=$(this).val();
  
  $.ajax({
    type : 'get',
    url : '{{URL::to('admin/rmsstocksearch')}}',
    data:{'search':$value
    },
    success:function(data){
      $('.stockbinresult').html(data);
    } 
  });
})


$('#addQuantity').on('keyup',function(){
    $quantity=$(this).val();
    $onhand = $('#onhandQuantity').val();
    if ($quantity <= $onhand ) {
        $('#rivAdd').prop('disabled', false);
    }else{
        $('#rivAdd').prop('disabled', true);        
    }
  console.log($quantity +' + ' + $onhand);
})
</script> 
<script type="text/javascript">
$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/rmsscript.js') }}"></script>
@endsection