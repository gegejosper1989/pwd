@extends('layouts.admin')

@section('content')

<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-6 col-sm-12 col-lg-4">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>Vehicle</h4>
                    
                </div>
               
                    {{ csrf_field() }}
                    
                    <label for="Vehicle Number">Vehicle Number</label>
                    <input type="text" name="vehiclenum"  class="form-control" id="vehiclenum">
                    <br>
                    <button class="btn btn-info btn-sm" id="btnsave" disabled>Save</button>
                <div class="clearfix"></div>
            </div>
        </div><!--col-->
        
        <div class="col-md-4 col-sm-12 col-lg-4">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>Vehicle Lists</h4>
                </div>
                <table class="table table-striped" id="vehicletable">
                    <thead>
                        <tr>
                        <th>Vehicle Number</th>
                        <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($dataVehicle as $Vehicle)
                        <tr class="item{{$Vehicle->id}}">
                            <td><a href="/admin/vehicle/{{$Vehicle->vehiclenum}}">{{$Vehicle->vehiclenum}}</a></td>
                            <td><button class="btn btn-success btn-sm edit-modal"
                                data-id="{{$Vehicle->id}}"
                                data-vehiclenum="{{$Vehicle->vehiclenum}}"
                                ><i class="fa fa-edit"></i></button></td>
                        </tr>
                        @endforeach
                        
                    </tbody>
                </table>
                <div class="clearfix"></div>
            </div>
        </div><!--col-->
    </div><!--row-->
</div>
<div id="vehicleModal" class="modal fade bs-example-modal-lg" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
               
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Update Vehicle</h4>
              
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">
                    {{ csrf_field() }}
                    <input type="hidden" class="form-control" id="fid">
                    <div class="row">
                        <div class="form-group col-md-6 col-lg-6 col-sm-12 col-xs-12">
                            <label class="control-label" for="Vehicle Number">Vehicle Number</label>
                            <input type="text" class="form-control" name="editVehicleNum" id="editVehicleNum"> 
                        </div>
                        
                    </div>
                    
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary editSave" data-dismiss="modal">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="deleteModal" class="modal fade bs-example-modal-lg" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
               
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Delete Vehicle</h4>
              
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">
                    {{ csrf_field() }}
                    <input type="hidden" class="form-control" id="delid">
                </form>
                <p>Are you sure you want to delete this vehicle?</p>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger delete" data-dismiss="modal">Delete</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/vehiclescript.js') }}"></script>
@endsection