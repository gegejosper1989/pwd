@extends('layouts.admin')

@section('content')
<script type="text/javascript">
$(document).ready( function() {
    $('#prDate').val(new Date().toDateInputValue());
});​
</script>
<div class="right_col" role="main">
    <form action="{{route('createPr')}}" method="post">
    {{ csrf_field() }}
    <div class="row">
    <div class="col-md-6 col-sm-12 col-lg-4">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>Create Purchase Request</h4>
                </div>
                <label for="Date">Date</label>
                <input type="date" class="form-control" id="prDate"  name="prDate" required>
                <label for="PR Number">PR Number</label>
                <?php 
                    $num = $prCount + 1;
                    if(strlen((string)$num) == 1) {
                        $pr = "00".$num;
                    }
                    elseif(strlen((string)$num) == 2) {
                        $pr = "0".$num;
                    }
                    elseif(strlen((string)$num) == 3){
                        $pr =  $num;
                    }
                ?>
                    
                <input type="text" class="form-control" id="prnumber" name="prnumber" value="PR-<?php echo date('y'); ?>-<?php echo date('m'); ?>-{{$pr}}" required>
                <label for="Mode of Payment">Mode of Payment</label>
                <select name="paymentmode" id="paymentmode" class="form-control" required>
                    <option value="After Delivery">After Delivery</option>
                    <option value="Check">Check</option>
                </select>
                <label for="Supplier">Supplier</label>
                <select name="supplier" id="supplier" class="form-control" required>
                    <option value=""></option>
                    @foreach($dataSupplier as $Supplier)
                        <option value="{{$Supplier->id}}">{{$Supplier->suppliername}}</option>
                    @endforeach
                </select> <br> <br>
                <input type="submit" class="btn btn-lg btn-info btn-sm" class="text-center" value="Create">
                <div class="clearfix"></div>
            </div>
            
        </div><!--col-->
        <div class="col-md-6 col-sm-12 col-lg-8">
        <div class="x_panel tile">
            <div class="x_title">
                <h4>Recent Purchase Request</h4>  
            </div>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                    <th>Date</th>
                    <th>PR Number</th>
                    <th>Supplier</th>
                    <th>Status</th>
                
                    </tr>
                </thead>
                <tbody>
                    @foreach($dataPrdetail as $Prdetail)
                    <tr>
                        <td>{{$Prdetail->prdate}}</td>
                        <td>
                            @if($Prdetail->prstatus == 'initial')
                            <a href="/admin/pr/create/{{$Prdetail->id}}">{{$Prdetail->prnum}}</a>
                            @else
                            <a href="/admin/pr-view/{{$Prdetail->prnum}}">{{$Prdetail->prnum}}</a>
                            @endif    
                       </td>
                        <td>{{$Prdetail->supplier->suppliername}}</td>
                        <td>{{$Prdetail->prstatus}}</td>
                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <a href="#" class="btn btn-sm btn-success pull-right"> View More</a> 
            <div class="clearfix"></div>
            </div>
        </div><!--col-->
        
    
    </div><!--row-->
    </form>
</div>

<script type="text/javascript">
$("#supplier").select2({
  tags: true
});
</script>
@endsection