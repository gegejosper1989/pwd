@extends('layouts.admin')

@section('content')

<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-6 col-sm-12 col-lg-4">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>Categories</h4>
                    
                </div>
                <label for="Category">Category Code</label>
                    {{ csrf_field() }}
                    <input type="text" name="catcode" class="form-control" id="catcode">
                    <label for="Category Name">Category Name</label>
                    <input type="text" name="catname"  class="form-control" id="catname">
                    <br>
                    <button class="btn btn-info btn-sm" id="btnsave" disabled>Save</button>
                <div class="clearfix"></div>
            </div>
        </div><!--col-->
        
        <div class="col-md-8 col-sm-12 col-lg-8">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>Category Lists</h4>
                    
                </div>
                <table class="table table-striped" id="categorytable">
                    <thead>
                        <tr>
                        <th>Category Code </th>
                        <th>Category Name</th>
                        <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($dataCategory as $Category)
                        <tr class="item{{$Category->id}}">
                            <td><a href="/admin/stocks/category/{{$Category->catcode}}">{{$Category->catcode}}</a></td>
                            <td><a href="/admin/stocks/category/{{$Category->catcode}}">{{$Category->catname}}</a></td>
                            <td><button class="btn btn-success btn-sm edit-modal"
                                data-id="{{$Category->id}}"
                                data-catcode="{{$Category->catcode}}"
                                data-catname="{{$Category->catname}}"
                                ><i class="fa fa-edit"></i></button></td>
                        </tr>
                        @endforeach
                        
                    </tbody>
                </table>
                <div class="clearfix"></div>
            </div>
        </div><!--col-->
    </div><!--row-->
</div>
<div id="categoryModal" class="modal fade bs-example-modal-lg" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
               
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Update Category</h4>
              
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">
                    {{ csrf_field() }}
                    <input type="hidden" class="form-control" id="fid">
                    <div class="row">
                        <div class="form-group col-md-6 col-lg-6 col-sm-12 col-xs-12">
                            <label class="control-label" for="Category Code">Category Code</label>
                            <input type="text" class="form-control" name="editCatCode" id="editCatCode"> 
                        </div>
                        <div class="form-group col-md-6 col-lg-6 col-sm-12 col-xs-12">
                            <label class="control-label" for="Category Name">Category Name</label>
                            <input type="text" class="form-control" name="editCatName" id="editCatName"> 
                        </div>
                        
                    </div>
                    
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary editSave" data-dismiss="modal">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="deleteModal" class="modal fade bs-example-modal-lg" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
               
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Delete Category</h4>
              
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">
                    {{ csrf_field() }}
                    <input type="hidden" class="form-control" id="delid">
                </form>
                <p>Are you sure you want to delete this category?</p>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger delete" data-dismiss="modal">Delete</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/categoryscript.js') }}"></script>
@endsection