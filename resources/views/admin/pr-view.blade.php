@extends('layouts.admin')

@section('content')

<div class="right_col" role="main">
    
    <div class="row">
        <div>
            <div class="title-header text-center" >
                <h3>PAGADIAN CITY WATER DISTRICT</h3>
                <p>Datoc St. Gatas Dist., Pagadian City</p>
                <p>Tel No. 2141-747-2144 Fax No. 2142-179</p>
                <h4>PURCHASE ORDER</h4>
            </div>
            
            <div class="x_panel tile col-md-12 col-lg-12">
                <!-- <div class="x_title">
                    <h4>Purchase Request #: {{$DetailPr->prnum}}</h4>
                </div> -->
                <div class="row">
                    <div class="col-lg-6 col-md-12"> 
                        <label for="Supplier">Supplier: </label> {{$DetailPr->supplier->suppliername}} <br>
                        <label for="Address">Address: </label> {{$DetailPr->supplier->address}} <br>

                    </div>
                    <div class="col-lg-6 col-md-12">
                        <label for="Payment"> Term of Payment: </label> {{$DetailPr->paymentmode}} <br>
                        <label for="Delivery">Deliver to: </label> PCWD <br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3"> 
                    <label for="PR"> P.O No.: </label> _______________ <br>
                    <label for="Tel"> Tel. No. / Fax No.: </label> _______________ <br>
                    </div>
                    <div class="col-lg-3"> 
                        <label for="PR"> P.O No.: </label>  <br>
                        <label for="Delivery">Date: </label> {{$DetailPr->prdate}} <br>
                    </div>
                    <div class="col-lg-6">
                        <label for="PR"> P.R No.: </label> {{$DetailPr->prnum}} <br>
                        <label for="Delivery">Date: </label> {{$DetailPr->prdate}} <br>
                    </div>
                </div>
                
                    <table class="table table-bordered" id="prdetails">
                        <thead>
                            <tr>
                            <th>Item </th>
                            <th>Qty</th>
                            <th>Unit</th>
                            <th>Description</th>
                            <th>Unit Price</th>
                            <th>Total Cost</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $item =1; 
                            $totalcost = 0;
                            ?>
                            @forelse($dataPrDetail as $prDetail)
                                <tr class="item{{$prDetail->id}}">
                                    <td>{{$item}}</td>
                                    <td>{{$prDetail->reqquantity}}</td>
                                    <td>{{$prDetail->stock->unit}}</td>
                                    <td>{{$prDetail->stock->stockname}}</td>
                                    <td>{{$prDetail->unitcost}}</td>
                                    <td>{{number_format($prDetail->reqquantity * $prDetail->unitcost, 2)}}</td>
                                
                                </tr>
                            
                            <?php 
                            $amount = $prDetail->reqquantity * $prDetail->unitcost;
                            $totalcost = $totalcost + $amount;
                            $item +=1; ?>
                            @empty
                            <tr class="norecordriv"><td colspan="6" align="center"> <em>No Record</em></td></tr>
                            @endforelse
                            <tr>
                                <td colspan="5" class="text-right">Total Cost</td>
                                <td>{{number_format($totalcost,2)}}</td>
                            </tr>
                        </tbody>
                    </table>
            <div class="terms">
                    <p>Please submit upon delivery copy of your invoice and delivery receipt showing our Purchase Order number together with your Taxpayer's Certificated, TIN, BIR Tax Clearance Certificate, & BIR Letter of Confirmation.</p>
                    <p>TERMS & CONDITIONS:</p>
                    <ol>
                        <li>Prices: F.O.B Pagadian City</li>
                        <li>Delinquency: Subject to penalty of 1/10 of 1% of the value of the item for each day of delay of delivery to be automatically deducted from total payables</li>
                        <li>Warranty against intrinsic defects: _____________________ from receipt of items.</li>
                    </ol>
                    <p>Please return duplicate copy after acknowledging receipt hereof.</p>
            </div>
            <div class="x_title">
            </div>
            <div class="signatories">
                <div class="row">
                    <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">Prepared by:
                        <br><br><br>
                         <strong>{{$data_pr->employee_prepared_by->fname}} {{$data_pr->employee_prepared_by->mname}} {{$data_pr->employee_prepared_by->lname}}</strong> <br>
                         <em>{{$data_pr->employee_prepared_by->position}}</em>
                    </div>
                    <div class="col-md-4  col-lg-4 col-sm-4 col-xs-4">Certified by:
                        <br><br><br>
                        <strong>{{$data_pr->employee_certified_by->fname}} {{$data_pr->employee_certified_by->mname}} {{$data_pr->employee_certified_by->lname}}</strong> <br>
                         <em>{{$data_pr->employee_certified_by->position}} </em>
                    </div>
                    <div class="col-md-4  col-lg-4 col-sm-4 col-xs-4">Approved by:
                    <br><br><br>
                         <strong>{{$data_pr->employee_approved_by->fname}} {{$data_pr->employee_approved_by->mname}} {{$data_pr->employee_approved_by->lname}}</strong> <br>
                         <em>{{$data_pr->employee_approved_by->position}}</em>
                    </div>
                </div>
            </div>
            <div class="">
                <div class="row">
                    <div class="col-md-8 col-lg-8 col-sm-8 col-xs-7">
                    </div>
                    <div class="col-md-4  col-lg-4 col-sm-4 col-xs-5">
                    <br><br><br>
                         <strong>Recieved Original Copy:</strong> <br>
                         <br> 

                         ___________________________________________________ <br> 
                         <em>
                             Supplier/Authorized Representative (Signature & Date)
                         </em>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <br>
            
            <div class="col-md-4 col-sm-12 col-lg-4 no-print">
                <label for="Date">Status: </label> {{strtoupper($DetailPr->prstatus)}}
            </div>
            <div class="clearfix"></div>
            <br>
            <button class="btn btn-success btn-sm no-print" id="btn-print"><i class="fa fa-print"></i> Print</button>
            <a href="/admin/pr" class="btn btn-sm btn-info no-print"><i class="fa fa-reply"></i> Back</a>
                <div class="clearfix"></div>
            </div>
    </div><!--row-->
</div>
<script type="text/javascript">
$('#btn-print').on('click',function(){
    $('body').removeClass('nav-sm').addClass('nav-md');
    window.print();
})
</script> 
<script src="{{ asset('js/prscript.js') }}"></script>
@endsection