@extends('layouts.admin')

@section('content')

<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-lg-12">
            <div class="x_content"> 
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <strong>Reseting deparment yearly budget successful.</strong> <a href="/admin/budget">Back</a>
                  </div>
            </div><!--x_content-->
        </div><!--col-->
        
    </div><!--row-->
</div>
@endsection