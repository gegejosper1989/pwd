@extends('layouts.admin')

@section('content')

<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-6 col-sm-12 col-lg-6">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>Recent Request and Issue Voucher</h4>
                </div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th>Date </th>
                        <th>RIV No.</th>
                        <th>Department</th>
                        <th>Status</th>
                        </tr>
                    </thead>
                    <tbody class="rivresult">
                        @foreach($dataRecentRiv as $rivdetails)
                            <tr>
                                <td>
                                @if($rivdetails->status == 'initial')
                                <a href="/admin/riv/create/{{$rivdetails->id}}">{{$rivdetails->date}}</a>
                                @else 
                                <a href="/admin/riv/view/{{$rivdetails->id}}">{{$rivdetails->date}}</a>
                                @endif
                                </td>
                                <td>
                                @if($rivdetails->status == 'initial')
                                <a href="/admin/riv/create/{{$rivdetails->id}}">{{$rivdetails->rivnumber}}</a>
                                @else 
                                <a href="/admin/riv/view/{{$rivdetails->rivnumber}}">{{$rivdetails->rivnumber}}</a>
                                @endif
                                </td>
                                <td>{{$rivdetails->department}}</td>
                                
                                <td>{{$rivdetails->status}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            <a href="/admin/riv/report" class="btn btn-sm btn-success pull-right"> View More</a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            
            </div><!--x_content-->
        </div><!--col-->
        <div class="col-md-6 col-sm-12 col-lg-6">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>Recent Return Material Slip</h4>
                </div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th>Date </th>
                        <th>RMS No</th>
                        <th>Status</th>
                        <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($return_data as $return)
                        <tr>
                            <td>{{$return->return_date}}</td>
                            <td>
                            @if($return->status == 'initial')
                            <a href="/admin/rms/create/{{$return->return_number}}">{{$return->return_number}}</a>
                            @else 
                            <a href="/admin/rms/view/{{$return->return_number}}">{{$return->return_number}}</a></a>
                            @endif    
                            </td>
                            <td>{{$return->status}}</td>
                            
                            <td><a href="/admin/rms/view/{{$return->return_number}}" class="btn btn-success btn-sm"><i class="fa fa-search"></i></a></td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="4"><em>No Record</em></td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            <a href="#" class="btn btn-sm btn-success pull-right"> View More</a>
                <div class="clearfix"></div>
            </div>
        </div><!--col-->
    </div>
    <div class="row"> 
        <div class="col-md-6 col-sm-12 col-lg-6">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>Out of Stocks</h4>
                </div>
                <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                    <th>Category </th>
                    <th>Stock No</th>
                    <th>Discription</th>
                    <th>Current Quantity</th>
                    <th>Ordering Point</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($data_out_of_stocks as $out_of_stock)
                    <tr>
                        <td>{{$out_of_stock->catcode}}</td>
                        <td>{{$out_of_stock->stocknumber}}</td>
                        <td>{{$out_of_stock->stockname}}</td>
                        <td>{{$out_of_stock->quantity}}</td>
                        <td>{{$out_of_stock->orderingpoint}}</td>  
                    </tr>
                    @empty
                    <tr>
                        <td colspan="5"><em>No Record</em></td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
            <a href="/admin/pr" class="btn btn-sm btn-danger pull-right"> Create Purchase Request</a>
                <div class="clearfix"></div>
            </div>

        </div><!--col-->
        <div class="col-md-6 col-sm-12 col-lg-6">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>Recent Purchase Request</h4>
                    
                </div>
                <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                    <th>Date</th>
                    <th>PR Number</th>
                    <th>Supplier</th>
                    <th>Status</th>
                
                    </tr>
                </thead>
                <tbody>
                    @foreach($dataPrdetail as $Prdetail)
                    <tr>
                        <td>{{$Prdetail->prdate}}</td>
                        <td>
                            @if($Prdetail->prstatus == 'initial')
                            <a href="/admin/pr/create/{{$Prdetail->id}}">{{$Prdetail->prnum}}</a>
                            @else
                            <a href="/admin/pr-view/{{$Prdetail->prnum}}">{{$Prdetail->prnum}}</a>
                            @endif    
                       </td>
                        <td>{{$Prdetail->supplier->suppliername}}</td>
                        <td>{{$Prdetail->prstatus}}</td>
                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <a href="#" class="btn btn-sm btn-success pull-right"> View More</a> <a href="#" class="btn btn-sm btn-info"> Create PR</a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            
            </div><!--x_content-->
        </div><!--col-->
    </div><!--row-->
</div>
@endsection