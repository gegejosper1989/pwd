@extends('layouts.department')

@section('content')

<div class="right_col" role="main">
    <form action="{{route('create_riv')}}" method="post">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-md-4 col-sm-12 col-lg-4">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>Recent RIV - {{$department}} </h4>
                </div>

                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th>Date </th>
                        <th>RIV No.</th>
                        <th>Department</th>
                        <th>Status</th>
                        </tr>
                    </thead>
                    <tbody class="rivresult">
                        @foreach($dataRecentRiv as $rivdetails)
                            <tr>
                                <td>
                                @if($rivdetails->status == 'initial')
                                <a href="/admin/riv/create/{{$rivdetails->id}}">{{$rivdetails->date}}</a>
                                @else 
                                <a href="/admin/riv/view/{{$rivdetails->id}}">{{$rivdetails->date}}</a>
                                @endif
                                </td>
                                <td>
                                @if($rivdetails->status == 'initial')
                                <a href="/admin/riv/create/{{$rivdetails->id}}">{{$rivdetails->rivnumber}}</a>
                                @else 
                                <a href="/admin/riv/view/{{$rivdetails->rivnumber}}">{{$rivdetails->rivnumber}}</a>
                                @endif
                                </td>
                                <td>{{$rivdetails->department}}</td>
                                
                                <td>{{$rivdetails->status}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <a href="/admin/riv/report" class="btn btn-lg btn-info btn-sm">View More</a>
            </div>
        </div>
        <div class="col-md-8 col-sm-12 col-lg-8">
            <div class="x_panel tile">
                <div class="x_title">
                    <h4>Create RIV - {{$department}}</h4>
                </div>
                <div class="col-md-4 col-sm-12 col-lg-6">
                <label for="Date">Date</label>
                <input class="form-control" type="date" name="rivdate" id="today" required>
                </div>
                
                <div class="col-md-4 col-sm-12 col-lg-6">
                <label for="Responsibility">Responsibility Center</label>
                <input class="form-control" type="text" name="department" id="department" value="{{$department}}" readonly>
                </div>
                
            <div class="clearfix"></div>
            <br>
           
            <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                <label for="Purpose">Purpose</label>
                <textarea name="purpose" id="purpose" cols="30" rows="4" class="form-control" required></textarea>
                
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group">
               
                <label for="Vehicle Number">Vehicle Number  <em>(Optional)</em></label>
                <select name="vehicle" id="vehicle" class="form-control">
                    <option value=""></option>
                    @foreach($dataVehicle as $Vehicle)
                        <option value="{{$Vehicle->vehiclenum}}">{{$Vehicle->vehiclenum}}</option>
                    @endforeach
                </select>
                <label for="Work Order">Work Order  <em>(Optional)</em></label>
                <input type="text" class="form-control" id="workorder" placeholder="Work Order">
            </div>
            <br>
            <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12 text-center">
            <br>
            <input type="submit" class="btn btn-lg btn-info btn-sm" class="text-center" value="Create">
            
            </div>

        </div><!--col-->
    </div><!--row-->
    </div>
    </form>
</div>

<script type="text/javascript">
$("#vehicle").select2({
  tags: true
});
</script>
@endsection