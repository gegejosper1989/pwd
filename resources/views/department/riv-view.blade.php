@extends('layouts.department')

@section('content')

<div class="right_col" role="main">
<div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
            <div class="title-header text-center" >
                <h3>PAGADIAN CITY WATER DISTRICT</h3>
                <p>Datoc St. Gatas Dist., Pagadian City</p>
                <p>Tel No. 2141-747-2144 Fax No. 2142-179</p>
                <h4>REQUISITION AND ISSUE VOUCHER</h4> <br>
            </div>
            <div class="col-xs-8">
            <label for="Purpose">Purpose : </label> 
                {{$rivDetail->purpose}} <br>
                <label for="Vehicle Number">Vehicle Number  <em>(Optional) : </em></label> {{$rivDetail->vehiclenum}} <br>
                <label for="Work Order">Work Order  <em>(Optional) : </em></label> {{$rivDetail->workorder}}
            </div>
            
            <div class="col-xs-4">
                <label for="">RIV No.</label> {{$rivDetail->rivnumber}} <br>
                <label for="Date">Date : </label> {{$rivDetail->date}} <br>
                <label for="Responsibility">Responsibility Center : </label> {{$rivDetail->department}} <br>
            </div>
        </div><!--col-->
    </div><!--row-->
    <div class="row">
        <div class="col-md-12 col-sm-12 col-lg-12">
            <div class="x_panel tile">
                <table class="table table-bordered" id="rivdetails">
                <thead>
                    <tr>
                    <th>Bal. On Hand </th>
                    <th>QTY</th>
                    <th>Unit</th>
                    <th>Article</th>
                    <th>QTY</th> 
                    <th>Stock No.</th> 
                    <th>Unit Price</th>
                    <th>Total Value</th>
                    <th>Acct #</th>
                    <th>Amount</th>
                    </tr>
                </thead>
                <tbody>                  
                    @forelse($dataRivDetail as $rivDetail)
                        <tr class="item{{$rivDetail->id}}">
                            <td>{{$rivDetail->stockdetails->quantity}}</td>
                            <td>{{$rivDetail->reqquantity}}</td>
                            <td>{{$rivDetail->stockdetails->unit}}</td>
                            <td>{{$rivDetail->stockdetails->stockname}}</td>
                            <td>{{$rivDetail->stockdetails->quantity}}</td>
                            <td>{{$rivDetail->stockdetails->stocknumber}}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    @empty
                    <tr class="norecordriv"><td colspan="6" align="center"> <em>No Record</em></td></tr>
                    @endforelse
                </tbody>
            </table>
            </div>
        
            <div class="x_panel tile">
            <div class="clearfix"></div>
                 
            <table class="table table-striped" id="rivconcessionaire">
                <thead>
                    <tr>
                    <th>Name </th>
                    <th>Address</th>
                    <th>Meter Number</th>
                    <th>Incharge </th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($dataConsessionaire as $Consessionaires)
                    <tr class="cons-item{{$Consessionaires->id}}">
                        <td>{{$Consessionaires->fullname}}</td>
                        <td>{{$Consessionaires->address}}</td>
                        <td>{{$Consessionaires->meternum}}</td> 
                        <td>{{$Consessionaires->incharge}}</td> 
                    </tr>
                    @empty
                    <tr class="norecordcons"><td colspan="5" align="center"> <em>No Record</em></td></tr>
                    @endforelse
                    

                </tbody>
            </table>
               
        </div><!--col-->
        
    <div class="row">
        <div class="col-xs-3">
            <p>Requisition Officer:</p> 
            <br><br>
            <span style="text-decoration:underline">{{$data_riv->employee_request_officer->fname}} {{$data_riv->employee_request_officer->mname}} {{$data_riv->employee_request_officer->lname}}</span><br>
            {{$data_riv->employee_request_officer->position}}

            <br><br><br>
            <p>Approved for Release:</p> 
            <br><br>
            <span style="text-decoration:underline">{{$data_riv->employee_approved_release->fname}} {{$data_riv->employee_approved_release->mname}} {{$data_riv->employee_approved_release->lname}}</span><br>
            {{$data_riv->employee_approved_release->position}}
        </div>
        <div class="col-xs-4">
        
        </div>
        <div class="col-xs-5">
            <p><em>Recieved the supplies shown above as issued</em></p> 
            <br><br><br>

            
            <em>Signature of person receiving supplies</em>
            <br><br><br>
            <p>Issued by:</p> 
            <br><br>
            <span style="text-decoration:underline">{{$data_riv->employee_issued_by->fname}} {{$data_riv->employee_issued_by->mname}} {{$data_riv->employee_issued_by->lname}}</span><br>
            Storekeeper
            <br><br><br>
            <p>Posted by:</p> 
            <br>
            <span style="text-decoration:underline">{{$data_riv->employee_posted_by->fname}} {{$data_riv->employee_posted_by->mname}} {{$data_riv->employee_posted_by->lname}}</span><br>
            Bookkeeper
        </div>
    </div>
    </div><!--row-->

    <div class="clearfix"></div>
    <br>
    <div align="center">
    <button id="btn-print" class="btn btn-success btn-sm no-print"
    ><i class="fa fa-print"></i> Print</button>
    </div>
</div>
<script type="text/javascript">
$('#btn-print').on('click',function(){
    $('body').removeClass('nav-sm').addClass('nav-md');
    window.print();
})
</script> 

<script src="{{ asset('js/app.js') }}"></script>
@endsection