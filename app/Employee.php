<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    //
    public function department_details()
    {
        return $this->belongsTo('App\Department','department','depcode');
    }
}
