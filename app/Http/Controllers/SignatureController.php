<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Signature_pr;
use App\Signature_riv;
use App\Signature_rms;
use App\Signature_rr;
use Illuminate\Support\Facades\Log;

class SignatureController extends Controller
{
    //
    public function signatures_rms(Request $req){
        $update_signatures_rms = Signature_rms::where('id', '=', $req->rms_id)
                    ->update(['approved_by' => $req->approved_by ,
                        'approved_return' => $req->approved_return,
                        'posted_bin_card' => $req->posted_bin_card,
                        'posted_stock_card' => $req->posted_stock_card,
                        'accounted_by' => $req->accounted_by
                    ]);
        return redirect()->back()->with('success_rms','RMS Signatures successfully updated!');            
    }

    public function signatures_rr(Request $req){
        $update_signatures_rr = Signature_rr::where('id', '=', $req->rr_id)
                    ->update(['member_1' => $req->member_1 ,
                        'member_2' => $req->member_2,
                        'team_leader' => $req->team_leader,
                        'accepted_by' => $req->accepted_by,
                        'certified_correct' => $req->certified_correct,
                        'posted_bin_card' => $req->posted_bin_card,
                        'costed_by' => $req->costed_by,
                        'posted_stock_card' => $req->posted_stock_card
                    ]);
        return redirect()->back()->with('success_rr','RR Signatures successfully updated!');            
        
    }
    public function signatures_riv(Request $req){
        $update_signatures_riv = Signature_riv::where('id', '=', $req->riv_id)
                    ->update(['request_officer' => $req->request_officer ,
                        'approved_release' => $req->approved_release,
                        'issued_by' => $req->issued_by,
                        'posted_by' => $req->posted_by
                    ]);
        return redirect()->back()->with('success_riv','RIV Signatures successfully updated!');            
        
    }
    public function signatures_pr(Request $req){
        $update_signatures_rr = Signature_pr::where('id', '=', $req->pr_id)
                    ->update(['prepared_by' => $req->prepared_by ,
                        'certified_by' => $req->certified_by,
                        'approved_by' => $req->approved_by
                    ]);
        return redirect()->back()->with('success_pr','PR Signatures successfully updated!');            
    }
}
