<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stock;
use App\Category;
use App\Budget;
use App\Department;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
class StockController extends Controller
{

    public function stocksearchlive(Request $request){
        if($request->ajax())
        {
            $dataStock = Stock::where('stockname','LIKE','%'.$request->search.'%')
                ->get();
            $output="";
            if($dataStock)
            {
                foreach ($dataStock as $Stock) {
                        $output .='<tr class="row'.$Stock->id.'">
                            <td><a href="/admin/stocks/category/'.$Stock->catcode.'">'.$Stock->catcode.'</a></td>
                            <td><a href="/admin/stock/'.$Stock->id.'">'.$Stock->stocknumber.'</td>
                            <td>'.$Stock->stockname.'</td>
                            <td>'.$Stock->quantity.'</td>
                            <td>'.$Stock->unit.'</td>
                            <td>'.$Stock->orderingpoint.'</td>
                            <td><button class="btn edit-modal btn-success btn-sm" 
                                data-id="'.$Stock->id.'"
                                data-catcode="'.$Stock->catcode.'"
                                data-stocknumber="'.$Stock->stocknumber.'"
                                data-stockname="'.$Stock->stockname.'"
                                data-quantity="'.$Stock->quantity.'"
                                data-unit="'.$Stock->unit.'"
                                data-ordering="'.$Stock->orderingpoint.'"
                                ><i class="fa fa-edit"></i>Edit</button>
                                <a href="/admin/bincard/'.$Stock->id.'" class="btn btn-info btn-sm"><i class="fa fa-folder"></i> Bin Card</a> 
                                <a href="/admin/stock/'.$Stock->id.'" class="btn btn-warning btn-sm"><i class="fa fa-search"></i> View Stock</a> 
                            </td>';
                        $output .='</tr>'; 
                }
                return Response($output);
            }
        }
    }
    public function stocksearchbudget(Request $request){
        if($request->ajax())
        {
            $dataStock = Stock::where('stockname','LIKE','%'.$request->search.'%')
                ->get();
            $output="";
            if($dataStock)
            {  
                foreach ($dataStock as $StockResult) {
                    $dataBudget = Budget::with('stock')->where('stocknumber', '=', $StockResult->id)->get();
                    foreach($dataBudget as $Stock){
                        $output .='<tr class="row'.$Stock->id.'">
                        <td><a href="/admin/stocks/category/'.$Stock->category.'">'.$Stock->category.'</a></td>
                        <td><a href="/admin/stock/'.$Stock->id.'">'.$Stock->stocknumber.'</td>
                        <td>'.$Stock->stock->stockname.'</td>
                        <td>'.$Stock->department.'</td>
                        <td>'.$Stock->budget.'</td>
                        <td>'.$Stock->consumed.'</td>
                        <td><button class="btn edit-modal btn-success btn-sm" 
                            data-id="'.$Stock->id.'"
                            data-catcode="'.$Stock->category.'"
                            data-stocknumber="'.$Stock->stock->stocknumber.'"
                            
                            data-stockid="'.$Stock->stock->id.'"
                            data-stockname="'.$Stock->stock->stockname.'"
                            data-consumed="'.$Stock->consumed.'"
                            data-department="'.$Stock->department.'"
                            data-budget="'.$Stock->budget.'"
                            ><i class="fa fa-edit"></i></button>
                        </td>';
                        $output .='</tr>'; 
                    }     
                }
                return Response($output);
            }
        }
    }
    public function stocksearchbincard(Request $request){
        if($request->ajax())
        {
            $dataStock = Stock::where('stockname','LIKE','%'.$request->search.'%')
                ->get();
            $output="";
           
            foreach ($dataStock as $Stock) {
                $output .='<tr>
                    <td><a href="/admin/stocks/category/'.$Stock->catcode.'">'.$Stock->catcode.'</a></td>
                    <td><a href="/admin/stock/'.$Stock->id.'">'.$Stock->stocknumber.'</td>
                    <td>'.$Stock->stockname.'</td><td>
                    <a href="/admin/bincard/'.$Stock->id.'" class="btn btn-info btn-sm"><i class="fa fa-search"></i></a> 
                    </td>';
                $output .='</tr>'; 
            }
            return Response($output);
          
        }
    }
    public function rivstocksearch(Request $request){
        if($request->ajax())
        {
            $dataStock = Stock::where('stockname','LIKE','%'.$request->search.'%')
                ->get();
            $output="";
           
            if($dataStock)
            {  
                foreach ($dataStock as $StockResult) {
                    $dataBudget = Budget::with('stock')->where('department', '=', $request->department)->where('stocknumber', '=', $StockResult->id)->get();
                    foreach($dataBudget as $Stock){
                        $output .='<tr><td><a href="/admin/stocks/category/'.$Stock->category.'">'.$Stock->category.'</a></td>
                            <td><a href="/admin/stock/'.$Stock->stocknumber.'">'.$Stock->stock->stocknumber.'</td>
                            <td>'.$Stock->stock->stockname.'</td>
                            <td>'.$Stock->stock->quantity.'</td>
                            <td>'.$Stock->budget.'</td>
                            <td>'.$Stock->consumed.'</td>
                            <td></td><td>';
                            if($Stock->stock->quantity > 0) {
                                if($Stock->budget > $Stock->consumed){
                                    $output.='<a href="javascript:;" class="add-modal btn btn-sm btn-success" 
                                    data-stocknumber="'.$Stock->stock->stocknumber.'"
                                    data-stock_id="'.$Stock->stock->id.'" 
                                    data-catcode="'.$Stock->category.'" 
                                    data-stockname="'.$Stock->stock->stockname.'" 
                                    data-quantity="'.$Stock->stock->quantity.'" 
                                    data-id="'.$Stock->stock->id.'">
                                    <i class="fa fa-plus"></i>
                                    </a>';
                                }    
                            }
                        $output .='</td></tr>';
                    }
                } 
            }
            return Response($output);
          
        }
    }
    public function prstocksearch(Request $request){
        if($request->ajax())
        {
            $dataStock = Stock::where('stockname','LIKE','%'.$request->search.'%')
                ->get();
            $output="";
           
            if($dataStock)
            {  
                foreach ($dataStock as $Stock) {
                    // $dataBudget = Budget::with('stock')->where('department', '=', $request->department)->where('stocknumber', '=', $StockResult->id)->get();
                    // foreach($dataBudget as $Stock){
                        $output .='<tr><td><a href="/admin/stocks/category/'.$Stock->category->catcode.'">'.$Stock->category->catcode.'</a></td>
                            <td><a href="/admin/stock/'.$Stock->stocknumber.'">'.$Stock->stocknumber.'</td>
                            <td>'.$Stock->stockname.'</td>
                            <td>'.$Stock->quantity.'</td>
                
                            <td></td><td>';
                            // if($Stock->stock->quantity > 0) {
                                // if($Stock->budget > $Stock->consumed){
                                    $output.='<a href="javascript:;" class="add-modal btn btn-sm btn-success" 
                                    data-stocknumber="'.$Stock->stocknumber.'"
                                    data-catcode="'.$Stock->category->catcode.'" 
                                    data-stockname="'.$Stock->stockname.'" 
                                    data-quantity="'.$Stock->quantity.'" 
                                    data-id="'.$Stock->id.'">
                                    <i class="fa fa-plus"></i>
                                    </a>';
                                // }    
                            // }
                        $output .='</td></tr>';
                    }
                } 
            }
            return Response($output);
    }
    public function rrstocksearch(Request $request){
        if($request->ajax())
        {
            $dataStock = Stock::where('stockname','LIKE','%'.$request->search.'%')
                ->get();
            $output="";
           
            if($dataStock)
            {  
                foreach($dataStock as $Stock){
                    $output .='<tr><td><a href="/admin/stocks/category/'.$Stock->catcode.'">'.$Stock->catcode.'</a></td>
                        <td><a href="/admin/stock/'.$Stock->id.'">'.$Stock->stocknumber.'</td>
                        <td>'.$Stock->stockname.'</td>
                        <td>'.$Stock->quantity.'</td>
                        <td></td><td>';
                    $output.='<a href="javascript:;" class="add-modal btn btn-sm btn-success" 
                    data-stocknumber="'.$Stock->stocknumber.'"
                    data-catcode="'.$Stock->category.'" 
                    data-stockname="'.$Stock->stockname.'" 
                    data-quantity="'.$Stock->quantity.'" 
                    data-id="'.$Stock->id.'">
                    <i class="fa fa-plus"></i>
                    </a>';
                    $output .='</td></tr>';
                }
            }
            return Response($output);
          
        }
    }
    public function editStock(Request $req){

        $updateStock = Stock::where('id', '=', $req->id)
                    ->update(['catcode' => $req->category,
                    'stocknumber' => $req->stocknum,
                    'stockname' => $req->description,
                    'quantity' => $req->quantity,
                    'unit' => $req->unit,
                    'orderingpoint' => $req->orderingpoint
                    ]);
        $stockArray = array(['id' => $req->id,
        'catcode' => $req->category,
        'stocknumber' => $req->stocknum,
        'stockname' => $req->description,
        'quantity' => $req->quantity,
        'unit' => $req->unit,
        'orderingpoint' => $req->orderingpoint
        ]);
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::notice($name.' updated the stock '.$req->description);

        //dd($stockArray);
        return response($stockArray);

    }

    public function setreorder(Request $req){
        $updateStock = Stock::where('id', '=', $req->id)
                    ->update(['orderingpoint' => $req->orderingpoint
                    ]);
        $stockArray = array(['id' => $req->id,
        'catcode' => $req->catcode,
        'stocknumber' => $req->stocknumber,
        'stockname' => $req->description,
        'orderingpoint' => $req->orderingpoint
        ]);

        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::info($name.' set ordering point for stock '.$req->description);
        return response()->json($stockArray);
    }

    public function viewstocksbycategory($catcode){
        
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::info($name.' viewed category '.$catcode);
        $dataStock = Stock::where('catcode', '=', $catcode)->with('category')->paginate(50);
        $dataCategory = Category::get();
        return view('admin.stocks-category', compact('dataStock', 'dataCategory'));
    }
    public function searchstocksbycategory(Request $req){
        $dataStock = Stock::where('catcode', '=', $req->selectcategory)->with('category')->paginate(50);
        $dataCategory = Category::get();
        $category = $req->selectcategory;
        $categoryDetail = Category::where('catcode', '=', $category)->first();
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::info($name.' viewed stock category '.$category);
        return view('admin.stocks-category', compact('dataStock', 'dataCategory', 'categoryDetail'));
    }

    public function addStock(Request $req) {
        $dataStock = new Stock();
        $dataStock->catcode = $req->stockcat;
        $dataStock->stocknumber = $req->stocknumber;
        $dataStock->stockname = $req->description;
        $dataStock->orderingpoint = $req->orderingpoint;
        $dataStock->quantity = $req->quantity;
        $dataStock->unit = $req->unit;
        $dataStock->save();
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::info($name.' added new stock '.$req->description);

        return response()->json($dataStock);
    }

    public function rmsstocksearch(Request $request){
        if($request->ajax())
        {
            $dataStock = Stock::where('stockname','LIKE','%'.$request->search.'%')
                ->get();
            $output="";
           
            if($dataStock)
            {  
                foreach($dataStock as $Stock){
                    $output .='<tr><td><a href="/admin/stocks/category/'.$Stock->catcode.'">'.$Stock->catcode.'</a></td>
                        <td><a href="/admin/stock/'.$Stock->id.'">'.$Stock->stocknumber.'</td>
                        <td>'.$Stock->stockname.'</td>
                        <td>'.$Stock->quantity.'</td>
                        <td></td><td>';
                    $output.='<a href="javascript:;" class="add-modal btn btn-sm btn-success" 
                    data-stocknumber="'.$Stock->stocknumber.'"
                    data-catcode="'.$Stock->category.'" 
                    data-stockid="'.$Stock->id.'" 
                    data-stockname="'.$Stock->stockname.'" 
                    data-quantity="'.$Stock->quantity.'" 
                    data-id="'.$Stock->id.'">
                    <i class="fa fa-plus"></i>
                    </a>';
                    $output .='</td></tr>';
                }
            }
            return Response($output);
          
        }
    }
}
