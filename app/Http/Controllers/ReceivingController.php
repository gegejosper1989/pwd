<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Receiving;
use App\Receivingitem;
use App\Pritem;
use App\Bincard;
use App\Prdetail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Stock;
use App\Signature_rr;
class ReceivingController extends Controller
{
    //
    public function create (Request $req){
        //dd($req);
        $dataRR = new Receiving();
        $dataRR->rdate = $req->rdate;
        $dataRR->rrnumber = $req->rrnumber;
        $dataRR->pr_number = $req->pr_number;
        $dataRR->supplier = $req->supplier;
        $dataRR->address = $req->supaddress;
        $dataRR->deliverynum = $req->drnum;
        $dataRR->chargenum = $req->chargeinvoicenum;
        $dataRR->ordertype = $req->ordertype;
        $dataRR->status = 'initial';
        $dataRR->save();

        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::notice($name.' created Receiving Report with RR #'.$req->rrnumber);

        return redirect('/admin/rr/create/'.$dataRR->id);
    }
    public function createRRItems($rrid){
        $rrReceiving = Receiving::where('id', '=', $rrid)->first();
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        $dataStock = Stock::take(20)->get();
        $rr_items = Receivingitem::with('stock')->where('rrnumber', '=', $rrReceiving->rrnumber)->get();
        $pr_item = Pritem::with('stock')->where('prnumber', '=', $rrReceiving->pr_number)->get();
        Log::notice($name.' created Receiving Report details for rr #'.$rrid);
        
        return view('admin.rr-create', compact('dataStock', 'rrReceiving', 'pr_item', 'rr_items'));

    }

    public function rr_items_add (Request $req){
          
        $data_rr = new Receivingitem();
        $data_rr->rrnumber = $req->rr_number;
        $data_rr->catcode = $req->catcode;
        $data_rr->stocknumber = $req->stockid;
        $data_rr->description = $req->description;
        $data_rr->quantity = $req->rec_quantity;
        $data_rr->deliverytype = $req->order_type;
        $data_rr->unitcost = $req->unit_cost;
        $data_rr->status = 'initial';
        $data_rr->save();
        $data_rr->stockid = $req->stocknumber;
        return response($data_rr);
    }

    public function rr_items_delete(Request $req)
    {
        Receivingitem::find($req->id)->delete();
        return response($req->id);
    }

    public function rr_save_partial($rrnumber){
        $rrReceiving = Receiving::where('rrnumber', '=', $rrnumber)->first();
        $rr_item_data = Receivingitem::where('rrnumber', '=', $rrnumber)->where('status', '=', 'initial')->get();
        foreach($rr_item_data as $item_data){
            $item_data->quantity;
            $data_stock = Stock::where('id', '=', $item_data->stocknumber)->first();
            $new_quantity = $item_data->quantity + $data_stock->quantity;
            $data_bincard = new Bincard();
            $data_bincard->stockid = $item_data->stocknumber;
            $data_bincard->stockcategoryid = $item_data->catcode;
            $data_bincard->quantity = $item_data->quantity;
            $data_bincard->type = 'RECEIVED';
            $data_bincard->unitcost = $item_data->unitcost;
            $data_bincard->reference = $rrnumber;
            $data_bincard->department = 'N/A';
            $data_bincard->balance = $new_quantity;
            $data_bincard->save();
            $update_rr_item = Receivingitem::where('id', '=', $item_data->id)
                    ->update(['status' => 'partial']);
            $update_stock_quantity = Stock::where('id', '=', $item_data->stocknumber)
                    ->update(['quantity' => $new_quantity]);
        }
        $update_pr_detail = Prdetail::where('prnum', '=', $rrReceiving->pr_number)
                    ->update(['prstatus' => 'partial']);
        $update_receiving = Receiving::where('rrnumber', '=', $rrnumber)
                    ->update(['status' => 'partial']);
        $rrReceiving = Receiving::where('rrnumber', '=', $rrnumber)->first();
        return redirect('/admin/rr/rr-view/'.$rrnumber);
    }

    public function rr_save($rrnumber){
        $rrReceiving = Receiving::where('rrnumber', '=', $rrnumber)->first();
        $rr_item_data = Receivingitem::where('rrnumber', '=', $rrnumber)->where('status', '=', 'initial')->get();
        foreach($rr_item_data as $item_data){
            $item_data->quantity;
            $data_stock = Stock::where('id', '=', $item_data->stocknumber)->first();
            $new_quantity = $item_data->quantity + $data_stock->quantity;
            $data_bincard = new Bincard();
            $data_bincard->stockid = $item_data->stocknumber;
            $data_bincard->stockcategoryid = $item_data->catcode;
            $data_bincard->quantity = $item_data->quantity;
            $data_bincard->type = 'RECEIVED';
            $data_bincard->unitcost = $item_data->unitcost;
            $data_bincard->reference = $rrnumber;
            $data_bincard->department = 'N/A';
            $data_bincard->balance = $new_quantity;
            $data_bincard->save();
            $update_rr_item = Receivingitem::where('id', '=', $item_data->id)
                    ->update(['status' => 'received']);
            $update_stock_quantity = Stock::where('id', '=', $item_data->stocknumber)
                    ->update(['quantity' => $new_quantity]);
        }
        $update_pr_detail = Prdetail::where('prnum', '=', $rrReceiving->pr_number)
                    ->update(['prstatus' => 'received']);
        $update_receiving = Receiving::where('rrnumber', '=', $rrnumber)
                    ->update(['status' => 'received']);
        $rrReceiving = Receiving::where('rrnumber', '=', $rrnumber)->first();
        return redirect('/admin/rr/rr-view/'.$rrnumber);
    }

    public function rr_view($rr_number){
        $data_rr_info = Receiving::where('rrnumber', '=', $rr_number)->with('pr_detail.supplier')->first();
        $data_rr_item = Receivingitem::where('rrnumber', '=', $rr_number)->with('stock')->get();
        //dd($data_rr);
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::notice($name.' view RR details for RR #'.$rr_number);
        $data_rr = Signature_rr::with('employee_member_1', 'employee_member_2', 'employee_team_leader', 'employee_accepted_by', 'employee_certified_correct', 'employee_posted_bin_card', 'employee_posted_stock_card', 'employee_costed_by')->first();
        return view('admin.rr-view', compact('data_rr', 'data_rr_item', 'data_rr_info'));
    }
}
