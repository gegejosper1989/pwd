<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use App\Employee;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class DepartmentController extends Controller
{
    //
    public function add(Request $req){
        $dataDep = new Department();
        $dataDep->depcode = $req->depcode;
        $dataDep->depname = $req->depname;
        $dataDep->dephead = $req->dephead;
        $dataDep->save();

        $getEmployee = Employee::where('id', '=', $req->dephead)->first();
        
        $empName = $getEmployee->fname." ".$getEmployee->mname. " ". $getEmployee->lname;

        $depArray = array(['id' => $dataDep->id,
        'depcode' => $req->depcode,
        'depname' => $req->depname,
        'depheadid' => $req->dephead,
        'dephead' => $empName
        ]);

        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::info($name.' added new department named '.$req->depname);
        
        return response()->json($depArray);
    }

    public function delete(Request $req){
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::info($name.' deleted department '.$req->id);

        Department::find($req->id)->delete();
        return response()->json();
    }
    public function update(Request $req)
    {
        $dataDep = Department::find($req->id);
        $dataDep->depcode = $req->depcode;
        $dataDep->depname = $req->depname;
        $dataDep->dephead = $req->dephead;
        $dataDep->save();

        $getEmployee = Employee::where('id', '=', $req->dephead)->first();
        
        $empName = $getEmployee->fname." ".$getEmployee->mname. " ". $getEmployee->lname;
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::notice($name.' updated the department named '.$req->depname);

        $depArray = array(['id' => $dataDep->id,
        'depcode' => $req->depcode,
        'depname' => $req->depname,
        'depheadid' => $req->dephead,
        'dephead' => $empName
        ]);
        return response()->json($depArray);
    }
}
