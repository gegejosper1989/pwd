<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class EmployeeController extends Controller
{
    //
        //
        public function add(Request $req){
            
            $data_employee = new Employee();
            $data_employee->fname = $req->fname;
            $data_employee->lname = $req->lname;
            $data_employee->mname = $req->mname;
            $data_employee->position = $req->position;
            $data_employee->department = $req->department;
            $data_employee->save();
    
        
            $emp_array = array(['id' => $data_employee->id,
            'fname' => $req->fname,
            'lname' => $req->lname,
            'mname' => $req->mname,
            'position' => $req->position,
            'department' => $req->department
            ]);
    
            if (Auth::check())
            {
                $name = Auth::user()->name;
            }
            Log::info($name.' added new Employee named '.$req->fname);
            
            return response()->json($emp_array);
        }
    
        public function delete(Request $req){
            if (Auth::check())
            {
                $name = Auth::user()->name;
            }
            Log::info($name.' deleted Employee '.$req->id);
    
            Employee::find($req->id)->delete();
            return response()->json();
        }
        public function update(Request $req)
        {
            $data_employee = Employee::find($req->id);
            $data_employee->fname = $req->fname;
            $data_employee->lname = $req->lname;
            $data_employee->mname = $req->mname;
            $data_employee->position = $req->position;
            $data_employee->department = $req->department;
            $data_employee->save();
    
           
            $emp_name = $req->fname." ".$req->mname. " ". $req->lname;
            if (Auth::check())
            {
                $name = Auth::user()->name;
            }
            Log::notice($name.' updated the employee named '.$emp_name);
    
            $emp_array = array(['id' => $data_employee->id,
            'fname' => $req->fname,
            'lname' => $req->lname,
            'mname' => $req->mname,
            'position' => $req->position,
            'department' => $req->department
            ]);
            return response()->json($emp_array);
        }
}
