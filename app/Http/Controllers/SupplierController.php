<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Supplier;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class SupplierController extends Controller
{
    //

    public function add(Request $req){
        $dataSupplier = new Supplier();
        $dataSupplier->suppliername = $req->suppliername;
        $dataSupplier->status = 'initial';
        $dataSupplier->save();
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::info($name.' added new Supplier'.$req->vehiclenum);
        return response()->json($dataSupplier);
    }

    public function delete(Request $req){
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::warning($name.' deleted Supplier '.$req->id);
        Supplier::find($req->id)->delete();
        return response()->json();
    }
    public function update(Request $req)
    {
        
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::notice($name.' updated Supplier '.$req->suppliername);
        $dataSupplier = Supplier::find($req->id);
        $dataSupplier->suppliername = $req->suppliername;
        $dataSupplier->save();
        return response()->json($dataSupplier);
    }
}
