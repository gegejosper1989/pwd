<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    //
    public function add(Request $req){
        $dataCat = new Category();
        $dataCat->catcode = $req->catcode;
        $dataCat->catname = $req->catname;
        $dataCat->save();
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::warning($name.' added Category '.$req->catname);

        return response()->json($dataCat);
    }

    public function delete(Request $req){
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::warning($name.' deleted Category '.$req->id);
        
        Category::find($req->id)->delete();
        return response()->json();
    }
    public function update(Request $req)
    {
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::warning($name.' deleted Category '.$req->catname);
        
        $dataCat = Category::find($req->id);
        $dataCat->catcode = $req->catcode;
        $dataCat->catname = $req->catname;
        $dataCat->save();
        return response()->json($dataCat);
    }
}
