<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Returninfo;
use App\Returndetail;
use App\Stock;
use App\Bincard;
use App\Signature_rms;

class ReturnController extends Controller
{
    //
    public function rms_create(Request $req){

        $data_return = new Returninfo();
        $data_return->return_date = $req->return_date;
        $data_return->return_number = $req->return_number;
        $data_return->srs_num = $req->srs_num;
        $data_return->reason = $req->reason;
        $data_return->department = $req->department;
        $data_return->requestor = $req->requisitioner;
        $data_return->status = 'initial';
        $data_return->save();
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::notice($name.' created RMS with rms #'.$data_return->return_number);

        return redirect('/admin/rms/create/'.$data_return->id);
    }

    public function create_rms_items ($return_id){
        
        $return_data = Returninfo::with('employee')->where('id', '=', $return_id)->first();
        $return_detail = Returndetail::with('stock')->where('return_number', '=', $return_data->return_number)->where('status', '=', 'initial')->get();
        
        $dataStock = Stock::take(20)->get();
        
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::notice($name.' created RMS details for rms #'.$return_data->return_number);
        
        return view('admin.rms-create', compact('return_data', 'return_detail', 'dataStock'));
    }
    public function rms_items_add (Request $req){
        
            
        $data_return_detail = new Returndetail();
        $data_return_detail->return_number = $req->return_number;
        $data_return_detail->cat_number = $req->catcode;
        $data_return_detail->stock_number = $req->stock_id;
        $data_return_detail->quantity = $req->return_quantity;
        $data_return_detail->status = 'initial';
        $data_return_detail->save();
        $data_return_detail->stock_name = $req->stockname;
        $data_return_detail->stock_id = $req->stocknumber;
        return response($data_return_detail);
    }
    public function rms_items_delete(Request $req)
    {
        Returndetail::find($req->id)->delete();
        return response($req->id);
    }

    public function process_rms (Request $req){
        
        $data_return_info = Returninfo::where('return_number', '=', $req->return_number)->first();
        $data_return_item = Returndetail::where('return_number', '=', $req->return_number)->get();

        foreach($data_return_item as $return_item)   {
            $dataStock = Stock::where('catcode', '=', $return_item->cat_number)
                ->where('id', '=', $return_item->stock_number)
                ->first();
            $quantity = $dataStock->quantity + $return_item->quantity;
            Stock::where('id', '=', $dataStock->id)
                    ->update(['quantity' => $quantity]);
            
            $data_bincard = new Bincard();
            $data_bincard->stockid = $dataStock->id;
            $data_bincard->stockcategoryid = $return_item->cat_number;
            $data_bincard->quantity = $return_item->quantity;
            $data_bincard->type = 'RETURNED';
            $data_bincard->unitcost = '0';
            $data_bincard->reference = $req->return_number;
            $data_bincard->department = 'N/A';
            $data_bincard->balance = $quantity;
            $data_bincard->save();
        }
        
        Returndetail::where('return_number', '=', $req->return_number)
                 ->update(['status' => 'processed']);
        
        Returninfo::where('return_number', '=', $req->return_number)
                 ->update(['status' => 'processed']);
        return response()->json($req);
    }

    public function rms_view($return_number){
        $return_info = Returninfo::with('employee')->where('return_number', '=', $return_number)->first();
        $return_detail = Returndetail::where('return_number', '=', $return_number)->with('stock')->get();
                
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::notice($name.' created RMS details for rms #'.$return_number);
        $data_rms = Signature_rms::with('employee_approved', 'employee_approved_return', 'employee_posted_bin_card', 'employee_posted_stock_card', 'employee_accounted_by')->first();
        return view('admin.rms-view', compact('return_info', 'return_detail', 'data_rms'));
    }
}
