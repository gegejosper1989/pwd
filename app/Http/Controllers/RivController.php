<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rivdetail;
use App\Rivitem;
use App\Stock;
use App\Budget;
use App\Consessionaire;
use App\Bincard;
use App\Signature_riv;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class RivController extends Controller
{
    //

    public function rivItemsAdd (Request $req){
        
            
        $dataRiv = new Rivitem();
        $dataRiv->rivnumber = $req->rivnumber;
        $dataRiv->catcode = $req->catcode;
        $dataRiv->stocknumber = $req->stock_id;
        $dataRiv->description = $req->description;
        $dataRiv->onhandquantity = $req->onhandquantity;
        $dataRiv->reqquantity = $req->reqquantity;
        $dataRiv->status = 'initial';
        $dataRiv->save();
        $dataRiv->stock_id = $req->stocknumber;
        return response($dataRiv);
    }

    public function rivConcessionareAdd (Request $req){
        
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::info($name.' added concessionaire '.$req->fullname.' on RIV #'.$req->rivnumber);
        $dataConcessionare = new Consessionaire();
        $dataConcessionare->rivnumber = $req->rivnumber;
        $dataConcessionare->fullname = $req->fullname;
        $dataConcessionare->address = $req->address;
        $dataConcessionare->meternum = $req->meternum;
        $dataConcessionare->incharge = $req->incharge;
        $dataConcessionare->status = 'initial';
        $dataConcessionare->save();
        return response($dataConcessionare);
    }

    public function rivItemsDelete(Request $req)
    {
        Rivitem::find($req->id)->delete();
        return response($req->id);
    }

    public function rivConcessionareDelete(Request $req)
    {
        Consessionaire::find($req->id)->delete();
        return response($req->id);
    }
    
    public function createRiv(Request $req){
        
        if(empty($req->workorder)) {
            $workorder = 'no details';
        }
        else {
            $workorder = $req->workorder;
        }
        if(empty($req->vehicle)) {
            $vehicle = 'no details';
        }
        else {
            $vehicle = $req->vehicle;
        }
        $dataRiv = new Rivdetail();
        $dataRiv->date = $req->rivdate;
        $dataRiv->rivnumber = $req->rivnumber;
        $dataRiv->purpose = $req->purpose;
        $dataRiv->department = $req->department;
        $dataRiv->workorder = $workorder;
        $dataRiv->vehiclenum = $vehicle;
        $dataRiv->incharge = '';
        $dataRiv->status = 'initial';
        $dataRiv->save();
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::notice($name.' created RIV with riv #'.$req->rivnumber);

        return redirect('/admin/riv/create/'.$dataRiv->id);
    }

    public function createRivItems ($rivid){
        
        $rivDetail = Rivdetail::where('id', '=', $rivid)->first();
        $dataRivDetail = Rivitem::where('rivnumber', '=', $rivDetail->rivnumber)->where('status', '=', 'initial')->get();
        $dataConsessionaire = Consessionaire::where('rivnumber', '=', $rivDetail->rivnumber)->where('status', '=', 'initial')->get();
        $dataStock = Stock::take(20)->get();
        $dataBudget = Budget::with('stock')->where('department', '=', $rivDetail->department)->get();
        
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::notice($name.' created RIV details for riv #'.$rivDetail->rivnumber);
        
        return view('admin.riv-create', compact('dataStock', 'dataRivDetail', 'dataConsessionaire', 'rivDetail', 'dataBudget'));
    }

    public function rivlist(){
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::info($name.' view RIV List page');
        $dataRecentRiv = Rivdetail::paginate(50);
        return view('admin.riv-report', compact('dataRecentRiv'));
    }
    public function viewRivItems($rivnumber){
        $rivDetail = Rivdetail::where('rivnumber', '=', $rivnumber)->first();
        $dataRivDetail = Rivitem::where('rivnumber', '=', $rivDetail->rivnumber)->with('stockdetails')->get();
        $dataConsessionaire = Consessionaire::where('rivnumber', '=', $rivDetail->rivnumber)->get();
       
        $dataBudget = Budget::with('stock')->where('department', '=', $rivDetail->department)->get();
        
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::notice($name.' created RIV details for riv #'.$rivDetail->rivnumber);
        $data_riv = Signature_riv::with('employee_request_officer', 'employee_approved_release', 'employee_issued_by', 'employee_posted_by')->first();
        return view('admin.riv-view', compact('dataStock', 'dataRivDetail', 'dataConsessionaire', 'rivDetail', 'dataBudget', 'data_riv'));
    }

    public function processRiv (Request $req){
        $dataRivItem = Rivitem::where('rivnumber', '=', $req->rivnumber)->get();
        $dataRivDetail = Rivdetail::where('rivnumber', '=', $req->rivnumber)->first();

        foreach($dataRivItem as $rivItem)   {
            $dataStock = Stock::where('catcode', '=', $rivItem->catcode)
                ->where('id', '=', $rivItem->stocknumber)
                ->first();
            $quantity = $dataStock->quantity - $rivItem->reqquantity;
            Stock::where('id', '=', $dataStock->id)
                    ->update(['quantity' => $quantity]);
            $dataBudget = Budget::where('department', '=', $dataRivDetail->department)
                ->where('category', '=', $rivItem->catcode)
                ->where('stocknumber', '=', $dataStock ->id)
                ->first();
            $budgetquantity = $dataBudget->consumed + $rivItem->reqquantity;
            Budget::where('id', '=', $dataBudget->id)
                    ->update(['consumed' => $budgetquantity]);
            $data_bincard = new Bincard();
            $data_bincard->stockid = $dataStock->id;
            $data_bincard->stockcategoryid = $rivItem->catcode;
            $data_bincard->quantity = $rivItem->reqquantity;
            $data_bincard->type = 'ISSUED';
            $data_bincard->unitcost = '0';
            $data_bincard->reference = $req->rivnumber;
            $data_bincard->department = $dataRivDetail->department;
            $data_bincard->balance = $quantity;
            $data_bincard->save();
        }
        
        Rivitem::where('rivnumber', '=', $req->rivnumber)
                 ->update(['status' => 'processed']);
        Consessionaire::where('rivnumber', '=', $req->rivnumber)
                 ->update(['status' => 'processed']);
        Rivdetail::where('rivnumber', '=', $req->rivnumber)
                 ->update(['status' => 'processed']);
        return response()->json($req);
    }
}
