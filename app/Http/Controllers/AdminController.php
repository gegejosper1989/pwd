<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stock;
use App\Position;
use App\Budget;
use App\Category;
use App\Bincard;
use App\Department;
use App\Employee;
use App\User;
use App\Rivitem;
use App\Prdetail;
use App\Pritem;
use App\Rivdetail;
use App\Returninfo;
use App\Returndetail;
use App\Vehicle;
use App\Consessionaire;
use App\Supplier;
use App\Receiving;
use App\Signature_pr;
use App\Signature_riv;
use App\Signature_rms;
use App\Signature_rr;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;


class AdminController extends Controller
{
    //
    public function index()
    {
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::info($name.' opened Home page');
        $return_data = Returninfo::take(5)->latest()->get();
        $dataRecentRiv = Rivdetail::take(5)->latest()->get();
        $data_rr= Receiving::with('supplierdetail')->take(5)->latest()->get();
        $dataPrdetail = Prdetail::with('supplier')->latest()->take(5)->get();

        $data_out_of_stocks = Stock::whereColumn('quantity', '<=','orderingpoint')->get();
        //$dataOutOfStock = Productquantity::whereColumn('quantity', '<=','orderingpoint')->with('product', 'productvariants')->get();
        return view('admin.dashboard', compact('dataRecentRiv', 'return_data', 'data_rr', 'dataPrdetail', 'data_out_of_stocks'));
    }
    public function rr()
    {
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        $rrCount = Receiving::whereMonth('created_at', '=', date('m'))->count();
        $data_pr =  Prdetail::where('prstatus', '=','requesting')->get();
        $dataSupplier = Supplier::get();
        $data_rr= Receiving::with('supplierdetail')->take(10)->latest()->get();
        //dd($data_rr);
        Log::info($name.' opened Receiving Report page');
        return view('admin.rr', compact('dataSupplier', 'rrCount', 'data_pr', 'data_rr'));
    }
    public function pr()
    {
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        $prCount = Prdetail::whereMonth('created_at', '=', date('m'))->count();
        $dataSupplier = Supplier::get();
        $dataPrdetail = Prdetail::with('supplier')->latest()->paginate(50);
        Log::info($name.' opened Purchase Request page');
        return view('admin.pr', compact('dataSupplier', 'prCount', 'dataPrdetail'));
    }
    public function riv()
    {
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::info($name.' opened RIV page');
        $rivCount = Rivdetail::whereMonth('created_at', '=', date('m'))->count();
        $dataRivDetail = Rivitem::where('status', '=', 'initial')->get();
        $dataConsessionaire = Consessionaire::where('status', '=', 'initial')->get();
        $dataStock = Stock::take(20)->get();
        $dataRecentRiv = Rivdetail::take(10)->latest()->get();
        //dd($dataConsessionaire);
        $dataVehicle = Vehicle::get();
        $dataDeparment = Department::get();
        return view('admin.riv', compact('dataStock', 'dataRivDetail', 'dataConsessionaire', 'rivCount', 'dataRecentRiv', 'dataDeparment', 'dataVehicle'));
    }
    public function rms()
    {
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        $dataDeparment = Department::get();
        $returnCount = Returninfo::whereMonth('created_at', '=', date('m'))->count();
        $return_data = Returninfo::take(10)->latest()->get();
        $data_employee = Employee::all();
        Log::info($name.' opened RMS page');
        return view('admin.rms', compact('returnCount', 'return_data', 'dataDeparment', 'data_employee'));
    }
    public function stocks()
    {
        
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::info($name.' opened Stocks page');
        $dataStock = Stock::with('category')->paginate(50);
        $dataCategory = Category::get();
        
        //dd($dataStock);
        return view('admin.stocks', compact('dataStock', 'dataCategory'));
    }
    public function bincard()
    {
        
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::info($name.' opened Bincard page');
        $dataStock = Stock::take(30)->get();
       
        return view('admin.bincard', compact('dataStock'));
    }
    public function viewbincard($stockid)
    {
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::info($name.' opened Bincard for stock '.$stockid );

        $dataStock = Stock::take(30)->get();
        $getStock = Stock::where('id', '=', $stockid)->first();
        $data_bincard = Bincard::where('stockid', '=', $stockid)->get();
        return view('admin.viewbincard', compact('dataStock', 'getStock', 'data_bincard'));
    }
    public function watermeter()
    {
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        $data_water_meters = Consessionaire::paginate(50);
        Log::info($name.' opened Water Meter page');
        return view('admin.watermeter', compact('data_water_meters'));
    }
    public function reorder()
    {
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::info($name.' opened Re-order page');
        $dataStockNoReorder = Stock::where('orderingpoint', '=', '')->orWhere('orderingpoint', '=', 0)->with('category')->get();
        $dataStock = Stock::where('orderingpoint', '>', 0)->with('category')->get();
        return view('admin.reorder', compact('dataStock', 'dataStockNoReorder'));
    }
    public function category()
    {
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::info($name.' opened Category page');
        $dataCategory = Category::get();
        return view('admin.category', compact('dataCategory'));
    }
    public function descriptions()
    {
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::info($name.' opened Stock List page');
        $dataCategory = Category::get();
        $dataStock = Stock::with('category')->paginate(50);
        return view('admin.descriptions', compact('dataCategory', 'dataStock'));
    }
    public function accounts()
    {
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::info($name.' opened Accounts page');
        return view('admin.accounts');
    }
    public function deparments()
    {
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::info($name.' opened Departments page');
        $dataDeparment = Department::with('employee')->get();
        $dataEmployee = Employee::get();
        return view('admin.deparments', compact('dataDeparment', 'dataEmployee'));
    }
    public function employees()
    {
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::info($name.' opened Employees page');
        $data_deparment = Department::get();
        $data_employee = Employee::with('department_details')->get();
        //dd($data_employee);
        return view('admin.employees', compact('data_deparment', 'data_employee'));
    }
    public function vehicles(){
        
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::info($name.' opened Vehicles page');
        $dataVehicle = Vehicle::get();
        return view('admin.vehicle', compact('dataVehicle'));
    }

    public function suppliers(){
        
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::info($name.' opened Suppliers page');
        $dataSupplier = Supplier::get();
        return view('admin.suppliers', compact('dataSupplier'));
    }
    public function budget()
    {
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::info($name.' opened Budget page');
        $dataDepartment = Department::get();
        $dataBudget = Budget::with('stock')->get();
        return view('admin.budget', compact( 'dataDepartment', 'dataBudget'));
    }
    public function logs()
    {
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::info($name.' opened Logs page');
        return view('admin.logs');
    }
    public function backup()
    {
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::info($name.' opened Backup page');
        return view('admin.backup');
    }
    public function consessioaniresearch(Request $request){
        if($request->ajax())
        {
            $data_consessionaire = Consessionaire::where('fullname','LIKE','%'.$request->search.'%')
                ->get();
            $output="";
           
            if($data_consessionaire)
            {  
                foreach ($data_consessionaire as $Consessionaire) {
                    // $dataBudget = Budget::with('stock')->where('department', '=', $request->department)->where('stocknumber', '=', $StockResult->id)->get();
                    // foreach($dataBudget as $Stock){
                        $output .='<tr><td><a href="/admin/riv/view/'.$Consessionaire->rivnumber.'">'.$Consessionaire->rivnumber.'</a></td>
                            <td>'.$Consessionaire->created_at->format('m-d-Y').'</td>
                            <td>'.$Consessionaire->meternum.'</td>
                            <td>'.$Consessionaire->fullname.'</td>
                            <td>'.$Consessionaire->address.'</td>
                            <td>'.$Consessionaire->incharge.'</td>
                            <td>';
                            // if($Stock->stock->quantity > 0) {
                                // if($Stock->budget > $Stock->consumed){
                                    $output.='<a href="javascript:;" class="edit-modal btn btn-sm btn-success" 
                                    data-id = "'.$Consessionaire->id.'"
                                    data-meternum = "'.$Consessionaire->meternum.'"
                                    data-fullname = "'.$Consessionaire->fullname.'"
                                    data-address = "'.$Consessionaire->address.'"
                                    data-incharge = "'.$Consessionaire->incharge.'">
                                    <i class="fa fa-pencil"></i>
                                    </a>';
                                // }    
                            // }
                        $output .='</td></tr>';
                    }
                } 
            }
            return Response($output);
    }

    public function consessionaire_edit(Request $req){

        $update_consessionaire = Consessionaire::where('id', '=', $req->id)
                    ->update(['meternum' => $req->meternum,
                    'fullname' => $req->fullname,
                    'address' => $req->address,
                    'incharge' => $req->incharge
                    ]);
        $onsessionaire_array = array(['id' => $req->id,
        'meternum' => $req->meternum,
        'fullname' => $req->fullname,
        'address' => $req->address,
        'incharge' => $req->incharge,
        'riv_num' => $req->riv_num,
        'date_created' => $req->date_created
        ]);
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::notice($name.' updated the Consessionaire '.$req->fullname);

        //dd($stockArray);
        return response($onsessionaire_array);

    }

    public function signatures(){

        $data_pr = Signature_pr::with('employee_prepared_by', 'employee_certified_by', 'employee_approved_by')->first();
        $data_riv = Signature_riv::with('employee_request_officer', 'employee_approved_release', 'employee_issued_by', 'employee_posted_by')->first();
        $data_rms = Signature_rms::with('employee_approved', 'employee_approved_return', 'employee_posted_bin_card', 'employee_posted_stock_card', 'employee_accounted_by')->first();
        $data_rr = Signature_rr::with('employee_member_1', 'employee_member_2', 'employee_team_leader', 'employee_accepted_by', 'employee_certified_correct', 'employee_posted_bin_card', 'employee_posted_stock_card', 'employee_costed_by')->first();
        $data_employee = Employee::get();
        return view('admin.signatures', compact('data_pr', 'data_riv', 'data_rms', 'data_rr','data_employee'));
    }

}
