<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Rivdetail;
use App\Rivitem;
use App\Consessionaire;
use App\Stock;
use App\Vehicle;
use App\Department;
use App\Budget;
use App\Signature_riv;

class DeparmentAccountController extends Controller
{
    //
    public function index(Request $req){
        //return('Hello');
        if (Auth::check())
        {
            $name = Auth::user()->name;
            $department = Auth::user()->department;
        }
        Log::info($name.' opened RIV page');
        //$rivCount = Rivdetail::whereMonth('created_at', '=', date('m'))->count();
        $dataRivDetail = Rivitem::where('status', '=', 'initial')->get();
        $dataConsessionaire = Consessionaire::where('status', '=', 'initial')->get();
        $dataStock = Stock::take(20)->get();
        $dataRecentRiv = Rivdetail::where('department', '=', $department)->take(10)->latest()->get();
        //dd($dataConsessionaire);
        $dataVehicle = Vehicle::get();
        $dataDeparment = Department::get();
        return view('department.home', compact('dataStock', 'dataRivDetail', 'dataConsessionaire', 'dataRecentRiv', 'dataDeparment', 'dataVehicle', 'department'));
    }

    public function create_riv(Request $req){
        
        if(empty($req->workorder)) {
            $workorder = 'no details';
        }
        else {
            $workorder = $req->workorder;
        }
        if(empty($req->vehicle)) {
            $vehicle = 'no details';
        }
        else {
            $vehicle = $req->vehicle;
        }
        $dataRiv = new Rivdetail();
        $dataRiv->date = $req->rivdate;
        $dataRiv->rivnumber = 'RIV-'.$req->department.'-000';
        $dataRiv->purpose = $req->purpose;
        $dataRiv->department = $req->department;
        $dataRiv->workorder = $workorder;
        $dataRiv->vehiclenum = $vehicle;
        $dataRiv->incharge = '';
        $dataRiv->status = 'initial-dpt';
        $dataRiv->save();
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::notice($name.' created RIV for '.$req->department);

        return redirect('/department/riv/create/'.$dataRiv->id);
    }

    public function create_riv_items ($rivid){
        
        $rivDetail = Rivdetail::where('id', '=', $rivid)->first();
        $dataRivDetail = Rivitem::where('rivnumber', '=', $rivDetail->rivnumber)->where('status', '=', 'initial-dpt')->get();
       
        $dataStock = Stock::take(20)->get();
        $dataBudget = Budget::with('stock')->where('department', '=', $rivDetail->department)->get();
        
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::notice($name.' created RIV details for riv #'.$rivDetail->rivnumber);
        
        return view('department.riv-create', compact('dataStock', 'dataRivDetail', 'rivDetail', 'dataBudget'));
    }

    public function riv_items_add (Request $req){
        
            
        $dataRiv = new Rivitem();
        $dataRiv->rivnumber = $req->rivnumber;
        $dataRiv->catcode = $req->catcode;
        $dataRiv->stocknumber = $req->stock_id;
        $dataRiv->description = $req->description;
        $dataRiv->onhandquantity = $req->onhandquantity;
        $dataRiv->reqquantity = $req->reqquantity;
        $dataRiv->status = 'initial-dpt';
        $dataRiv->save();
        $dataRiv->stock_id = $req->stocknumber;
        return response($dataRiv);
    }

    public function riv_items_delete(Request $req)
    {
        Rivitem::find($req->id)->delete();
        return response($req->id);
    }

    public function view_riv_items($rivnumber){
        $rivDetail = Rivdetail::where('rivnumber', '=', $rivnumber)->first();
        $dataRivDetail = Rivitem::where('rivnumber', '=', $rivDetail->rivnumber)->with('stockdetails')->get();
        $dataConsessionaire = Consessionaire::where('rivnumber', '=', $rivDetail->rivnumber)->get();
       
        $dataBudget = Budget::with('stock')->where('department', '=', $rivDetail->department)->get();
        
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::notice($name.' created RIV details for riv #'.$rivDetail->rivnumber);
        $data_riv = Signature_riv::with('employee_request_officer', 'employee_approved_release', 'employee_issued_by', 'employee_posted_by')->first();
        return view('admin.riv-view', compact('dataStock', 'dataRivDetail', 'dataConsessionaire', 'rivDetail', 'dataBudget', 'data_riv'));
    }

    public function processRiv (Request $req){
        $dataRivItem = Rivitem::where('rivnumber', '=', $req->rivnumber)->get();
        $dataRivDetail = Rivdetail::where('rivnumber', '=', $req->rivnumber)->first();
        
        Rivitem::where('rivnumber', '=', $req->rivnumber)
                 ->update(['status' => 'requested']);
        
        Rivdetail::where('rivnumber', '=', $req->rivnumber)
                 ->update(['status' => 'requested']);
        return response()->json($req);
    }

    public function riv_stock_search(Request $request){
        if($request->ajax())
        {
            $dataStock = Stock::where('stockname','LIKE','%'.$request->search.'%')
                ->get();
            $output="";
           
            if($dataStock)
            {  
                foreach ($dataStock as $StockResult) {
                    $dataBudget = Budget::with('stock')->where('department', '=', $request->department)->where('stocknumber', '=', $StockResult->id)->get();
                    foreach($dataBudget as $Stock){
                        $output .='<tr><td><a href="/admin/stocks/category/'.$Stock->category.'">'.$Stock->category.'</a></td>
                            <td><a href="/admin/stock/'.$Stock->stocknumber.'">'.$Stock->stock->stocknumber.'</td>
                            <td>'.$Stock->stock->stockname.'</td>
                            <td>'.$Stock->stock->quantity.'</td>
                            <td>'.$Stock->budget.'</td>
                            <td>'.$Stock->consumed.'</td>
                            <td></td><td>';
                            if($Stock->stock->quantity > 0) {
                                if($Stock->budget > $Stock->consumed){
                                    $output.='<a href="javascript:;" class="add-modal btn btn-sm btn-success" 
                                    data-stocknumber="'.$Stock->stock->stocknumber.'"
                                    data-stock_id="'.$Stock->stock->id.'" 
                                    data-catcode="'.$Stock->category.'" 
                                    data-stockname="'.$Stock->stock->stockname.'" 
                                    data-quantity="'.$Stock->stock->quantity.'" 
                                    data-id="'.$Stock->stock->id.'">
                                    <i class="fa fa-plus"></i>
                                    </a>';
                                }    
                            }
                        $output .='</td></tr>';
                    }
                } 
            }
            return Response($output);
          
        }
    }
}
