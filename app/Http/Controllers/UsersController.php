<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Department;
use Validator;
use Response;
use Illuminate\Support\Facades\Input;
class UsersController extends Controller
{
    //
        //

        public function addUser(Request $request)
        {
            $rules = array(
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'username' => 'required|string|max:255|unique:users',
                'usertype' => 'required'
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return Response::json(array(
                         'errors' => $validator->getMessageBag()->toArray(),
                ));
            
            } else {
                $data = new User();
                $data->name = $request->name;
                $data->usertype = $request->usertype;
                $data->email = $request->email;
                $data->username = $request->username;
                $data->password = bcrypt($request->password);
                $data->department = $request->department;
                $data->save();
                
                return response()->json($data);
            }
        }
        
        public function readUser(Request $req)
        {
            $data = User::all();
            $dataDepartment = Department::get();
            return view('admin.users', compact('data', 'dataDepartment'));
            //return view('admin.home')->withData($data);
            
        }
        public function editUser(Request $req)
        {
            $data = User::find($req->id);
            $data->name = $req->name;
            $data->usertype = $req->usertype;
            $data->email = $req->email;
            $data->password = bcrypt($req->password);
            $data->save();
    
            return response()->json($data);
        }
        public function deleteUser(Request $req)
        {
            User::find($req->id)->delete();
            
            return response()->json();
        }
        public function showUser($id){
            $dataUser = User::where('id','=', $id)->get();
            
            return view('admin.user', compact('dataUser'));
        }
}
