<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vehicle;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class VehicleController extends Controller
{
    //
    public function add(Request $req){
        $dataVic = new Vehicle();
        $dataVic->vehiclenum = $req->vehiclenum;
        $dataVic->save();
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::info($name.' added new Vehicle'.$req->vehiclenum);
        return response()->json($dataVic);
    }

    public function delete(Request $req){
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::warning($name.' deleted vehicle '.$req->id);
        Vehicle::find($req->id)->delete();
        return response()->json();
    }
    public function update(Request $req)
    {
        
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::notice($name.' updated vehicle '.$req->vehiclenum);
        $dataVic = Vehicle::find($req->id);
        $dataVic->vehiclenum = $req->vehiclenum;
        $dataVic->save();
        return response()->json($dataVic);
    }
}
