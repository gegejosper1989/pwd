<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Budget;
use App\Department;
use App\Stock;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
class BudgetController extends Controller
{
    //
    public function updatebudget(Request $req){
        $updateBudget = Budget::where('id', '=', $req->id)
                    ->update(['consumed' => $req->consumed,
                    'budget' => $req->budget
                    ]);
        $budgetArray = array(['id' => $req->id,
        'catcode' => $req->catcode,
        'stocknumber' => $req->stocknumber,
        'stockid' => $req->stockid,
        'stockname' => $req->stockname,
        'consumed' => $req->consumed,
        'department' => $req->department,
        'budget' => $req->budget
        ]);

        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::alert($name.' update budget for budget id '.$req->id.' consumed to '.$req->consumed. ' and budget to '.$req->budget);
        return response($budgetArray);

    }

    public function processbudget(){
        $dataDeparment = Department::get();

        foreach($dataDeparment as $Department){
            
            $dataStock = Stock::get();
            
            foreach($dataStock as $Stock){
                $budgetQuery = Budget::where('department', '=', $Department->depcode)->where('stocknumber', '=', $Stock->id)->where('category', '=', $Stock->catcode)->count();
                if($budgetQuery == 0 ){
                    $dataBudget = new Budget();
                    $dataBudget->category = $Stock->catcode;
                    $dataBudget->stocknumber = $Stock->id;
                    $dataBudget->department = $Department->depcode;
                    $dataBudget->budget = '999';
                    $dataBudget->consumed = '0';
                    $dataBudget->status = 'initial';
                    $dataBudget->save(); 
                } 
            }
        }
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::notice($name.' Processed importing budget to each department');
        
        return redirect('/admin/processbudgetsuccess');
    }
    public function resetbudget(){
        
        $dataBudget = Budget::where('status', '=', 'initial')->update(['consumed' => 0]);
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::warning($name.' Reseted budget to each department');
        return redirect('/admin/resetbudgetsuccess');
    }
    public function processbudgetsuccess(){
        return view('admin.proc-success');
    }
    public function resetbudgetsuccess(){
        return view('admin.budget-success');
    }
}
