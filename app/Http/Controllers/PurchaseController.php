<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Prdetail;
use App\Pritem;
use App\Stock;
use App\Budget;
use App\Consessionaire;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Signature_pr;

class PurchaseController extends Controller
{
    //
    public function prItemsAdd (Request $req){
          
        $dataPr = new Pritem();
        $dataPr->prnumber = $req->prnum;
        $dataPr->catnumber = $req->catcode;
        $dataPr->stocknumber = $req->stockid;
        $dataPr->reqquantity = $req->reqquantity;
        $dataPr->receivequant = 0;
        $dataPr->unitcost = 0;
        $dataPr->totalcost = 0;
        $dataPr->status = 'initial';
        $dataPr->save();
        $dataPr->stockid = $req->stocknumber;
        $dataPr->description = $req->description;
        //dd($dataPr);
        return response($dataPr);
    }

    

    public function prItemsDelete(Request $req)
    {
        Pritem::find($req->id)->delete();
        return response($req->id);
    }


    
    public function createPr(Request $req){
        
        if(empty($req->workorder)) {
            $workorder = 'no details';
        }
        else {
            $workorder = $req->workorder;
        }
        if(empty($req->vehicle)) {
            $vehicle = 'no details';
        }
        else {
            $vehicle = $req->vehicle;
        }
        $dataPr = new Prdetail();
        $dataPr->prdate = $req->prDate;
        $dataPr->prnum = $req->prnumber;
        $dataPr->prsupplier = $req->supplier;
        $dataPr->paymentmode = $req->paymentmode;
        $dataPr->prstatus = 'initial';
        $dataPr->save();
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::notice($name.' created PR with PR #'.$req->prnumber);

        return redirect('/admin/pr/create/'.$dataPr->id);
    }

    public function createPrItems ($prid){
        
        $DetailPr = Prdetail::where('id', '=', $prid)->first();
        $dataPrDetail = Pritem::where('prnumber', '=', $DetailPr->prnum)->where('status', '=', 'initial')->get();
        
        $dataStock = Stock::paginate(20);
    
        
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::notice($name.' created PR details for PR #'.$DetailPr->prnum);
        
        return view('admin.pr-create', compact('dataStock', 'DetailPr', 'dataPrDetail'));
    }

    public function prlist(){
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::info($name.' view PR List page');
        $dataRecentPr = Prdetail::paginate(50);
        return view('admin.pr-report', compact('dataRecentPr'));
    }
    public function viewPrItems($rivnumber){

    }


    public function process_pr ($pr_num){
        Pritem::where('prnumber', '=', $pr_num)
                 ->update(['status' => 'requesting']);
        Prdetail::where('prnum', '=', $pr_num)
                 ->update(['prstatus' => 'requesting']);
        return redirect('/admin/pr-view/'.$pr_num);
        Log::notice($name.' Processed PR #'.$pr_num);
        //return response()->json($req);
    }

    public function pr_view($pr_num){
        $DetailPr = Prdetail::where('prnum', '=', $pr_num)->first();
        $dataPrDetail = Pritem::where('prnumber', '=', $pr_num)->get();
        
        
        if (Auth::check())
        {
            $name = Auth::user()->name;
        }
        Log::notice($name.' created PR details for PR #'.$pr_num);
        $data_pr = Signature_pr::with('employee_prepared_by', 'employee_certified_by', 'employee_approved_by')->first();
        return view('admin.pr-view', compact('DetailPr', 'dataPrDetail', 'data_pr'));

    }
}
