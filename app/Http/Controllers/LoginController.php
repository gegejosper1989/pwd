<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LoginController extends Controller
{
    public function userLogin(Request $request){
       
        if(Auth::attempt([
            'username' => $request->username,
            'password' => $request->password
        ])){
            $user = User::where('username', $request->username )->first();
        
            if($user->usertype=='admin'){
                $name = $user->name;
                Log::info($name.' logged-in in the System');
                return redirect('admin/home');
               
            }
            elseif($user->usertype=='department'){
                $name = $user->name;
                Log::info($name.' logged-in in the Department Account');
                return redirect('department/home');
               
            }
            else {
                return redirect('/');
            }
        }
        else {
            //return('error');
            return redirect()->back()->with('error', 'Login Credentials Incorrect!');
        }
        //return redirect('dashboard');
    }
}
