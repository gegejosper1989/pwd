<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;

use Closure;

class DepartmentAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $month = env('MONTH_EXPIRE');
        $year = env('YEAR_EXPIRE');
        if(date('m-Y') > date($month.'-'.$year)){
            return redirect('/expired');
            //dd('Error');
        }
        else {
            if(Auth::check()){
                if(Auth::user()->usertype == 'department'){       
                    return $next($request);
                }
                else {
                    //return back();
                    return redirect('/');
                }
            }
            //return back();
            return redirect('/');
        }
    }
}
