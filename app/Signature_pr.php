<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Signature_pr extends Model
{
    //
    public function employee_prepared_by()
    {
        return $this->belongsTo('App\Employee','prepared_by','id');
    }
    public function employee_certified_by()
    {
        return $this->belongsTo('App\Employee','certified_by','id');
    }
    public function employee_approved_by()
    {
        return $this->belongsTo('App\Employee','approved_by','id');
    }
    
}
