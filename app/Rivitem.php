<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rivitem extends Model
{
    //
    public function stockdetails()
    {
        return $this->belongsTo('App\Stock','stocknumber','id');
    }
}
