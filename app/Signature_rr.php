<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Signature_rr extends Model
{
    //
    public function employee_member_1()
    {
        return $this->belongsTo('App\Employee','member_1','id');
    }
    public function employee_member_2()
    {
        return $this->belongsTo('App\Employee','member_2','id');
    }
    public function employee_team_leader()
    {
        return $this->belongsTo('App\Employee','team_leader','id');
    }
    public function employee_accepted_by()
    {
        return $this->belongsTo('App\Employee','accepted_by','id');
    }

    public function employee_certified_correct()
    {
        return $this->belongsTo('App\Employee','certified_correct','id');
    }

    public function employee_posted_bin_card()
    {
        return $this->belongsTo('App\Employee','posted_bin_card','id');
    }
    public function employee_posted_stock_card()
    {
        return $this->belongsTo('App\Employee','posted_stock_card','id');
    }

    public function employee_costed_by()
    {
        return $this->belongsTo('App\Employee','costed_by','id');
    }
}
