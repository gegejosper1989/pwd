<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bincard extends Model
{
    //
    public function stock()
    {
        return $this->belongsTo('App\Stock','stockid','id');
    }
}
