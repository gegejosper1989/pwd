<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Signature_riv extends Model
{
    //
    public function employee_request_officer()
    {
        return $this->belongsTo('App\Employee','request_officer','id');
    }

    public function employee_approved_release()
    {
        return $this->belongsTo('App\Employee','approved_release','id');
    }

    public function employee_issued_by()
    {
        return $this->belongsTo('App\Employee','issued_by','id');
    }

    public function employee_posted_by()
    {
        return $this->belongsTo('App\Employee','posted_by','id');
    }
}
