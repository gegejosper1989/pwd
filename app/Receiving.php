<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receiving extends Model
{
    //
    public function supplierdetail()
    {
        return $this->belongsTo('App\Supplier','supplier','id');
    }

    public function pr_detail()
    {
        return $this->belongsTo('App\Prdetail','pr_number','prnum');
    }

    
}
