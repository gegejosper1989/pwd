<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Returninfo extends Model
{
    //
    public function employee()
    {
        return $this->belongsTo('App\Employee','requestor','id');
    }
}
