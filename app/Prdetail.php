<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prdetail extends Model
{
    //
    public function supplier()
    {
        return $this->belongsTo('App\Supplier','prsupplier','id');
    }
}
