<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Signature_rms extends Model
{
    //
    public function employee_approved()
    {
        return $this->belongsTo('App\Employee','approved_by','id');
    }

    public function employee_approved_return()
    {
        return $this->belongsTo('App\Employee','approved_return','id');
    }

    public function employee_posted_bin_card()
    {
        return $this->belongsTo('App\Employee','posted_bin_card','id');
    }

    public function employee_posted_stock_card()
    {
        return $this->belongsTo('App\Employee','posted_stock_card','id');
    }
    public function employee_accounted_by()
    {
        return $this->belongsTo('App\Employee','accounted_by','id');
    }
}
