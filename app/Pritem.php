<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pritem extends Model
{
    //
    public function stock()
    {
        return $this->belongsTo('App\Stock','stocknumber','id');
    }
}
