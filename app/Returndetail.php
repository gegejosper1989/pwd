<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Returndetail extends Model
{
    //
    public function stock()
    {
        return $this->belongsTo('App\Stock','stock_number','id');
    }
}
